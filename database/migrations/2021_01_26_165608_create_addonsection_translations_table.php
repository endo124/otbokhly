<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddonsectionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addonsection_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('addonsection_id')->constrained('addonsection');
            $table->string('locale')->index();

            $table->string('name');

            $table->unique(['addonsection_id','locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addonsection_translations');
    }
}
