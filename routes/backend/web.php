<?php

use App\Http\Controllers\API\FeastController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('backend.login
//     ');
// });

/*******************frontend routes**************************/
Route::get('/user/reset/{token}', 'API\AuthController@showForgetForm');
Route::post('/user/reset', 'API\AuthController@reset')->name('reset');
Route::get('dashboard/login','Backend\LoginController@login');
Route::post('dashboard/checklogin','Backend\LoginController@checklogin');
Route::post('dashboard/logout','Backend\LoginController@logout')->middleware('admin');
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath','admin' ]
    ], function(){



        Route::group(['namespace'=>'Backend','prefix'=>'dashboard'], function () {
            Route::resource('/cook', 'CookController');

            Route::group(['middleware'=>'cookterms'], function () {

                Route::resource('/', 'HomeController');

                Route::resource('/admin', 'AdminController');

                Route::get('/changepass', 'AdminController@changepass');
                Route::put('/changepass', 'AdminController@changePassword')->name('changePassword');

                Route::resource('/cusine', 'CusineController');

                Route::resource('/section', 'SectionController');

                Route::resource('/addons', 'AddonsController');
                Route::resource('/addons_section', 'AddonSectionController');

                Route::resource('/category', 'CategoryController');

                // Route::resource('/cook', 'CookController');

                Route::get('/cook/active/{id}', 'CookController@active');
                Route::get('/cook/deactive/{id}', 'CookController@deactive');

                Route::resource('/dish', 'DishController');

                Route::resource('/discount', 'DiscountController');

                Route::resource('/allergen', 'AllergenController');

                Route::resource('/order', 'OrderController');
                Route::post('/order/reject', 'OrderController@reject');
                Route::get('/order/orderdetails/{id}','OrderController@orderdetails');


                Route::resource('/customer', 'CustomerController');

                Route::resource('/city', 'CityController');

                Route::resource('/setting', 'SettingController');
                Route::post('/setting/loyality', 'SettingController@loyality')->name('loyality');

                Route::resource('/advertisement', 'AdvertisementController');
                Route::get('/advertisement/details/{name}', 'AdvertisementController@details');
                Route::resource('/cookAdv', 'CookAdvController');
                Route::resource('/withdraw', 'WithdrawController');
                Route::get('/withdraw/cancel/{id}', 'WithdrawController@cancel')->name('withdraw.cancel');
                Route::get('/withdraw/decline/{id}', 'WithdrawController@decline')->name('withdraw.decline');
                Route::get('/withdraw/accept/{id}', 'WithdrawController@accept')->name('withdraw.accept');

                Route::get('/customer/vip/{id}', 'CustomerController@vip_customer');
                Route::get('/vipCustomers', 'CustomerController@vip');
                Route::get('/customer/reset/{id}', 'CustomerController@vip_reset');

                Route::get('/cook/reset/{id}', 'CookController@vip_reset');
                Route::get('/cook/vip/{id}', 'CookController@vip_cook');
                Route::get('/vipCooks', 'CookController@vip');

                Route::resource('/drivers', 'DriverController');
                Route::resource('/tracking-drivers', 'TrackingDriverController');

                Route::post('/getDataDriver', 'DriverController@getDataDriver');

                Route::resource('/terms', 'TermsController');

                Route::resource('feast', 'FeastController');
                Route::resource('account', 'AccountController');

                Route::resource('notification', 'NotificationController');
                Route::get('notification/readall', 'NotificationController@readall');

                Route::resource('/CRM', 'CrmController');


                Route::resource('support', 'SupportController');


                Route::resource('review', 'ReviewController');


            });

        });

    });





