<?php

use App\Http\Controllers\Backend\TermsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api','lang'], 'namespace' => 'API'], function () {
    Route::get('/dishes/{items}/{lang}/{location}/{cat_id?}','DishesNearYouController@getaddress');
    Route::get('/cities/{lang}', 'CityController@index');
    Route::get('/cook/{lang}/{id}', 'CookController@show');
    Route::get('/cooks/{lang}', 'CookController@index');
    Route::get('/categories/{lang}/{location}', 'CategoryController@getaddress');
    Route::get('/dish/{lang}/{id}', 'DishController@show');
    Route::get('/cooksnearyou/{items}/{lang}/{location}', 'CookNearYouController@getaddress');
    Route::post('/search/{lang}/{location}','DishController@getaddress');
    Route::post('/filter/{lang}/{location}','FilterController@getaddress');
    // Route::post('/search/{lang}','DishController@search');
    Route::get('/filter/{lang}','FilterController@index');
    // Route::post('/filter/{lang}','FilterController@show');
    Route::get('/vipcooks/{lang}','VIPCooksController@index');
    Route::get('/offers','AdvertisementController@index');
    Route::get('/cookadv','CookAdvController@index');

    Route::post('/fav/{lang}','FavController@store');
    Route::get('/fav/{lang}','FavController@index');

    Route::get('/privacy/{lang}','TermsController@index');

  //Cook APIs
  Route::post('cook/login','Cook\LoginController@login');
  Route::post('cook/logout', 'Cook\LoginController@logout');




Route::post('/token','OrderController@updateFcnToken');


});

Route::post('register', 'API\AuthController@register');
Route::post('login', 'API\AuthController@login');
Route::post('logout', 'API\AuthController@logout');
Route::post('refresh', 'API\AuthController@refresh');
Route::post('forgetpassword', 'API\AuthController@forgetpassword');


Route::group(['middleware' => ['api','lang','auth:api'], 'namespace' => 'API'], function () {
    Route::post('/changePass','AuthController@changePass');

    Route::get('/profile/{lang}/{id}','ProfileController@edit');
    Route::post('/customer-profile/{lang}','ProfileController@update');
    Route::post('/addTocart/{lang}','CartController@store');
    Route::put('/editcart/{lang}/{id}','CartController@update');
    Route::post('/cart/editqty','CartController@updateqty');
    Route::get('/cart/{lang}','CartController@index');
    Route::post('/checkcoupon','ApplyCouponController@check');
    Route::post('/checkpoints','ApplyCouponController@checkPoints');
    Route::post('/checkout','OrderController@store');
    Route::get('/orders/{lang}','OrderController@index');
    Route::post('/get_paid','OrderController@get_paid');
    Route::post('/feast/{lang}','FeastController@store');


    Route::post('/address','AddressController@store');
    Route::post('/address/{id}','AddressController@update');
    Route::get('/address/{id}','AddressController@destroy');
    Route::get('/address','AddressController@index');

    Route::get('/wishlist/dishes/{lang}/{items}', 'DishController@wishlist');
    Route::post('/dish/{lang}', 'DishController@store');//add to wishlist

    Route::get('/wishlist/cooks/{lang}/{items}', 'CookController@wishlist');
    Route::post('/cook/{lang}', 'CookController@store');//add to wishlist



    Route::get('/loyality', 'LoyalityController@index');//add to wishlist

    Route::post('/review', 'ReviewController@store');//add to wishlist

    //Cook APIs
    Route::post('profile/{lang}/','Cook\ProfileController@update');
    Route::get('availability','Cook\ProfileController@availability');
    Route::get('orders/','Cook\OrderController@index');
    Route::get('order/current/','Cook\OrderController@currentorder');
    Route::get('order/accepted/','Cook\OrderController@accepted');
    Route::get('order/delivered/','Cook\OrderController@delivered');
    Route::get('order/rejected/','Cook\OrderController@rejected');
    Route::get('order/accept/{id}','Cook\OrderController@accept');
    Route::get('order/inprogress/{id}','Cook\OrderController@inprogress');
    Route::get('order/shipped/{id}','Cook\OrderController@shipped');
    Route::get('order/reject/{id}','Cook\OrderController@reject');
    Route::get('dishes/{lang}','Cook\DishController@index');
    Route::post('dish/','Cook\DishController@status');
    // Route::put('dish/{id}','Cook\DishController@update');
    Route::post('withdraw/{lang}','Cook\WithdrawController@store');
    Route::get('withdraw/','Cook\WithdrawController@index');
    Route::get('wallet/','Cook\WithdrawController@wallet');
    Route::post('withdraw/cancel/{id}/{lang}','Cook\WithdrawController@cancel');
    Route::post('withdraw/delete/{id}/{lang}','Cook\WithdrawController@destroy');

});




