<?php

return [

  'ar'=>[
      'name'=>' الاسم باللغة العربية',
      'cook_id'=>' اسم الطباخ باللغة العربية',
      'files'=>'صورة الطبق',
      'cusine'=>'نوع الطبق',
      'category'=>'الصنف',
      'addons'=>'الاضافات',
      'section'=>'القسم',
      'price'=>'السعر',
      'allergens'=>'مسببات الحساسية',
      'ingredients'=>'المكونات الرئيسية',
      'portions_available '=>'حصة الفرد',
      'availability'=>'متاح',
      'available_count'=>'متاح',
      'description'=>'  الوصف باللغة العربية  ',
      'dishname'=>'   اسم الطبق باللغة العربية',
      'time_of_preparation'=>' وقت التحضير باللغة العربية',
      'termsandconditions'=>'الشروط و الاحكام باللغة العريبة ',

  ],



  'en'=>[
    'name'=>' الاسم باللغة الانجليزية',
    'cook_id'=>' cook name',
    'files'=>' dish picture',
    'cusine'=>' cusine',
    'category'=>'category',
    'addons'=>'addons',
    'section'=>'section',
    'price'=>'price',
    'allergens'=>'allergens ',
    'ingredients'=>' ingredients',
    'description'=>'description',
    'portions_available '=>' portions',
    'availability'=>'availability',
    'available_count'=>'available count ',
    'dishname'=>'اسم الطبق باللغة الانجليزية',
    'description'=>'    الوصف باللغة الانجليزية  ',
    'time_of_preparation'=>'  وقت التحضير باللغة الانجليزية',
    'termsandconditions'=>'الشروط و الاحكام باللغة الانجليزية ',


  ],
            //sidebar
      'Welcome'=>'مرحبا',
      'Dashboard'=>'الرئيسية',
      'Branches'=>'الفروع',
      'Sections'=>'الاقسام',
      'Admins'=>'المشرفون',
      'Catalog'=>'فهرس',
      'Cusine'=>'الطعام المطهو',
      'Sections'=>'الاقسام',
      'Allergen'=>'مسببات الحساسية',
      'Categories'=>'التصنيفات',
      'Vendors'=>'المطاعم',
      'Delivery charges'=>'رسوم التوصيل',
      'Coupons'=>'كوبونات',
      'Customers'=>'العملاء',
      'Cities'=>'المدن',
      'Settings'=>'الضبط',
      'VIP Customer'=>' العملاء المميزون ',
      'Driver Filter'=>'فلتر السائقين',
      'Tracking Drivers'=>'تتبع السائقين',
      'Account'=>'الحسابات',
      'Addons'=>'الاضافات',
      'Addons Section'=>'مجموعة الاضافات',
      'Orders'=>'طلبات',
      'Dishes'=>'اطباق',
      'Offers'=>'عروض',
      'Reports'=>'التقارير',
      "Restautant's Advs"=>"اعلانات المطاعم",
      'Feast'=>'العزومات',
      'Withdraw Request'=>'طلب سحب  ',
      'Support'=>'الدعم',
      'change pass'=>'تغيير كلمة المرور',


            //index
      'SAR'=>'دينار',
      'USD'=>'دولار امريكي',
      'Accepted Orders'=>'الطلبات المقبولة',
      'Total Profit'=>'اجمالي الربح',
      'Total Discount'=>'اجمالي الخصم',
      'Revenue'=>'الإيرادات',
        'Annual'=>'سنويا',
        'Monthly'=>'شهريا',



          //branches
        'List of Branches'=>'قائمة الفروع',
        'Branch'=>' الفروع',
        'Add Branch'=>'اضافة فرع',
        'Email'=>'البريد الالكتروني',


            //edit vendor
        'Edit Vendor'=>'تعديل مطعم',
        'Bank'=>'البنك',
        'Contract Number'=>' رقم العقد',
        'Credit Number'=>'رقم الائتمان',
        'Commission'=>'العمولة',
        'Pickup Commission'=>'عمولة التوصيل',
        'Taxes'=>'الضرائب',
        'Card Holder Name'=>'إسم صاحب البطاقة',

            //actions
        'Action'=>'الاكشن',
        'Add'=>'اضف',
        'Edit'=>'تعديل',
        'Delete'=>'حذف',
        'Deactive'=>'تعطيل',
        'Active'=>'فعال',
        'Save'=>'حفظ',
        'Delete'=>'حذف',
        'Close'=>'الغاء',
        'Are you sure want to delete?'=>'هل انت متأكد انك تريد الحذف ؟',
        'Cancel'=>'الغاء',
        'Decline'=>'تراجع',
        'Declined'=>'تم التارجع',


            //table header
        'Name'=>'الاسم',
        'Phone'=>'التليفون',
        'E-mail'=>'البريد الالكتروني',
        'City'=>'المدينة',
        'Status'=>'الحالة',
        'password'=>'الرقم السري',
        'Save Changes'=>'حفظ',
        'Vendor Name'=>'اسم المطعم',


            //category
        'List of categories'=>'قائمة التصنيفات',

            //sections
        'section'=>'الاقسام',
        'List of sections'=>'قائمة الاقسام',
        'From'=>'من',
        'To'=>'الي',
        'Add section'=>'اضف قسم',
        'select vendor'=>'اختر مطعم',
        'Edit sections'=>'تعديل القسم',



        //addon
        'List of addons'=>'قائمة الاضافات',
        'addon'=>'الاضافات',
        'Section'=>'الاقسام',
        'Category'=>'التصنيفات',
        'Price'=>'السعر',
        'Calories'=>'السعرات الحرارية',
        'Edit addons'=>'تعديل الاضافات',
        'Add addon'=>'اضف اضافة',


        //addon section
        'List of Addon sections'=>'قائمة مجموعة الاضافات',
        'addon section'=>'مجموعة الاضافات',
        'addon'=>'الاضافات',
        'section'=>'القسم',
        'Min'=>'الادني',
        'Condition'=>'الشرط',
        'max'=>' الاقصي',
        'optional'=>'اختياري',
        'required'=>'اجباري',
        'Edit addon section'=>'تعديل  مجموعة الاضافات',
        'Add Addon section'=>'اضف  مجموعة اضافة',


        //dish
        'List of Dishes'=>'قائمة الاطباق',
        'Dishes'=>'الاطباق',
        'Images'=>'الصور',
        'Vendor'=>'المطعم',
        'Dish Images'=>'صور الطبق',
        'dish List'=>'قائمة الاطباق',
        'start from'=>'يبدأ من  ',
        'Add Dish'=>'اضف طبق ',
        'Choose a file'=>'اختر صورة',
        'Cusines'=>'الطعام المطهو',
        'portions available'=>'الأجزاء المتاحة',
        'Large'=>'كبير',
        'Medium'=>'وسط',
        'Small'=>'صغير',
        'availability'=>'التوفر',
        'availabe'=>'متوفر',
        'not available'=>'غير متوفر',
        'select'=>'اختر',
        'persons'=>'فرد',
        'Edit Dish'=>'تعديل الطبق',
        'search'=>'بحث',



        //bank

        'The National Commercial Bank'=>'البنك الأهلي التجاري',
        'The Saudi British Bank'=>'البنك السعودي البريطاني',
        'Saudi Investment Bank'=>'البنك السعودي للاستثمار',
        'Alinma Bank'=>'مصرف الإنماء',
        'Banque Saudi Fransi'=>'البنك السعودي الفرنسي',
        'Riyad Bank'=>'بنك الرياض',
        'Samba Financial Group (Samba)'=>'مجموعة سامبا المالية (سامبا)',
        'Alawwal Bank'=>'البنك الأول',
        'Al Rajhi Bank'=>'مصرف الراجحي',
        'Arab National Bank'=>'البنك العربي الوطني',
        'Bank AlBilad'=>'بنك البلاد',
        'Bank AlJazira'=>'بنك الجزيرة',
        'Gulf International Bank Saudi Aribia (GIB-SA)'=>'بنك الخليج الدولي العربية السعودية (جي اي بي - اس ايه)',


        //header
        'My Profile'=>'الصفحة الشخصية',
        'Logout'=>'تسجيل الخروج',

        //withdraw
        'withdraw'=>'سحب',
        'List of withdraws'=>'قائمة عمليات السحب',
        'withdraws List'=>'قائمة السحب',
        'Amount'=>'المبلغ',
        'date and Time'=>'التاريخ والوقت',
        'Add withdraw'=>'إضافة سحب',
        'you have no money to make withdraw request'=>' ليس لديك أموال لتقديم طلب سحب  ',


        //order
        'Current Activities List'=>'قائمة الأنشطة الحالية',
        'orders List'=>'قائمة الطلبات ',
        'Order Number'=>'رقم الطلب',
        'Order Time'=>'وقت الطلب ',
        'Order Date'=>'تاريخ الطلب ',
        'Customer Name'=>'اسم العميل ',
        'Quantity'=>'المبلغ',
        'Delivery Type'=>'نوع التوصيل ',
        'Orders List'=>'قائمة الطلبات',
        'order'=>'طلب',
        'Accepted Orders'=>' الطلبات المقبولة',
        'late orders'=>' الطلبات المتأخرة',
        'on duty drivers'=>'  السائقون المناوبون',
        'Location'=>'الموقع',
        'Total Price'=>' السعر الإجمالي',
        'Payment Method'=>' طريقة الدفع',
        'Payment Status'=>' حالة الدفع',
        'Delivery Status'=>' حالة التسليم',
        'Order Status'=>'حالة الطلب',
        'Customer service'=>'خدمة العملاء',
        'old'=>'قديم',
        'new'=>'جديد',
        'Home'=>'المنزل  ',
        'Pickup'=>' Pickup',
        'Delivery'=>'Delivery',
        'View Details'=>'مشاهدة التفاصيل',
        'available drivers'=>'السائقين المتاحين ',
        'Pending'=>'معلق',
        'out of delivery'=>'خارج التسليم',
        'Accepted'=>'مقبول',
        'progress'=>'التقدم',
        'Shipped'=>'تم الشحن',
        'Delivered'=>'تم التسليم',
        'Completed'=>'مكتمل',
        'Rejected'=>'مرفوض',
        'unAssigned'=>'unAssigned',
        'Cache'=>' نقدي ',
        'Mada'=>'مدى',
        'Apple Pay'=>'Apple Pay',
        'Reject'=>'رفض',
        'Accept'=>'قبول',
        'Cancelled'=>'ملغاة',
        'Assigned'=>'Assigned',
        'Order Details'=>'تفاصيل الطلب',
        'Restaurant Name'=>'اسم المطعم',
        'View Details '=>'عرض التفاصيل  ',
        'Admin Note'=>'ملاحظة المشرف ',
        'Credit'=>'ائتمان',
        'Cash'=>'نقدي',
        'In progress'=>'جاري التحضير',
        "Customer's  Orders"=>" طلبات العميل",


        'support'=>'الدعم',
        'Type a message'=>'اكتب رسالتك',

        //cook profile
        'Profile of Cooks'=>' نبذة عن المطعم ',
        'Cook profile'=>'الصفحة الشخصية ',
        'Basic Information'=>' المعلومات الاساسية ',
        'Upload Photo'=>'تحميل الصورة ',
        'Allowed JPG, GIF or PNG. Max size of 2MB'=>'مسموح بتنسيق JPG أو GIF أو PNG. أقصى حجم 2 ميجا بايت',
        'Username'=>'اسم المستخدم',
        'Phone Number'=>'رقم الهاتف ',
        'Date of Birth'=>'تاريخ الميلاد',
        'Address'=>'العنوان',
        'schedule'=>'جدول المواعيد',
        'Sat'=>'السبت',
        'Sun'=>'الاحد',
        'Mon'=>'الاثنين',
        'Tue'=>'الثلاثاء',
        'Wed'=>'الاربعاء',
        'Thu'=>'الخميس',
        'Fri'=>'الجمعة',
        'Holiday'=>'اجازة',
        'To'=>'من',
        'From'=>'الى',
        'Status'=>'الحالة',
        'About Me'=>'نبذة عني  ',
        'info'=>'معلومات',
        'terms and conditions'=>'  الشروط والأحكام',
        'by submit that mean you are  agree with the terms and conditions'=>'يعني إرسال هذا أنك توافق على البنود والشروط',



         //reports
         'List of reports'=>'قائمة التقارير',
         'reports'=>'التقارير',
         'Reports List'=>'قائمة التقارير',
         'Restaurant'=>'المطعم',
         'Customer'=>'العميل',
         'Date'=>'التاريخ',
         'Notes'=>'الملاحظات',
         'Tax'=>'الضرائب',
         'Delivery Fees'=>'رسوم التوصيل',
         'Total price'=>'السعر الإجمالي',
         'Driver Name'=>'اسم السائق',
         'Admin notes'=>'ملاحظات المشرف',


         //change pass
        'change password'=>'تغيير كلمة المرور',
        'Current Password'=>'كلمة المرور الحالية',
        'New Password'=>'كلمة المرور الجديدة',
        'Password Confirmation'=>'تاكيد كلمة المرور',

    ];
