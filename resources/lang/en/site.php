<?php

return [

  'ar'=>[
      'name'=>'name in arabic',
      'dishname'=>'dish name in Arabic',
      'time_of_preparation'=>'time of preparation in Arabic',
      'description'=>'description in Arabic',
      'termsandconditions'=>'terms and conditions in arabic',

  ],



  'en'=>[
    'name'=>'name in english',
    'dishname'=>'dish name in English',
    'time_of_preparation'=>'time of preparation in English',
    'description'=>'description in English',
    'termsandconditions'=>'terms and conditions in english',

  ],
            //sidebar
    'Welcome'=>'Welcome',
    'Dashboard'=>'Dashboard',
    'Branches'=>'Branches',
    'Sections'=>'Sections',
    'Admins'=>'Admins',
    'Catalog'=>'Catalog',
    'Cusine'=>'Cusine',
    'Sections'=>'Sections',
    'Allergen'=>'Allergen',
    'Categories'=>'Categories',
    'Vendors'=>'Vendors',
    'Delivery charges'=>'Delivery charges ',
    'Coupons'=>'Coupons',
    'Customers'=>'Customers',
    'Cities'=>'Cities',
    'Settings'=>'Settings',
    'VIP Customer'=>' VIP Customer  ',
    'Driver Filter'=>'Driver Filter ',
    'Tracking Drivers'=>'Tracking Drivers ',
    'Account'=>'Account',
    'Addons'=>'Addons',
    'Addons Section'=>'Addons Groups ',
    'Orders'=>'Orders',
    'Dishes'=>'Dishes',
    'Offers'=>'Offers',
    'Reports'=>'Reports',
    "Restautant's Advs"=>"Restautant's Advs ",
    'Feast'=>'Feast',
    'Withdraw Request'=>'Withdraw Request',
    'Support'=>'Support',
    'change pass'=>'Change Password',

            //index
    'SAR'=>'SAR',
    'USD'=>'USD',
    'Accepted Orders'=>'Accepted Orders',
    'Total Profit'=>'Total Profit',
    'Total Discount'=>'Total Discount',
    'Revenue'=>'Revenue',
    'Annual'=>'Annual',
    'Monthly'=>'Monthly',

          //branches

    'List of Branches'=>'List of Branches',
    'Branch'=>' Branches',
    'Add Branch'=>'Add Branch',
    'Email'=>'Email',



    //edit vendor
    'Edit Vendor'=>'Edit Vendor ',
    'Bank'=>'Bank',
    'Contract Number'=>' Contract Number ',
    'Credit Number'=>'Credit Number ',
    'Commission'=>'Commission',
    'Pickup Commission'=>'Pickup Commission ',
    'Taxes'=>'Taxes',
    'Card Holder Name'=>'Card Holder Name  ',


    //actions
    'Action'=>'Action',
    'Add'=>'Add',
    'Edit'=>'Edit',
    'Delete'=>'Delete',
    'Deactive'=>'Deactive',
    'Active'=>'Active',
    'Save'=>'Save',
    'Delete'=>'Delete',
    'Close'=>'Close',
    'Cancel'=>'Cancel',
    'Are you sure want to delete?'=>'Are you sure want to delete?',
    'Decline'=>'Decline',
    'Declined'=>'Declined',


        //table header
    'Name'=>'Name',
    'Phone'=>'Phone',
    'E-mail'=>'E-mail ',
    'City'=>'City',
    'Status'=>'Status',
    'password'=>'password ',
    'Save Changes'=>'Save',
    'Vendor Name'=>'Vendor Name ',
    'Restaurant Name'=>'Restaurant Name',



        //category
        'List of categories'=>'List of categories ',

        //sections
        'section'=>'section',
        'List of sections'=>' List of sections',
        'From'=>'From',
        'To'=>'To',
        'Add section'=>' Add section',
        'select vendor'=>'select vendor ',
        'Edit sections'=>' Edit sections',



        //addon
        'List of addons'=>'List of addons ',
        'addon'=>'addons',
        'Section'=>'Section',
        'Category'=>'Category',
        'Price'=>'Price',
        'Calories'=>'Calories ',
        'Edit addons'=>'Edit addons',



         //addon section
         'List of Addon sections'=>'List of Addon sections',
         'addon section'=>'addon sections',
         'addon'=>'addons',
         'Section'=>'Section',
         'Condition'=>'Condition',
         'max'=>'max',
         'Min'=>'Min ',
         'optional'=>'optional',
         'required'=>'required',
         'Edit addon section'=>'Edit addon section',
         'Add Addon section'=>'Add Addon section',



        //dish
        'List of Dishes'=>'List of Dishes',
        'Dishes'=>'Dishes',
        'Images'=>'Images',
        'Vendor'=>'Vendor',
        'Dish Images'=>'Dish Images',
        'dish List'=>'dish List',
        'start from'=>'start from',
        'Add Dish'=>'Add Dish',
        'Choose a file'=>'Choose a file',
        'Cusines'=>'Cusines',
        'portions available'=>'portions available',
        'Large'=>'Large',
        'Medium'=>'Medium',
        'Small'=>'Small',
        'availability'=>'availability',
        'availabe'=>'availabe',
        'not available'=>'not available',
        'persons'=>'persons',
        'Edit Dish'=>'Edit Dish ',
        'search'=>'Search',




        //bank

        'The National Commercial Bank'=>'The National Commercial Bank',
        'The Saudi British Bank'=>'The Saudi British Bank',
        'Saudi Investment Bank'=>'Saudi Investment Bank',
        'Alinma Bank'=>'Alinma Bank',
        'Banque Saudi Fransi'=>'Banque Saudi Fransi',
        'Riyad Bank'=>'Riyad Bank',
        'Samba Financial Group (Samba)'=>'Samba Financial Group (Samba)',
        'Alawwal Bank'=>'Alawwal Bank',
        'Al Rajhi Bank'=>'Al Rajhi Bank',
        'Arab National Bank'=>'Arab National Bank',
        'Bank AlBilad'=>'Bank AlBilad',
        'Bank AlJazira'=>'Bank AlJazira',
        'Gulf International Bank Saudi Aribia (GIB-SA)'=>'Gulf International Bank Saudi Aribia (GIB-SA)',



        //header
        'My Profile'=>'My Profile',
        'Logout'=>'Logout',

        //withdraw
        'withdraw'=>'withdraw',
        'List of withdraws'=>'List of withdraws',
        'withdraws List'=>'withdraws List',
        'Amount'=>'Amount',
        'date and Time'=>'date and Time',
        'Add withdraw'=>'Add withdraw',
        'you have no money to make withdraw request'=>'you have no money to make withdraw request',



        //order
        'Current Activities List'=>'Current Activities List',
        'orders List'=>'orders List',
        'Orders List'=>'Orders List',
        'order'=>'order',
        'Accepted Orders'=>'Accepted Orders',
        'late orders'=>'late orders',
        'on duty drivers'=>'on duty drivers',
        'Location'=>'Location',
        'Total Price'=>'Total Price',
        'Payment Method'=>'Payment Method',
        'Payment Status'=>'Payment Status',
        'Delivery Status'=>'Delivery Status',
        'Order Status'=>'Order Status',
        'Customer service'=>'Customer service',
        'old'=>'old',
        'new'=>'new',
        'Home'=>'Home',
        'Pickup'=>'Pickup',
        'Delivery'=>'Delivery',
        'View Details'=>'View Details',
        'available drivers'=>'available drivers',
        'Pending'=>'Pending',
        'out of delivery'=>'out of delivery',
        'Pending'=>'Pending',
        'Accepted'=>'Accepted',
        'progress'=>'Progress',
        'Shipped'=>'Shipped',
        'Delivered'=>'Delivered',
        'Completed'=>'Completed',
        'Rejected'=>'Rejected',
        'unAssigned'=>'unAssigned',
        'Cache'=>'Cache',
        'Mada'=>'Mada',
        'Apple Pay'=>'Apple Pay',
        'Reject'=>'Reject',
        'Accept'=>'Accept',
        'Cancelled'=>'Cancelled',
        'Assigned'=>'Assigned',
        'unAssigned'=>'UnAssigned',
        'Order Details'=>'Order Details',
        'Order Number'=>'Number',
        'Order Time'=>'Order Time',
        'Order Date'=>'Order Date',
        'Customer Name'=>'Customer Name',
        'Quantity'=>'Quantity',
        'Delivery Type'=>'Delivery Type',
        'View Details '=>'View Details ',
        'Admin Note'=>'Admin Note',
        'Credit'=>'Credit',
        'Cash'=>'Cash',
        'In progress'=>'In progress',
        "Customer's  Orders"=>"Customer's  Orders",


        'support'=>'support',
        'Type a message'=>'Type a message',



        //cook profile
        'Profile of Cooks'=>'Profile of Vendors',
        'Cook profile'=>'Vendor profile',
        'Basic Information'=>'Basic Information',
        'Upload Photo'=>'Upload Photo',
        'Allowed JPG, GIF or PNG. Max size of 2MB'=>'Allowed JPG, GIF or PNG. Max size of 2MB',
        'Username'=>'Username',
        'Phone Number'=>'Phone Number',
        'Date of Birth'=>'Date of Birth',
        'Address'=>'Address',
        'schedule'=>'Schedule',
        'Holiday'=>'Holiday',
        'To'=>'To',
        'From'=>'From',
        'Sat'=>'Sat',
        'Sun'=>'Sun',
        'Mon'=>'Mon',
        'Tue'=>'Tue',
        'Wed'=>'Wed',
        'Thu'=>'Thu',
        'Fri'=>'Fri',
        'About Me'=>'About Me',
        'Status'=>'Status',
        'info'=>'Info',
        'terms and conditions'=>'terms and conditions',
        'by submit that mean you are agree with the terms and conditions'=>'by submit that mean you are agree with the terms and conditions',




        //reports
        'List of reports'=>'List of reports',
        'reports'=>'Reports',
        'Reports List'=>'Reports List',
        'Restaurant'=>'Restaurant',
        'Customer'=>'Customer',
        'Date'=>'Date',
        'Notes'=>'Notes',
        'Tax'=>'Tax',
        'Delivery Fees'=>'Delivery Fees',
        'Total price'=>'Total price',
        'Driver Name'=>'Driver Name',
        'Admin notes'=>'Admin notes',

        //change pass
        'change password'=>'Change Password',
        'Current Password'=>'Current Password',
        'New Password'=>'New Password',
        'Password Confirmation'=>'Password Confirmation',

    ];
