@extends('backend.layouts.app')


@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Terms And Conditions</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Terms And Conditions</li>
                    </ul>
                </div>
                @if (auth()->guard('admin')->user()->hasRole('cook'))
                <div class="col-sm-12 col">
                    <a href="#Add_withdraw" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >Add</a>
                </div>
               @endif
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">
                <form action="" method="POST">
                    <span>tems and conditions in Arabic</span>
                    <textarea name="terms"  cols="30" rows="10">مرحبا بكم في اطبخلي
                        يرد فيما يلي الشروط والاحكام التي تسري بشأن عملية استخدام منصة "اطبخلي " وكذا كل ما يرتبط به مواقع وخدمات، علما بأن استخدام المنصة الالكتروني "اطبخلي" إذ يعتبر بمثابة موافقة من جانبكم على الشروط والاحكام الماثلة. وإذا لم توفقوا على هذه الشروط والاحكام، فيكون عليكم عدم الدخول على المنصة أو استخدامه.
                        تسري الشروط والاحكام التالي بيانها على جميع المُستخدمين، وذلك فيما يتعلق بالخدمات التي يقدمها اطبخلي من خلال المنصة الالكتروني "اطبخلي ". وتسري الشروط والاحكام الماثلة وتنفذ بمجرد قبولكم لأية خدمة تجري عبر المنصة الالكتروني أو من خلال أية وسيلة تواصل أخرى مع اطبخلي .
                        ونرجو الإحاطة بأنه ينبغي مُراجعة سياسة الخصوصية السارية لدينا على (رابط سياسة الخصوصية) والمُوافقة عليها باعتبارها جزءًا لا يتجزأ من هذه الشروط والاحكام.
                        يُـوافق المستخدم بل ويقبل بأن يكون استخدام المنصة الالكتروني والخدمات التي يقدمها اطبخلي استخداما يسري على مسئوليته الخاصة، كما يُـقر أيضا بأن اطبخلي إذ يبرأ بل ويُعفي من أية إقراراتٍ أو ضماناتٍ من أي نوع، سواءً صدرت صراحة أو ضمنا.
                        الصلاحية

                        حتى نضمن ان المستخدمين لديهم القدرة على ابرام عقود قانونية مُـلزمة، فلا يتم منح صلاحية استخدام المنصة للأشخاص ممن تقل أعمارهم عن الثمانية عشر عاما أو عن السن القانونية السارية في منطقتكم، أيهما أعلى.
                        وإذا قمتم بالتسجيل بصفتكم جهة عمل، فيكون عليكم الإقرار بأن لديكم الصلاحية اللازمة لإلزام الكيان الخاص بكم بالشروط والاحكام الماثلة، وأنكم وكذا كيان العمل ستلتزمون بكل ما هو واجب التطبيق من قوانين واشتراطاتٍ تسري على - وتخضع لها- عملية استخدام المنصة الالكتروني.
                        الحسابات والتسجيل

                        تستلزم منكم عملية تسجيل الحساب أن تتقدموا إلى اطبخلي بمعلوماتٍ مُعينة، ومنها على وجه العموم لا الحصراسم شركة او شخص والعنوان ورقم السجل التجاري او الرقم القومي ورقم الهاتف. ويكون عليكم الموافقة على أن يتم حفظ معلوماتٍ صحيحة ودقيقة وكاملة بل ومُحدثة بشأنكم على الحساب. كما يكون عليكم أن تتحملوا المسئولية عن كل نشاطٍ يجري في إطار حسابكم، كما تلتزمون بالموافقة على الحفاظ وباستمرار على سريان إجراءاتٍ امنية على اسم المستخدم وكلمة المرور الخاصة بحسابكم، هذا ما لم تصرح اطبخلي بغير ذلك كتابة.
                        وحيث أنه لا يجوز نقل أو حوالة حسابات المستخدم، فلهذا يكون عليكم الموافقة على أن تردوا إلى اطبخلي قيمة أية عملية استخدام لحسابكم، ولم تكن عملية صحيحة أو مُصرح بها أو تسري وفق القانون، وحدث ذلك من جانبكم أو من جانب أي شخص يمكنه الدخول على المنصة الالكتروني أو الخدمات أو خلافه من خلال استخدام اسم المستخدم أو كلمة المرور المحددة من جانبكم، بل وسواء سمحتم بذلك الدخول أم لا.
                        وإذا نما لدى اطبخلي (وحسب مطلق اختيارها) ثمة شكوك بأن أية معلومة من المعلومات التي تقدمتم بها هي معلومة غير صحيحة أو غير دقيقة أو غير كاملة أو غير محدثة، فدونما إخلال بأية حقوق أخرى وتعويضاتٍ مكفولة اطبخلي وفق الشروط والاحكام الماثلة أو بمقتضى القوانين واجبة التطبيق، فيكون لدينا الحق في إيقاف عملية دخولكم على المنصة الالكتروني أو خدماته، أو تحديد هذا الدخول أو قصره.
                        ويجوز اطبخلي القيام (وحسب مطلق اختيارها بل وفي أي حين) بإبداء أية استفسارات تراها ضرورية (سواء تم ذلك بصورة مباشرة أو من خلال طرف من الغير)، وكذا مطالبتكم بتقديم الرد وبمزيد من المعلومات أو المستندات، ومنها على وجه العموم لا الحصر معلوماتٍ ومستندات للتحقق من هويتكم و/أو ملكيتكم لسنداتكم المالية. ودونما تقييد لما سلف بيانه، فإذا كنتم بمثابة جهة عمل أو مُسجلين بالنيابة عن جهة عمل، فيجوز أن تشمل هذه المعلومات أو المستندات الترخيص التجاري وغير ذلك من مستنداتٍ رسمية خاصة بالشركة و/أو مستنداتٍ تستعرض أية صلاحية مكفولة لأي شخص للعمل نيابة عنكم. ويكون عليكم الموافقة على توفير أية معلوماتٍ و/أو مستندات إلى اطبخلي فور طلبها إياها. كما يكون عليكم الإقرار والموافقة على أنه إذا تعذر عليكم القيام بذلك، فيجوز اطبخلي القيام ودونما أية مسئولية عليه بتقييد أو إيقاف أو سحب عملية دخولكم على المنصة الالكتروني. كما نحتفظ أيضا بالحق في الغاء أية حساباتٍ لم يرد تأكيد بشأنها أو لم يتم التحقق منها أو أية حساباتٍ لم تكن مفعلة لمدة طويلة.
                        وعند إتمام عملية التسجيل من جانبكم، يكون عليكم الإقرار بأنكم اطلعتم على الشروط والاحكام الماثلة بل وتفهمتموها ووافقتم على الالتزام بها، وكذا سياسة الخصوصية بل وكل ما يطرأ عليها من تعديل من حين لآخر، حيث تعتبر السياسة وتعديلاتها - وبموجب هذا المحرر- بمثابة جزء لا يتجزأ بل وتندرج ضمن الشروط والاحكام الماثلة.
                        الخدمات

                        يقدم اطبخلي خدمات عرض وتوصيل الوجبات الغذائية التي يقوم بعرضها وإعدادها طباخين و/ او اشخاص مستقلين.

                        حيث يقوم الطباخين المتواجدين على المنصة بعرض المأكولات والوجبات التي يقوم بإعدادها ومدى توافرها والكميات المتاحة لمستخدمي المنصة وذلك حتى يتمكن مستخدمي المنصة من شراء وطلب تلك الوجبات

                        عمليات التواصل الالكتروني

                        عند انشاء حساب، يكون عليكم الموافقة على أنكم ستتواصلون معنا الكترونيا. وبالتالي، عليكم الموافقة على استلام رسائل دورية من جانبنا. وسوف يتواصل معكم اطبخلي عبر البريد الالكتروني أو قد يرسل لكم معلوماتٍ عبر رسائل نصية قصيرة، وكذا القيام بنشر إخطاراتٍ على المنصة الالكتروني كجزء من إدارة الاعمال المعتادة والتي ترتبط بعملية استخدامكم للخدمات. ويكون عليكم الإقرار بأن اختياركم عدم تطبيق وسائل التواصل المذكورة هو امر من شأنه إحداث ثمة تأثير على عملية استخدامكم للخدمات.
                        كما يكون عليكم الموافقة على أن تكون جميع الاتفاقات والاخطارات والإفصاحات وغير ذلك من مراسلاتٍ مما نقوم بتقديمه اليكم الكترونيا هي مراسلاتٍ تستوفي أي اشتراط قانوني يستلزم تقديم تلك المراسلات على نحو كتابي.
                        كما سيطلب منكم اطبخلي خلال عملية التسجيل الموافقة على ان نرسل لكم رسائل بريد الكتروني أو إخطاراتٍ دعائية ترتبط بالمنصة الالكتروني وما به من خدمات. وإذا حدث في أي حين أن قررتم أو ابديتم عدم الرغبة في استلام رسائل البريد الالكتروني الدعائية، فيمكنكم اختيار عدم استلامها من خلال الضغط على الرابط الكائن في اسفل أية رسالة بريد الكتروني دعائية.
                        حقوق التأليف والعلامات التجارية

                        بالنسبة لجميع المحتوى الوارد على المنصة الالكتروني، ومن ذلك على وجه العموم لا الحصر النص، وصور الجرافيك، وشعارات اطبخلي والازرار، والصور، والمقاطع الصوتية، وعمليات التحميل الرقمي، وتجميع البيانات، والبرامج الالكترونية، فيعتبر ذلك المحتوى بمثابة ملكية بل وعمل تأليفي خاص باطبخلي أو بمرخصيه. كما يخضع هذا المحتوى للحماية بموجب حق المؤلف وحقوق العلامات التجارية وبراءات الاختراع وغير ذلك من حقوق ملكية فكرية وقوانين. كما يكون المحتوى المجمع على المنصة الالكتروني بمثابة ملكية حصرية لاطبخلي بل وحق تأليف لها، بل ويخضع للحماية بموجب حق المؤلف وحقوق العلامات التجارية وبراءات الاختراع وغير ذلك من حقوق ملكية فكرية وقوانين.
                        وتعتبر علامة اطبخلي وما يرتبط بها من رموز هي علاماتٍ تجارية خاصة اطبخلي ، علما بأنه لا يجوز استخدام العلامات التجارية لاطبخلي فيما يتعلق بأي منتج أو أية خدمة ليست معروضة على اطبخلي ، أو أي منتج أو خدمة من شأنها التقليل من شأن اطبخلي أو الإساءة لسمعتها.
                        استخدام المنصة الالكتروني

                        نظير التزام المستخدم بالشروط والاحكام الماثلة، تمنح اطبخلي إلى المستخدم ترخيصًا محدودا وغير حصري ولا يجوز منحه من الباطن وغير قابل للإلغاء أو الإبطال بل وغير قابل للحوالة أو النقل فيما يتعلق بما يلي:
                        - الدخول على الخدمات واستخدامها فيما يتعلق بعملية تقديم الخدمات وحسب.
                        - والدخول على أي محتوى واستخدامه وكذا أية معلومات وما يرتبط بها من مواد قد تتوفر من خلال الخدمات.
                        علما بأن أي حق لم يتم منحه على نحو صريح في هذه الشروط هو حق محفوظ لاطبخلي ، بل ويجوز الغائه أو ابطاله في أي حين بل ودونما أي إخطارٍ بذلك إلى العميل.
                        ويحظر على العميل استخدام أية علامة تجارية أو أي حقوق ملكية فكرية تخص اطبخلي ، وذلك من أي جزء من الخدمات، كما يحظر عليه إعادة إنتاج الخدمات أو تعديلها أو إجراء أية عملية اعداد أو نشر أو تنفيذ أو ارسال أو عرض أو بث أو غير ذلك من وسائل استغلال الخدمات، هذا ما لم يسمح اطبخلي بذلك أو يأذن صراحة وبصورة كتابية، كما لا يجوز إجراء اعمال تفكيك لنظام الخدمات أو أية هندسية عكسية أو إعادة تشكيل لها، أو القيام بربط أي جزء من الخدمات بأي نظام عاكس أو تأطيره أو وضع أية برامج أو نصوص لغرض إزالة أي جزء من الخدمات أو فهرستها أو مسحها أو إجراء غير ذلك من عمليات استخراج للبيانات منه، أو القيام بتحميل عملية التشغيل و/ أو العمل الوظيفي لأي منحى من مناحي الخدمات بأحمال لا مبرر لها أو القيام بإعاقتها أو محاولة الحصول على إمكانية الدخول عليها دونما تصريح بذلك أو العمل على الإضرار بالخدمات أو ما يرتبط بها من أنظمة أو شبكات. أية
                        ولا يجوز للمستخدم القيام في قسم مراجعة الوجبة (الخاص بالوجبات المقيدة) أو في أي موضع آخر بالمنصة (وذلك حسبما يتقرر وحسب مطلق اختيارنا) بأية عملية نشر للآتي: أية دعاية لغرض الاستقطاب أو أية إعلانات أو استخدام لغة مبتذلة أو مواد تشوبها البذاءة أو تحتوي على سباب أو تجاوزات من الناحية الثقافية أو الدينية أو أي محتوى سياسي نقدي أو مواد من شأنها إحداث تهديد للصالح العام أو الامن القومي أو التسبب في إساءة السمعة أو القذف أو التشهير أو أي محتوى آخر قد تشوبه الإهانة أو البذاءة أو عدم اللياقة.
                        قوائم الوجبات ومدى توافرها

                        تستلزم الشروط المتفق عليها مع مختلف الطباخين لدينا أن يضمنوا أن الوجبات والاكلات تمتاز بالجودة المطلوبة وتحوز رضائكم.
                        تعتبر المعلومات الواردة في المنصة الالكتروني هي معلومات معروضة لأغراض عامة فقط. أما بالنسبة للمعلومات التي ترد من جانب اطبخلي ، فنحن نبذل بشأنها جهودنا للعمل على أن تظل المعلومات معلوماتٍ محدثة وصحيحة، إلا أننا لا نتقدم بأية إقراراتٍ أو ضمانات من أي نوع، سواء على نحو شفهي أو ضمني، وذلك من حيث اكتمال أو دقة أو مصداقية أو ملاءمة أو توافر المنصة الالكتروني أو المعلومات أو الخدمات أو ما يرتبط بها من رسوم جرافيك واردة على المنصة الالكتروني بالنسبة لأي غرض. وبالتالي يكون اعتمادكم على تلك المعلومات هو أمر يقع على مسئوليتكم الخاصة تماما.
                        وستعمل اطبخلي دائما على أن تعرض عليكم أفضل خبرات العملاء بل ومستوى متميز من الخدمة، حيث تبذل اطبخلي قصارى جهودها لتضمن أن منتجات الطباخ منتجاتٍ متاحة ومتوفرة، إلا أن اطبخلي لا تتحمل أيما مسئولية حال عدم توافر أية وجبات لدى أي طباخ وكانت هذه الوجبات معروضة على المنصة الالكتروني. وإذا كان الطعام او الوجبة الذي تقدمتم بطلبية بشأنه غير متوفر، فسوف نحيطكم علما بذلك بأسرع ما يمكن، كما سنمنحكم فرصة اختيار بدائل أو استرداد المبالغ الخاصة بذلك الوجبة أو تلك الوجبات التي لم تكن متاحة أو متوفرة.
                        ضمانات الوجبة

                        تعتبر الوجبات المُباعة من خلال المنصة الإلكتروني الماثل والتي لا تحمل الاسم التجاري لاطبخلي تكون مسئولية الطباخين، وذلك وفق الشروط والاحكام المرتبطة بكل منتج. كما لا تمنح اطبخلي أي ضمان من أي نوع بالنسبة للوجبات المباعة على المنصة الالكتروني. ولهذا يُرجى التواصل مع اطبخلي عند الحاجة للدعم الفني أو خدمة العملاء فيما يتعلق بمنتجاتهم.
                        المدفوعات

                        توفر اطبخلي وسائل سداد مختلفة على المنصة الالكتروني، ومن ضمن هذه الوسائل ما يلي:
                        -  السداد نقدا عند التسليم.
                        - السداد الكترونيا من خلال بطاقات ائتمان.
                        ونرجو الإحاطة بأن السداد عند التسليم وبأية وسيلة هو سداد يقتصر على مبالغ معينة.
                        وتحتفظ اطبخلي بالحق في إجراء أي تغيير أو تعديل في وسيلة السداد أو عملية توفيرها.
                        يوفر اطبخلي خدمات سداد الكتروني من خلال مقدمي خدمة من الغير، علما بأن اطبخلي لن يقوم بتخزين أية بياناتٍ ترتبط ببطاقات الائتمان. ولا يتحمل اطبخلي أيما مسئولية عن أي خطأ أو ضرر أو أي أمر مثيل قد يتعرض له المستخدم من واقع أو جراء استخدام وسيلة السداد الالكتروني، حيث يقع ذلك على مسئولية مقدم الخدمة من الغير.
                        ويكون على المستخدم أن يضمن وجود تغطية مالية كافية في الحساب ذو الصلة، بحيث يُمكن تغطية الطلبيات، كما يكون عليه الالتزام بحدود معاملات بطاقة الائتمان.
                        ولا تفرض اطبخلي أية رسوم إضافية على المستخدمين من واقع إجراء عملية السداد الكترونيا.
                        ويجوز لاطبخلي القيام (وحسب مطلق اختياره بل وفي أي حين) بإبداء أية استفساراتٍ يراها ضرورية (وذلك سوءا بصورة مباشرة أو من خلال الغير)، كما يجوز له أن يطلب منكم موافاته بالمزيد من المعلومات أو المستندات، ومن ذلك على وجه العموم لا الحصر أية معلومات أو مستندات تلزم للتحقق من هويتكم و/أو ملكيتكم للسندات المالية. ويكون عليكم بالتالي الموافقة على توفير أية معلوماتٍ و/أو مستنداتٍ إلى اطبخلي فور طلبه إياها. كما يكون عليكم الإقرار والموافقة على أنه إذا تعذر عليكم ذلك، فيجوز لاطبخلي القيام - ودونما أية مسئولية عليها- بإلغاء المعاملة أو تقييد أو إيقاف إمكانية دخولكم على المنصة الالكتروني و/أو سحب عضويتكم منه.
                        ودونما أية مسئولية على اطبخلي ، فإذا حدث وأن كانت أسعار الوجبات الخاصة بالطباخين، والتي يتم عرضها على المنصة الالكتروني، اسعارًا تختلف عن الأسعار الفعلية الواردة في الايصال النهائي. وفي هذه الًحالة، يتم اخطار المستخدم بذلك، ويكون للمستخدم الحق في سداد السعر الفعلي أو الغاء الطلبية.
                        التسليم

                        بالنسبة للطلبيات التي ترد من خلال المنصة الالكتروني، فيتم تسليمها إلى المستخدم من خلال المنصة الالكترونية، وذلك خلال الموعد الذي يتحدد عند المغادرة. ويتم إعادة التأكيد على موعد التسليم الفعلي بطريق الهاتف، وذلك على رقم الهاتف الذي يرد من جانبكم في سند المغادرة. كما سيُطلب منكم أيضا إعادة التأكيد على العنوان الخاص بالتسليم أو الادلاء بمزيد من الايضاح بشأنه.
                        وفي حالة وقوع أي تأخير في عملية التسليم، فسيقوم اطبخلي  بإخطار المستخدمين بذلك مع إجراء الترتيبات بشأن الميعاد المتوقع للتسليم أو إحاطتهم به.
                        وفي حالة وقوع أي ضرر بالوجبة (او الوجبات) خلال عملية التسليم، وحدث ذلك قبل الاستلام من جانب المستخدم، فستقوم الطباخ  بإحاطة المستخدم بذلك الضرر مع تسليمه وجبه أخرى بنفس الموصفات دون أية رسوم إضافية، إن كانت الوجبه متاحًا. أما إذا لم يكن الوجبة متاحه فسيقوم الطباخ برد قيمة شراء الوجبة (الوجبات) إلى المستخدم، وهي القيمة التي تحددت في تاريخ الشراء حين سدادها في المنصة.
                        ويتم تسليم الطلبيات مباشرة من خلال فريق عمل اطبخلي أو فريق خدمة من جانب الغير، أو من خلال أعضاء من هذين الفريقين.
                        خدمات الغير

                        قد يتم توفير الخدمة أو الدخول عليها من خلال مقدمي خدمة من الغير وكذا عبر محتوى (ومن ذلك أي محتوى إعلاني) لا يخضع لنطاق سيطرة أو تحكم اطبخلي . ولهذا يكون عليكم الإقرار بأن هناك شروط استخدام مختلفة بل وسياسات خصوصية أخرى قد تسري عليكم حين استخدام خدمات ذلك الغير وذلك المحتوى. إلا أن اطبخلي لا تمنح أيما تصديق على خدمات الغير أو ما يخصهم من محتوى، بل ولا يتحمل اطبخلي أية مسئولية أو التزام من أي نوع عن أية خدمة من تلك الخدمات الخاصة بمقدمي الخدمة من الغير.
                        المردودات والتبديلات والمستردات المالية

                        يتم تطبيق القانون المصري لحماية المستهلك، بالأضافة الي انه يمكن لمستخدم المنصة إلغاء -العدول عن الطلب- طلب الوجبة خلال 5 دقائق فقط من طلب الوجبة،
                        كما يقوم كل طباخ بعرض سياسة الاستبدال الخاصة به وعلى المستخدم الالتزام بسياسة الاستبدال والاسترجاع الخاصة بالطباخ طالما انها لم تخالف قانون حماية المستهلك، وذلك دون أدني مسئولية على اطبخلي


                        إن اطبخلي تتعامل مع رضا العملاء بمنتهى الجدية. وفي حالة وجود مشاكل مع طلب أي وجبة، يرجى الاتصال بـاطبخلي عبر المحادثة المباشرة لدينا أو بالاتصال بنا عبر الهاتف وسوف نقوم بمساعدتك ونحاول حل المشكلة، ، سوف تقوم اطبخلي برد كامل او جزء من المبلغ طبقا للحالات التالية: في حالة عدم حصولك على الطلب او استلام طلب غير صحيح، يجوز اصدار رد المبلغ كاملا لك، وفي حالة نقص جزء من طلبك، سوف نقوم باصدار رد جزء من المبلغ، وفي جميع الحالات، سوف نبذل قصارى جهدنا لضمان رضائك عن خدمة اطبخلي.

                        السرية والخصوصية

                        لا يجوز للمستخدم (المستخدمين) الإفصاح للغير عن أية معلوماتٍ ترد اليه بموجب عقد خدمة مُـبرم مع اطبخلي ، بل وينبغي أن يتم الدخول على أية معلوماتٍ ترتبط بأعمال اطبخلي على نحو تشمله السرية، وذلك في الحدود التي قد تؤثر سلبا على أعمال اطبخلي . ويتحمل المستخدم (المستخدمين) المسئولية عن تعويض اطبخلي عن أية خسارة في العمل أو في السمعة، إن حدث ذلك جراء أي تصرف يقع من المستخدم (المستخدمين).
                        الابراء من المسئولية

                        يتم تقديم الخدمات "كما هي" معروضة و"حسبما هو متاح" منها. وتعفى اطبخلي بل وتبرأ من كافة الاقرارات والضمانات الصريحة منها أو الضمنية أو القانونية التي لم يرد نصها صراحة في الشروط الماثلة، ومن ذلك الضماناتٍ المعروضة ضمنا بشأن قدرة الطباخ وملاءمة خدماته لغرض معين وعدم اخلاله. كما لا يتقدم اطبخلي بأي إقرار أو ضمان بشأن مصداقية الخدمات أو دقة مواعيدها أو جودتها أو استدامتها أو توافرها هي أو أية خدمات تطلب من خلال استخدام خدمات اطبخلي ، كما لا تضمن اطبخلي عدم تعرض الخدمات للتعطيل أو خلوها من الخطأ. ولا تمنح اطبخلي أيما ضمان بشأن جودة الطباخين وملاءمتهم وقدراتهم. ولهذا يكون عليكم الموافقة على تحملكم حصرا كامل المسئولية الناشئة عن استخدامكم الخدمات، وذلك إلى أقصى حد يسمح به القانون واجب التطبيق.
                        وتعتبر المعلومات الواردة في المنصة الالكتروني هي معلومات معروضة لأغراض عامة فقط. أما بالنسبة للمعلومات التي ترد من جانب اطبخلي ، فنحن نبذل بشأنها جهودنا للعمل على أن تظل المعلومات معلوماتٍ محدثة وصحيحة، إلا أننا لا نتقدم بأية إقراراتٍ أو ضمانات من أي نوع، سواء على نحو شفهي أو ضمني، وذلك من حيث اكتمال أو دقة أو مصداقية أو ملاءمة أو توافر المنصة الالكتروني أو المعلومات أو الخدمات أو ما يرتبط بها من رسوم جرافيك واردة على المنصة الالكتروني بالنسبة لأي غرض. وبالتالي يكون اعتمادكم على تلك المعلومات هو أمر يقع على مسئوليتكم الخاصة تماما.
                        وتعتبر جميع الحسابات المسجلة على اطبخلي هي حسابات محل متابعة مستمرة، وذلك لأغراض أمنية ولأسبابٍ تتعلق بالأداء. كما أننا وبموجب هذا المحرر إذ نوضح لكم صراحة أن اطبخلي لا تملك أي حساب من أي نوع.




                        حدود المسئولية

                        تعتبر اطبخلي بمثابة منصة تكنولوجية، ولا تتحمل أيما مسئولية عن أي إخفاق في يرتبط بمنتجات الطباخين.
                        لا تتحمل اطبخلي المسئولية بأي حال من الأحوال أو عن أي وضع من الأوضاع المرتبطة بالطباخين و/أو الوجبات او الاكلات /أو طراز الوجبات او علامتها أو مصداقيتها، إلا أن اطبخلي سوف تبذل قصارى جهودها لضمان جودة تلك الوجبات وجودتها.
                        ويعتبر اطبخلي بمثابة طرف وسيط يقوم بإجراء عملية ربط بين المستخدمين والطباخين فيما يتعلق بعمليات الشراء والتسليم.
                        ويعتبر الطباخين ومقدمي الخدمات من الغير بمثابة مقدمي خدماتٍ مستقلين ومتعاقدين فقط مع اطبخلي ، ولا يجوز اعتبارهم عاملين لدى اطبخلي أو ممثلين له و/أو وكلاء عنه. كما أن اطبخلي لا يؤدي المهام بنفسه، بل يتولى إدارة عملية تنفيذ المهام من خلال منصته الالكترونية وذلك من خلال توفير عملية تواصل بين المستخدم والطباخين. ويتحمل الطباخين المسئولية عن جودة منتجاتهم وكفاءتها واصليتها وتوافرها وقانونيتها، علما بأن أي إخفاق فيما ذكر أمرًا سيكون أمرا من شأنه إنهاء العلاقة مع الطباخ وكذا تواجده على المنصة الالكترونية اطبخلي ، بل وسيتم استبداله بمورد آخر لغرض الوفاء بأية طلبات مفتوحة من جانب المستخدم في هذا الشأن.
                        ويوافق كل من المستخدمين والطباخين على ألا تشارك اطبخلي في أية نزاعاتٍ بين كلا الطرفين بأي حال من الأحوال. وحيث أنه باستخدام الخدمات المذكورة يكون المستخدم قد أقر باحتمال وقوع مخاطر من الطباخين أو أخطاء منهم أو من مقدمي الخدمة من الغير، بالتالي يكون استخدام الخدمات أو أمر يقع على مسئولية المستخدم ووفق قراره، ولا تتحمل اطبخلي أيما مسئولية تنشأ أو ترتبط على أي نحوٍ أيا كان بمعاملات المستخدم أو علاقاته مع الطباخين أو مقدمي الخدمات من الغير.
                        ويكون من حق اطبخلي الإفصاح لأي مِمَّا في حيازته معلومات تخص المستخدم وذلك إلى الكيانات المؤسسية التابعة اطبخلي والكائنة ضمن مجموعة شركاته او أن استلزم القانون ذلك أو إن طـُـلب أو صدر توجيه بذلك من جانب أية جهة حكومية رسمية. وسيقوم اطبخلي بذلك حسب مطلق اختياره أو حسبما قد يتقرر ملاءمته من جانبنا أو إن كان ذلك الإفصاح امرا ينصب في مصلحتنا.
                        ويكون من حق اطبخلي إجراء أية إضافة إلى الشروط والاحكام الماثلة أو التغيير أو التعديل فيها في أي حين ودونما إخطار بذلك. ويكون على المستخدم الالتزام بأية إضافات أو تغييرات أو تعديلاتٍ حين إدراجها في متن الشروط والاحكام الماثلة على المنصة الالكترونية لاطبخلي .
                        ولا يتحمل اطبخلي أية مسئولية تجاهكم عما قد ينشأ من أضرارٍ غير مباشرة أو عارضة أو خاصة أو جزائية أو عقابية أو تبعية، حتى لو ورد اطبخلي ما يفيد باحتمال وقوع تلك الاضرار. كما لا يتحمل اطبخلي المسئولية عن أية اضرار أو التزاماتٍ أو خسائر تتحملونها، وكانت تنشأ من واقع استخدامكم أو اعتمادكم على الخدمات أو جراء عدم أمكانية دخولكم على الخدمات أو استخدامها أو من واقع أية معاملة أو علاقة نشأت بينكم وبين أي مورد من الطباخين أو مقدمي الخدمة من الغير، بل وحتى لو ورد لاطبخلي ما يفيد باحتمال وقوع تلك الاضرار. ولا يتحمل اطبخلي أيضا المسئولية عن أي تأخير أو إخفاق في التنفيذ، وكان ذلك ينشأ كنتيجة لأسبابٍ خارجة عن نطاق تحكم اطبخلي أو حدود سيطرتها المعقولة، كما لا نتحمل المسئولية في حالة احتمال عدم حيازة الطباخ أو مقدم الخدمة من الغير على ترخيص أو تصريح مهني أو تخصصي بالعمل.
                        ولا تعتبر حدود المسئولية المذكورة أمرا من شأنه تقييد المسئولية التي لا يجوز استثنائها وفق القوانين واجبة التطبيق.

                        التعويض

                        يكون عليكم الموافقة على تعويض ودريء الضرر عن اطبخلي وكياناته التابعة والجهات الراعية له وشركائه ومديرية ومسئوليه وعامليه بل ورد قيمة جميع الخسائر له/لهم وكذا قيمة الاضرار والمسئوليات والمطالبات والاحكام والتسويات والغرامات والتكاليف والمصروفات (ومنها ما هو معقول من مصروفات ورسوم قانونية وتكاليف تحقيقات)، وكانت تنشأ أو ترتبط بالإخلال بالشروط والاحكام الماثلة أو سياسة الخصوصية أو استخدامكم أو استخدام الغير (من طرفكم) للخدمات.
                        القوة القاهرة

                        لا يتحمل اطبخلي المسئولية عن أي تأخير أو إعاقة أو إخفاق في توفير الخدمات، إن كان السبب فيها يرجع إلى حدث قوة قاهرة أو ما شابهه من أحداث تخرج عن نطاق تحكمنا أو سيطرتنا وكان من شأنها منع عملية تقديم الخدمة أو أعاقتها.
                        القوانين واجبة التطبيق

                        تخضع الحقوق والالتزامات الخاصة بالأطراف المنصوص عليها في الشروط والاحكام الماثلة بل وتفسر وتؤوَّل بما يتوافق وأحكام قوانين جمهورية مصر العربية.
                        يكون عليكم القيام بموجب هذا المحرر بالخضوع ودون قيد أو شرط لمناط الاختصاص الحصري لمحاكم جمهورية مصر العربية، وذلك بالنسبة لأي نزاع ينشأ عن الشروط والاحكام الماثلة أو يرتبط بها، كما يكون عليكم التنازل عن حقكم في اتخاذ أية إجراءاتٍ قانونية في أي موضع آخر. في حين يكون لنا الحق في اتخاذ إجراءاتٍ قانونية فيما يتعلق بأي موضوع ينشأ وفق الشروط والاحكام الماثلة في أي موضع تقطنون فيه أو تجري فيه أعمالكم أو يكون لكم فيه أصول.
                        في حالة وقوع أي نزاع أو ثمة تعارض ينشأ من واقع نص الشروط والاحكام الماثلة، فيسري حينئذٍ النص العربي.



                        الاحكام المقرر عدم سريانها

                        إذا تقرر عدم صلاحية أي جزء من الشروط والاحكام الماثلة أو عدم سريانها أو نفاذها وفق القانون واجب التطبيق، فيعتبر حينئذٍ ذلك الجزء بمثابة غير ساري أو نافذ في حدود ما هو مقرر عدم صلاحيته أو سريانه أو نفاذه وحسب، ودون أن يؤثر ذلك وبأي حال من الأحوال على باقي أجزاء الشروط والاحكام.
                        مُجمل الاتفاق

                        يمثل الاتفاق الماثل وكذا سياسة الخصوصية السارية لدى اطبخلي ، بل وما يطرأ عليهم من تحديث من حين لآخر وما يتم نشره بشأنهم على كل ما تم الاتفاق والتوافق والتفاهم بشأنه فيما بيننا، وذلك فيما يتعلق بالخدمات، كما يعدو وينسخ أي مما سلفه من اتفاقات أخرى من أي نوع.
                        </textarea>
                        <span>tems and conditions in English</span>
                        <textarea name="terms"  style="width: 100%" rows="30">
                            Welcome to OTBOKHLY

                            These are the terms and conditions that govern the use of OTBOKHLY    and all its related sites and services. By using OTBOKHLY Platform, you agree to these terms and conditions. If you do not agree with these terms and conditions, please do not access or use the site.

                            The following Terms and Conditions shall apply to all Users in relation to OTBOKHLY provided services through OTBOKHLY platform “the Platform”. These Terms and Conditions come into force once you accept a service through the Platform or through any other form of communications with OTBOKHLY.

                            Please note that you should review and agree with our Privacy Policy [PP’s LINK] as it is an integral part of these terms and conditions.

                            The User agrees and accepts that the use of the Platform and the Services provided by OTBOKHLY is at the sole liability of the User, and further acknowledges that Pi disclaims all representations and warranties of any kind, whether expressed or implied.

                            Eligibility

                            In ensuring that Users are able to form legally binding contracts, eligibility to use the site is not granted to persons under the age of 18 years, or the age of legal majority in your jurisdiction, whichever is greater.

                            If you are registering as a business entity, you represent that you have the authority to bind that entity to Terms and Conditions and that you and the business entity will comply with all applicable laws and terms governing the use of the Platform.

                            Accounts and Registration

                            Account registration requires you to submit to OTBOKHLY         certain personal information, including but not limited to your company name, address, commercial register, and mobile phone number, you agree to maintain true and accurate, complete, and up to date information in your Account. You are responsible for all activity that occurs under your Account, and as such, you agree to maintain the security of your Account username and password at all times, unless otherwise permitted by OTBOKHLY in writing.

                            User accounts are not transferable. You agree to reimburse OTBOKHLY for any improper, unauthorized or illegal use of your account by you or by any person obtaining access to the Platform, services or otherwise by using your designated username and password, whether or not you authorized such access.

                            If OTBOKHLY suspects, at its sole discretion, that any of the information you provided is untrue, inaccurate, incomplete, or not current, without prejudice to any other rights and remedies of OTBOKHLY under these Terms and Conditions or under the Applicable Laws, we have the right to suspend, or limit your access to the Platform and its Services.

                            OTBOKHLY may (in its sole discretion and at any time), make any inquiries it considers necessary (whether directly or through a third party), and request that you provide a response with further information or documentation, including without limitation to verify your identity and/or ownership of your financial instruments. Without limiting the foregoing, if you are a business entity or registered on behalf of a business entity such information or documentation may include your trade license, other incorporation documents and/or documentation showing any person's authority to act on your behalf. You agree to provide any information and/or documentation to OTBOKHLY         upon such requests. You acknowledge and agree that if you do not, OTBOKHLY without liability may limit, suspend or withdraw your access to the Platform. We also reserve the right to cancel unconfirmed / unverified accounts or accounts that have been inactive for a long time.

                            By completing your registration, you acknowledge having read, understood and agreed to be bound by these Terms and Conditions, along with the Privacy Policy, as they may be amended from time to time, which are hereby incorporated and made an integral part hereof.

                            Services

                            .ETBO5LY provides display and delivery services for meals presented and prepared by chefs and / or independent persons.
                            Where the chefs present on the platform display the foods and meals that they prepare and upon their availability and the quantities available to the users of the platform so that the users of the platform can buy and order those meals

                            Electronic Communications

                            By creating an Account, you agree that you are communicating with us electronically. Therefore, you consent to receive periodic communications from us. OTBOKHLY will communicate with you via e-mail or may send you information via text messages (SMS), as well as by posting notices on the Platform as part of the normal business operation of your use of the Services. You acknowledge that opting out of any of the said means of communication may affect your use of the Services.

                            You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.

                            OTBOKHLY will request your agreement during the registration process to send you promotional emails or notifications related to the Platform and its services. If, at any time, you decide that you do not wish to receive promotional emails, you can opt out of receiving such promotional emails by clicking on the link at the bottom of any promotional email.

                            Copyrights and Trademarks

                            All content included on the Platform, including but not limited to text, graphics, OTBOKHLY’s logos, button icons, images, audio clips, digital downloads, data compilations and software, is the property and copyright work of OTBOKHLY or its licensors and is protected by copyright, trademarks, patents or other intellectual property rights and laws. The compilation of the content on the Platform is the exclusive property and copyright of OTBOKHLY and is protected by copyright, trademarks, patents or other intellectual property rights and laws.

                            OTBOKHLY and related logos are the trademarks of OTBOKHLY. OTBOKHLY's trademarks may not be used in connection with any meal or service that is not OTBOKHLY nor in any manner that disparages or discredits OTBOKHLY.

                            Use of the Platform

                            Subject to the User`s compliance with these Terms and Conditions, OTBOKHLY grants the User a limited, non-exclusive, non-sub licensable, revocable, non-transferrable license to:

                            - access and use the Services solely in connection with the provision of the Services;

                            - access and use any content, information and related materials that may be made available through the Services; and

                            Any rights not expressly granted herein are reserved by OTBOKHLY and may be revoked at any time without notice to the customer.

                            The User shall not use any trademark or any intellectual property rights belonging to OTBOKHLY from any portion of the Services, and shall not reproduce, modify, prepare, publicly, perform, transmit, stream, broadcast, or otherwise exploit the Services except as expressly permitted by OTBOKHLY  in writing, and shall not decompile, reverse engineer or disassemble the Services, and shall not link to mirror or frame any portion of the Services or launch any programs or scripts for the purpose of scraping, indexing, surveying or otherwise data mining any portion of the Services, or unduly burdening, or hindering the operation and/or functionality of any aspect of the Services or attempt to gain unauthorized access to or impair any aspect of the Services or its related systems or networks.

                            The User may not post in the meal review section for the listed meals, or anywhere else on the site (as determined at our sole discretion): solicitation, advertising, foul language, profanities, obscenities, culturally offensive material, religiously offensive material, critical political content, material that may threaten the public interest or national security, defamatory or libelous harassment, or other content that may be offensive or indecent.

                            Meal Listings and Availability

                            The terms agreed to with our various Cooks require them to ensure that the meals offered are It اhas the required quality and meets your satisfaction.

                            The information contained in the Platform is for general information purposes only. The information provided by OTBOKHLY while we endeavor to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the Platform or the information, Services, or related graphics contained on the Platform for any purpose. Any reliance you place on such information is therefore strictly at your own risk.

                            OTBOKHLY will always try to offer you the best customer experience and an exceptional level of service. OTBOKHLY is making every effort to ensure that the Cooks’ meals are available. However, OTBOKHLY shall have no responsibility if any of the Cooks’ meals that in the Platform are not available. If a meal you ordered is not available, we will inform you as quickly as possible and offer you the opportunity to choose alternatives or refund the amount of such meal(s) that were not available.

                            Meal Warranties

                            Meals sold through this platform that do not bear the OTBOKHLY brand name are under cooks’ responsibility with the terms and conditions associated with each meal. OTBOKHLY does not provide any warranty of any kind for the meals sold on the Platform. Please contact the Otbokhly for technical support and customer service related to their meals.



                            Payments

                            OTBOKHLY provides different payment methods on the Platform, which include among others, the following:

                            - Cash on delivery; and

                            - Online payment through credit cards.

                            Please note that payment on delivery in any form will be limited to a certain amount

                            OTBOKHLY reserve the right to alter, change, modify, or provide any payment methods.

                            OTBOKHLY provides the online payment services through Third-Party Providers. OTBOKHLY shall not store any data related to credit cards. OTBOKHLY shall have no responsibility to any error, damage or any matter similar that may arise to the User as a result of using the online payment, such shall be the responsibility of Third-Party Provider.

                            The User shall ensure sufficient coverage of the respective account that cover the orders and shall abide by the credit card transaction limits.

                            OTBOKHLY shall not charge the Users any extra fees for making an online payment.

                            OTBOKHLY may (in its sole discretion and at any time), make any inquiries it considers necessary (whether directly or through a third party), and request that you provide it with further information or documentation, including without limitation to verify your identity and/or ownership of your financial instruments. You agree to provide any information and/or documentation to OTBOKHLY upon such requests. You acknowledge and agree that if you do not, OTBOKHLY without liability may cancel the transaction, limit, suspend or withdraw your access to the Platform and/or your membership of the Platform.

                            Without any responsibility on OTBOKHLY, if the Cook’s meals prices, which displayed on the Platform, do not correspond to the actual price that is in the final receipt, in such case, the User will be notified with such and the User shall have the right to pay the actual price or cancel the order.

                            Delivery

                            Orders made through the Platform shall be delivered to the user within the time indicated during the checkout process. The appointment of the actual delivery will be reconfirmed by telephone on the number provided by You at check out. You will also be asked to reconfirm or clarify the address for the delivery.

                            In case of any delay in the delivery process, the Otbokhly shall notify the Users and arrange or inform you the new expected date for delivery.

                            In case of any damage to the meal(s) during the process of delivery, and before received by the User the cook will notify the User with such damage and deliver the same meal (s) to the User with no additional charges, if available. If such meal(s) is not available, the Cook will refund the meal(s) purchase amount, at the date of purchase as paid on The Site, to the User.

                            Delivery of orders will be performed directly by Otbokhly fulfillment team or by a Third-Party Service, or by a combination of both.

                            Third Party Services

                            The Services may be made available or accessed through Third-Party Service Providers and content (including advertising) that OTBOKHLY does not control. You acknowledge that different terms of use and privacy policies may apply to you for use of such third-party services and content. OTBOKHLY does not endorse such third-party services and content and in no event shall OTBOKHLY         be responsible or liable for any of the services of these Third-Party Service Providers.

                            Returns, Exchanges, and Refunds

                            The Egyptian Consumer Protection Law is binding In addition to that, the user of the platform can cancel -reverse the request- the meal request within 5 minutes only of the meal request,
                            each cook displays his own Exchange policy and the user must adhere to such exchange and return policy as long as this policy is not violating the Consumer Protection Law, without any responsibility on OTBOKHLY.

                            OTBOKHLY takes customer satisfaction very seriously. In the case of problems with your food order, please contact with OTBOKHLY through call us on our hotline number and we will assist you. In appropriate cases, if you have already been billed by OTBOKHLY, OTBOKHLY will issue full or partial refunds. In the following cases: if you did not receive your order or received an incorrect order, you may be issued a full refund; if part of your order is missing, we may issue a partial refund. In every event, we will do our best to ensure your satisfaction.


                            Privacy and Confidentiality

                            User(s) shall not disclose any information received under the contract of service with OTBOKHLY to any third party. Access to any information which pertains to business of OTBOKHLY shall be kept confidential to the extent it might adversely impact OTBOKHLY’s business. User(s) shall be liable to indemnify OTBOKHLY against any loss of business or reputation due to the act of the User(s).

                            Disclaimer

                            THE SERVICES ARE PROVIDED “AS IS” AND “AS AVAILABLE.” OTBOKHLY DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE IMPLIED WARRANTIES OF COOK ABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT, IN ADDITION, OTBOKHLY MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE REGARDING THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY OR AVAILABILITY OF THE SERVICES OR ANY SERVICES REQUESTED THROUGH THE USE OF THE SERVICES, OR THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. OTBOKHLY DOES NOT GUARANTEE THE QUALITY, SUITABILITY OR ABILITY OF THE COOKS. YOU AGREE THAT THE ENTIRE RISK ARISING FROM THE USE OF SERVICES REMAINS SOLELY WITH YOU, TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.

                            THE INFORMATION CONTAINED IN THE PLATFORM IS FOR GENERAL INFORMATION PURPOSES ONLY. THE INFORMATION PROVIDED BY OTBOKHLY         WHILE WE ENDEAVOR TO KEEP THE INFORMATION UP TO DATE AND CORRECT, WE MAKE NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, ABOUT THE COMPLETENESS, ACCURACY, RELIABILITY, SUITABILITY OR AVAILABILITY WITH RESPECT TO THE PLATFORM OR THE INFORMATION, SERVICES, OR RELATED GRAPHICS CONTAINED ON THE PLATFORM FOR ANY PURPOSE. ANY RELIANCE YOU PLACE ON SUCH INFORMATION IS THEREFORE STRICTLY AT YOUR OWN RISK.

                            ALL ACCOUNTS REGISTERED WITH OTBOKHLY ARE CONTINUOUSLY TRACKED FOR SECURITY PURPOSES AND PERFORMANCE REASONS. IT IS EXPRESSLY MADE CLEAR TO YOU HEREBY THAT OTBOKHLY DOES NOT OWN ANY ACCOUNT OF ANY KIND.

                            Limitations of Liability

                            OTBOKHLY is only a technology platform and shall not be held liable for any failure related to the Cook Meals.

                            OTBOKHLY shall not be held liable in any case or for any matter related to the cook and/or the cook’s meals and/ or the brand of such meals and its authenticity. However, OTBOKHLY will use it best endeavors to ensure the quality of such meals.

                            OTBOKHLY         is a mediator that connects Users and cooks in relation to the purchase and delivery process.

                            The cooks and the Third-Party Service Providers are independent Services providers under contract with OTBOKHLY   and are not employees, representative, and/or agents of OTBOKHLY. OTBOKHLY does not perform the task itself, but rather manages the execution of said task through its platform by providing a connection for the User with the Cooks. Cooks shall be responsible for the Cooks Meal quality, efficiency, originality, availability, and legality. Any breach of said shall result in the termination of the Cook from OTBOKHLY         platform and shall be replaced to fulfill any open requests with reference to the User regarding such.

                            Users and the Cooks agree that OTBOKHLY shall not participate in disputes between both parties in any case. Whereas, by using the said Services the User acknowledge the potential risks or mistakes of the Cooks or the Third-Party Service Providers. Accordingly, the use of the Services is at the User’s risk and judgment, and OTBOKHLY         shall have no liability arising from or in any way related to the User’s transactions or relationship with the Cooks or the Third-Party Service Providers.

                            OTBOKHLY shall be entitled to disclose any User information it may possess to affiliated corporate entities within its group of companies, or if required by law, or if requested or directed to do so by any official government body. OTBOKHLY will do so at its sole discretion or as We may determine to be suitable or in Our best interest to do so.

                            OTBOKHLY shall be entitled to add to, vary or amend any or all of these Terms and Conditions at any time without notice. The User shall be bound by any additions, variations, or amendments once incorporated into these Terms and Conditions on SOUQ ALMIDICAL ’s Platform.

                            OTBOKHLY shall not be liable to you for indirect, incidental, special, exemplary, punitive, or consequential damages, lost data, or damages that may arise, even if OTBOKHLY has been advised of the possibility of such damages. OTBOKHLY shall not be liable for any damages, liability or losses incurred by you arising out of your use of or reliance on the Services or your inability to access or use the Services or any transaction or relationship between you and any the Cook or the Third-Party Service Providers, even if OTBOKHLY has been advised of the possibility of such damages. OTBOKHLY shall not be liable for delay or failure in performance resulting from causes beyond OTBOKHLY`s reasonable control, and We shall not be liable in the event that the Cook or Third-Party Service Providers may not be professionally licensed or permitted.

                            These limitations do not purport to limit liability that cannot be excluded under the applicable laws.

                            Indemnification

                            You agree to indemnify and hold SOUQ ALMIDICAL , its affiliates, sponsors, partners, directors, officers and employees harmless from and against, and to reimburse OTBOKHLY with respect to, any and all losses, damages, liabilities, claims, judgments, settlements, fines, costs and expenses (including reasonable related expenses, legal fees, costs of investigation) arising out of or relating to your breach of these Terms and Conditions, along with our Privacy Policy or use by you or any third party of the Services.

                            Force Majeure

                            OTBOKHLY shall not be liable for any delay, interruption or failure in the provisioning of Services if caused by any act of Force Majeure, or other similar events beyond our control that may prevent or delay service provisioning.

                            Governing Laws

                            The rights and obligations of the parties pursuant to these Terms and Conditions are governed by and shall be construed in accordance with the laws of the Arab Republic of Egypt.

                            You hereby irrevocably submit to the exclusive jurisdiction of the Courts of the Arab Republic of Egypt for any dispute arising under or relating to these Terms and Conditions and waive your right to institute legal proceedings in any other jurisdiction. We shall be entitled to institute legal proceedings in connection with any matter arising under these Terms and Conditions in any jurisdiction where you reside, do business, or have assets.

                            Where there is any dispute or inconsistency arising from the text of these Terms and Conditions, the Arabic version shall prevail.

                            Unenforceable Provisions

                            If any part of these Terms and Conditions is found to be invalid or unenforceable under applicable law, such part will be ineffective to the extent of such invalid or unenforceable part only, without affecting the remaining parts of the Terms and Conditions in any way.

                            Entire Agreement

                            This Agreement along with OTBOKHLY ’s Privacy Policy, as may be updated from time to time and posted at , constitutes the complete agreement and understanding between us with respect to the Service and supersedes any other form of Agreement.

                        </textarea>
                </form>




                <!-- Recent Orders -->
                {{-- <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">withdraws List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                       @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                         <th> Cook</th>
                                       @endif
                                        <th> Amount</th>
                                        <th> Status</th>
                                        <th>date and Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @isset($withdraws)
                                   @foreach ($withdraws as $index=>$withdraw)
                                   <tr>
                                       <td>
                                           {{ $index+1 }}
                                       </td>
                                       @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                        <td>{{ $withdraw->cooks->name }}</td>
                                       @endif
                                       <td>{{ $withdraw->amount }}</td>
                                       <td>
                                           @if ($withdraw->status ==0)
                                           Pending
                                           @elseif($withdraw->status ==-1)
                                           Declineed
                                           @else
                                           Accepted
                                           @endif
                                        </td>
                                       <td>{{ $withdraw->created_at }}</td>

                                       <td >
                                           <div class="actions">
                                                @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                                    @if($withdraw->status == 0 )
                                                        <a id="" class="link_decline" href="{{ url('dashboard/withdraw/decline/'. $withdraw->id ) }}"  class="btn btn-sm btn-danger" style="color: #f00">
                                                            Decline
                                                        </a>
                                                        <a id="" class="link_accept ml-2" href="{{ url('dashboard/withdraw/accept/'.$withdraw->id ) }}" class="btn btn-sm btn-danger" style="color: Green">
                                                            Accept
                                                        </a>
                                                    @else
                                                    <a class="link_delete" id="{{ $withdraw->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> Delete</a>

                                                    @endif
                                                @elseif(auth()->guard('admin')->user()->hasRole('cook'))
                                                @if ($withdraw->status == 0)
                                                    <a id="" href="{{ url('dashboard/withdraw/cancel/'.$withdraw->id ) }}" class="link_delete" href=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        Cancel
                                                    </a>
                                                @else
                                                <a class="link_delete" id="{{ $withdraw->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> Delete</a>

                                                @endif


                                                @endif
                                           </div>
                                       </td>
                                   </tr>
                                   @endforeach
                                   @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> --}}
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->
  <!-- Add Modal -->
  {{-- <div class="modal fade" id="Add_withdraw" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add withdraw  </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span id="total">{{$total_profit??' '}}</span> EGP
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')

                <form method="POST" action="{{ route('withdraw.store') }}">
                    @csrf
                    <div class="row form-row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>Amount</label>
                                <input id="withdraw" type="number" name="amount" min="0" max="{{$total_profit <=1? 1:$total_profit }}" class="form-control" value="{{ old('amount') }}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
</div> --}}
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      {{-- <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- /Delete Modal -->


@endsection


@push('scripts')
    <script>
        @if (count($errors) > 0)
                $('#Add_withdraw').modal('show');
        @endif


        $('.link_delete').on('click',function(){
            var withdraw_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/withdraw/'+withdraw_id)

        });

        // var total_profit=$('#total').html();
        // $('#withdraw').attr("max",total_profit)

        // $('#withdraw').keydown(function(){
        //     var total=$('#total').html();
        //     var subtotal=total-$(this).val();
        //     $('#total').html(subtotal);
        // });


    </script>
@endpush
