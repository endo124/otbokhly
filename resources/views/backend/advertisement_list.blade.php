@extends('backend.layouts.app')
<style>
    .cookName{

    }
</style>

@advertisement('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of advertisement</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">advertisement</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('coupon')) --}}
                <div class="col-sm-12 col">
                    <a href="#Add_advertisement" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >Add</a>
                </div>
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">advertisement List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> Advertisement</th>
                                        <th> coupon</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset ($ads)
                                        @foreach ($ads as $index=>$ad)
                                        <tr>
                                            <td>
                                                {{ $index }}
                                            </td>

                                            <td><img class="avatar" src="{{ asset('backend/img/offers/'. $ad->advertisement ) }}" alt=""></td>
                                            <td>
                                                <div>
                                                    <span style="padding: 2px;border:1px solid #ccc">{{ $ad->coupon }}</span>
                                                    <a href="#view" data-toggle="modal" class="btn btn-otbokhly details" id="{{ $ad->coupon }}"  style="padding:5px 8px">registered chefs</a>
                                                </div>


                                            </td>

                                            <td >
                                                <div class="actions">
                                                    <a id="{{ $ad->id }}" class="link_update bg-success-light mr-2" data-toggle="modal"  href="#edit_advertisement_details{{ $ad->id }}" >
                                                        <i class="fe fe-pencil"></i> Edit
                                                    </a>
                                                    <a id="{{ $ad->id }}" class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        <i class="fe fe-trash"></i> Delete
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- Edit Details Modal -->
                                        <div class="modal fade" id="edit_advertisement_details{{ $ad->id }}" aria-hidden="true" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document" >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit advertisement</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group errors" style="display: none" >
                                                            <div class="alert alert-danger">
                                                              <ul class="list">

                                                              </ul>
                                                            </div>
                                                        </div>
                                                    @if (isset($ad))

                                                    <form id="formUpdate" method="post" action="{{ route('advertisement.update',$ad->id) }}" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('put')
                                                        <div class="row form-row">
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label>Advertisement Images : </label>
                                                                    <br>
                                                                    <input class="box__file" type="file" name="files" id="file" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  />
                                                                    {{-- <label for="file"><strong>Choose a file</strong></label> --}}
                                                                    <div class="upload-wrap">

                                                                    </div>
                                                                </div>
                                                                {{-- <div class="form-group">
                                                                    <label>Advertisement Images : </label>
                                                                    <input class="box__file" type="file" name="files" id="file" onchange="showImage(this)"   data-multiple-caption="{count} files selected"    hidden/>
                                                                    <label for="file"><strong>Choose a file</strong></label>
                                                                    <div class="upload-wrap">
                                                                        <div class="upload-image float-left" id="upload-image" style="display:inline-block">
                                                                            <img  width=50 src="{{asset('backend/img/offers/'.$ad->advertisement)  }}">
                                                                        </div>
                                                                    </div>
                                                                </div> --}}
                                                            </div>
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group row">
                                                                    <div class="col-9 col-sm-9">
                                                                    <label>Coupon Name : </label>

                                                                    <select  name="coupon_name"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                        @foreach ($coupons as $coupon)
                                                                            <option value="{{ $coupon->name }}" {{$ad->coupon ==$coupon->name ? 'selected': ' '  }}>{{ $coupon->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                    </form>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Edit Details Modal -->
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>



        <div class="modal fade" id="view" aria-hidden="true" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">registered chefs </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >
                        <ul id="advdetails">

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

  <!-- Add Modal -->
  <div class="modal fade" id="Add_advertisement" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add advertisement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')
                <div class="form-group errors" style="display: none" >
                    <div class="alert alert-danger">
                      <ul class="list">

                      </ul>
                    </div>
                </div>
                <form method="POST" action="{{ route('advertisement.store') }} " enctype="multipart/form-data">
                    @csrf
                    <div class="row form-row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>Advertisement Images : </label>
                                <br>
                                <input class="box__file" type="file" name="files" id="file" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  />
                                {{-- <label for="file"><strong>Choose a file</strong></label> --}}
                                <div class="upload-wrap">

                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>Coupons : </label>
                                    <select id="coupon" name="coupon"   data-live-search="true" style="width:50%;paddind:2px">
                                        @foreach ($coupons as $coupon)
                                            <option value="{{ $coupon->name }}">{{ $coupon->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->




@push('scripts')
    <script>


                var err= <?php echo json_encode(session()->get('message_update')); ?>;
                var errors_add= <?php echo json_encode(session()->get('message_add')); ?>;

                if(err !=null) {
                    let id= <?php echo json_encode(session()->get('ad_id')); ?>;
                    let modal_edit='#edit_advertisement_details'+id;
                    for (x in err) {
                        var err='<li>'+ err[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');
                    $(modal_edit).modal('show');
                }

                else if(errors_add != null)
                {
                    for (x in errors_add) {
                        var err='<li>'+ errors_add[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');

                    $('#Add_advertisement').modal('show');

                }

                $(".modal").on("hidden.bs.modal", () => {
                    $('.errors').css('display','none');
                });

        function removeDiv(elem){
            $(elem).parent('div').remove();
        }

        function showImage(file){


        for (var i = 0; i < file.files['length']; i++) {

            var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
                        "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
                        '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
                    '</div>';

            $(".upload-wrap").append(img);

            $('.upload-image').css('display','block');
        };
        };




        $('.link_delete').on('click',function(){
            var advertisement_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/advertisement/'+advertisement_id)

        });



        $('.details').click(function(){
            let coupon_id =$(this).attr('id');
            $('#advdetails').html('')

            $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/dashboard/advertisement/details/'+coupon_id,

                    success:function(data){
                       var cooks= data.success;
                    //    console.log(data)
                        for (var x in cooks) {
                                var cook='<li class="cookName">'+cooks[x]+'</li>';
                            $('#advdetails').append(cook)

                        }
                    }
            });

      });

    </script>
@endpush
