
@extends('backend.layouts.app')



@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">change password</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">change password</li>
                    </ul>
                </div>

            </div>

        </div>
        <!-- /Page Header -->

    <!-- Page Content -->
    <div class="content mt-5">
        <div class="container">

            <div class="row">
                <div class="col-md-7 col-lg-8 col-xl-9">

                    <form action="{{ route('changePassword') }}" method="post">
                        @csrf
                        @method('put')
                        <!-- Basic Information -->
                        <div class="card">
                            <div class="card-body">
                                @include('backend.partials.errors')

                                <div class="form-group errors" style="display: none" >
                                    <div class="alert alert-danger">
                                      <ul class="list">

                                      </ul>
                                    </div>
                                </div>

                                <div class="row form-row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Current Password</label>
                                            <input type="password" name="currentpassword" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>New Password </label>
                                            <input type="password" name="password" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Password Confirmation </label>
                                            <input type="password" name="password_confirmation" class="form-control" value="" >
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!-- /Basic Information -->


                        <div class="submit-section submit-btn-bottom">
                            <button type="submit" class="btn btn-primary submit-btn"> <i class="fas fa-edit"></i> Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
    <!-- /Page Content -->
    </div>
</div>


@endsection




@push('scripts')

<script>
        var err= <?php echo json_encode(session()->get('validate')); ?>;

        if(err !=null) {

        console.log(err,'lllllllllllll')

        // for (x in err) {
        //     var err='<li>'+ err[x]+'</li>';

        //     $('.list').append(err);
        // }
        $('.list').append(err);

        $('.errors').css('display','block');
    }
</script>
@endpush
