<!-- Header -->
@php
        $notifications=App\Models\Notification::all();
        if(auth('admin')->user()->hasRole('cook')){
            $notifications_cook_notread=App\Models\Notification::where('read',0)->where('cook_id',auth('admin')->user()->id)->get();

        }
        $notifications_admin_notread=App\Models\Notification::where('read',0)->get();
@endphp

<div class="header">

    <!-- Logo -->
    <div class="header-left mt-1">
        <a href="#" class="logo" style="color: #fff">
            <img src="{{ asset('front/images/msol-logo.png') }}" alt="">
            {{-- <img src="{{ asset('front/images/msol-logo.png') }}" width="150" alt="Logo"> --}}
        </a>
        <a href="#" class="logo logo-small">
            {{-- <img src="{{ asset('front/images/msol-logo.png') }}" alt="Logo" width="30" height="30"> --}}
        </a>
    </div>
    <!-- /Logo -->

    <a href="javascript:void(0);" id="toggle_btn" style="color: #fff">
        <i class="fe fe-text-align-left" ></i>
    </a>

    {{-- <div class="top-nav-search">
        <form>
            <input type="text" class="form-control" placeholder="Search here">
            <button class="btn" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div> --}}

    <!-- Mobile Menu Toggle -->
    <a class="mobile_btn" id="mobile_btn">
        <i class="fa fa-bars"></i>
    </a>
    <!-- /Mobile Menu Toggle -->

    <!-- Header Right Menu -->
    <ul class="nav user-menu">
        <ul style="list-style: none">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <li style="display: block">
                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" style="color: #fff">
                        {{ $properties['native'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        {{-- @isset($notifications) --}}

                 <!-- Notifications -->
        <li class="nav-item dropdown noti-dropdown">
            {{-- <a href="#" id="pill" class="dropdown-toggle nav-link" data-toggle="dropdown">
                <i class="fe fe-bell"></i>
                @if (!auth('admin')->user()->hasRole('cook'))
                    @if (count($notifications_admin_notread) > 0)
                        <span class="badge badge-pill">{{ count($notifications_admin_notread) }}</span>
                    @endif
                @else
                    @if (count($notifications_cook_notread) > 0)
                        @php
                        $ordersarray=[];
                        foreach ($notifications_cook_notread as $notification) {
                                $notify=json_decode( $notification['notify']);
                                if($notify->notify == 'withdraw'){
                                    array_push($ordersarray , $notify);
                                }
                        }
                        @endphp
                        @dump($notify)
                        <span class="badge badge-pill">
                            {{ count($ordersarray) }}
                        </span>
                    @endif
                @endif

            </a> --}}
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                <i class="fe fe-bell"></i> <span class="badge badge-pill " id="notif"></span>
            </a>
            <div class="dropdown-menu notifications">
                <div class="topnav-dropdown-header">
                    <span class="notification-title">Notifications</span>
                    {{-- <a href="javascript:void(0)" class="clear-noti"> Clear All </a> --}}
                </div>
                <div class="noti-content">

                    <ul class="notification-list">
                        <li class="notification-message">

                        </li>


                    </ul>
                </div>
                {{-- <div class="topnav-dropdown-footer">
                    <a href="#">View all Notifications</a>
                </div> --}}
            </div>

        </li>
        <!-- /Notifications -->

        {{-- @endisset --}}

        <!-- User Menu -->
        <li class="nav-item dropdown has-arrow">
            @if (auth()->guard('admin')->user()->hasRole('cook'))
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <span class="user-img">
                        {{-- <img class="rounded-circle" src="{{ asset('backend/img/cook/'.auth()->guard('admin')->user()->images) }}" width="31"> --}}
                        <h4 style="color: #fff">{{ auth()->guard('admin')->user()->name }}</h4>
                    </span>
                </a>
            @else
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                <span class="user-img">
                    {{-- <img class="rounded-circle" src="{{ asset('backend/img/profiles/avatar-01.jpg') }}" width="31" alt="Ryan Taylor"> --}}
                    <h4 style="color: #fff">{{ auth()->guard('admin')->user()->name }}</h4>

                </span>
            </a>
            @endif

            <div class="dropdown-menu">
                <div class="user-header">
                    @if (auth()->guard('admin')->user()->hasRole('cook'))
                    <div class="avatar avatar-sm">
                        <img src="{{ asset('backend/img/cook/'.auth()->guard('admin')->user()->images) }}" alt="User Image" class="avatar-img rounded-circle">
                    </div>

                    @else
                        <div class="avatar avatar-sm">
                            <img src="{{ asset('backend/img/profiles/avatar-01.jpg') }}" alt="User Image" class="avatar-img rounded-circle">
                        </div>
                    @endif
                    <div class="user-text">
                        <h6>{{ auth()->guard('admin')->user()->name }}</h6>
                        <p class="text-muted mb-0">{{ auth()->guard('admin')->user()->roles[0]->display_name }}</p>
                    </div>
                </div>
                @if (auth()->guard('admin')->user()->hasRole('cook'))

                <a class="dropdown-item" href="{{ auth()->guard('admin')->user() ? url('dashboard/cook/{id}/edit') : '' }}">My Profile</a>
                @endif

                {{-- <a class="dropdown-item" href="settings.html">Settings</a> --}}

                <form action="{{ url('dashboard/logout') }}" method="post">
                    @csrf
                    <button type="submit" class="dropdown-item" >Logout</button>
                </form>
            </div>
        </li>
        <!-- /User Menu -->

    </ul>
    <!-- /Header Right Menu -->

</div>


<!-- /Header -->
