@extends('backend.layouts.app')

@push('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css">
@endpush

<style>
    thead input {
        width: 100%;
    }
</style>
@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of account</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">account</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                {{-- <div class="col-sm-12 col">
                    <a href="#Add_feast" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >Add</a>
                </div> --}}
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">Account List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Cook Name</th>
                                        <th>Commission</th>
                                        <th>Delivery Fees</th>
                                        <th>Discount</th>
                                        <th>Total price</th>
                                        <th>Total profit</th>
                                        <th>Revenue</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $index=> $order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                        if ($cook->commission_type == '%') {
                                            $commission=($order->total_price *$cook->commission)/100 ." EGP";
                                        }else{
                                        $commission=$cook->commission . " " .$cook->commission_type ;
                                        }

                                    $fees=0;
                                    $discount=0;
                                    if($order->delivery_fees){
                                        $fees=$order->delivery_fees;
                                    }
                                    if ($order->coupon['discount_type'] =="%") {
                                        $discount= ($order->coupon['total']*$order->total_price)/100;
                                    }
                                    else if ($order->coupon['discount_type'] =="EGP") {
                                        $discount= $order->coupon['total'];
                                    }
                                    if ($order->total_price) {
                                        $total= $order->total_price;
                                    }
                                    $revenue =$fees+$discount+ $total;
                                    $profit =floatVal($commission) -(intVal($discount)/2) .' EGP';

                                    @endphp
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{ $cook_name }}</td>
                                        <td>{{ $commission ?? '0'}}</td>
                                        <td class="fees">{{ $order->delivery_fees ?? '0' }} EGP</td>
                                        <td class="discount">{{ $order->coupon['discount_type'] =="%" ? ($order->coupon['total']*$order->total_price)/100 : $order->coupon['total'] .' EGP' }}</td>
                                        <td class="total">{{ $order->total_price.' EGP' }}</td>
                                        <td class="profit">{{ $profit }}</td>
                                        <td class="revenue">{{ $revenue.' EGP' }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->


      <!-- Delete Modal -->
{{--
      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- /Delete Modal -->


@endsection


@push('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>
    <script>

        $( document ).ready(function() {
            $('.revenue').html()
        });


        // @if (count($errors) > 0)
        //         $('#Add_feast').modal('show');
        // @endif

        // $('.link_update').on('click',function(){
        //     var feast_id=$(this).attr('id');

        //     $('#formUpdate').attr('action','/{{ Config::get('app.locale') }}/dashboard/feast/'+feast_id)

        // })
        // $('.link_delete').on('click',function(){
        //     var feast_id=$(this).attr('id');

        //     $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/feast/'+feast_id)

        // })
        $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#example').DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    } );

} );

    </script>
@endpush
