@extends('backend.layouts.app')
<style>
.dish-type  li{
    list-style: none;
    float: left;
    margin-left:10px
}

/* #addRow{
    display: hidden;
}
#addRow:first-child {
display: block;

} */
#new_addon2{
    margin-left: 61px;
    margin-top: 6px;
    width: 100%;
}
.fa-trash-alt{
    margin-left: 5px
}
</style>

@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container">

        <div class="row" style="justify-content: center" >
            <div class="col-md-8 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">Edit Dish</h4>

                    </div>
                    <div class="card-body" style="padding: 20px 50px">
                        @include('backend.partials.errors')

                            <form id="formUpdate" method="post" action="{{ route('dish.update',$dish->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('put')


                                <div class="row form-row">
                                    @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                    <div class="col-12 col-sm-12 mb-4">

                                        <label>Cooks : </label>

                                        <select  name="cook_name"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                            @foreach ($cooks as $cook)
                                            <option {{ $cook->id== $dish->users->id? 'selected' :' ' }}  value="{{ $cook->id }}">{{ $cook->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    @foreach (config('translatable.locales') as $locale)
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.dishname')</label>
                                            <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ $dish->translate($locale)->name ??old($locale.'.name') }}">
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <div  class="dropzone dz-clickable" >
                                                <div class="dz-default dz-message">
                                                <label>Dish Images : </label>
                                                    <br>
                                                    <input class="box__file" type="file" name="files[]" id="file1" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  multiple  />
                                                    {{-- <label for="file"><strong>Choose a file</strong></label> --}}
                                                </div>
                                            </div>
                                            <div class="upload-wrap">
                                                <div class="upload-image float-left" id="upload-image" style="display:inline-block">
                                                    @php
                                                        $dishes=explode('__',$dish->images)
                                                    @endphp
                                                @foreach ($dishes as $img)
                                                <img id='img_src' width=50 src="{{ asset('/backend/img/dishes/'.$img) }}">
                                                {{-- <a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a> --}}
                                                @endforeach
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 mt-4">
                                        <div class="form-group row">
                                            <div class="col-9 col-sm-9">
                                            <label>Cusines : </label>
                                                <select name="cusine"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                    @foreach ($cusines as $cusine)
                                                        <option {{ (in_array($cusine->id, $dish->cusines->pluck('id')->toArray()))? 'selected' :' ' }}  value="{{ $cusine->id }}" >{{ $cusine->name }}</option>
                                                    @endforeach

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-9 col-sm-9">
                                            <label>Categories : </label>
                                                <select id="category" name="category"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                    <option value="">select</option>
                                                    @foreach ($categories as $category)
                                                        @if (isset($dish->categories->id))
                                                                <option {{ $category->id== $dish->categories->id ? 'selected' :' ' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @else
                                                            <option alue="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row" id="addon_section2"  >
                                            @if (count($dish->addons) >= 1)
                                                @for ($i = 0; $i < count($dish->addons); $i++)
                                                    <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                        <label>Addons : </label>
                                                        @if ($i==0)
                                                            <a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a>
                                                            <a onclick="addRow()"  class="float-right mt-2" style="font-size: medium;">+</a>
                                                        @else
                                                        <a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a>
                                                        @endif
                                                        <select class="addons select_addon"   name="addons[]"    style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                            <option value=" " >select</option>
                                                            @foreach ($addons as $addon)
                                                                <option value="{{ $addon->id }}" {{ $addon->id== $dish->addons[$i]->id ? 'selected' :' ' }}>{{ $addon->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div  id="new_addon2" style="margin-left: 60px; ">
                                                            <select  name="addon_conditions[]"      style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" required>
                                                                <option value="1" {{ $dish->addons[$i]->pivot->condition == '1' ? 'selected' : '' }} >required</option>
                                                                <option value="0"  {{ $dish->addons[$i]->pivot->condition == '0' ? 'selected' : '' }}>optional</option>
                                                            </select>
                                                            <p>
                                                                <span >max</span><input type="number" min="0" value="{{ $dish->addons[$i]->pivot->max }}" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                <span>min</span><input type="number" min="0" value="{{ $dish->addons[$i]->pivot->min }}" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                            </p>

                                                        </div>
                                                    </div>
                                                @endfor
                                            @else
                                            <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                <label>Addons : </label><a onclick="removeDiv(this)" class="float-right mt-2" style=";margin-right:3px;font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a><a onclick="addRow()" class="float-right mt-2" style="font-size: medium;">+</a>
                                                <select class="addons select_addon"    name="addons[]"      style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                    <option value=" ">select</option>
                                                    @foreach ($addons as $addon)
                                                        <option value="{{ $addon->id }}" {{ (in_array($addon->id, $dish->addons->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $addon->name }}</option>
                                                    @endforeach
                                                </select>

                                                <div  id="new_addon2" >
                                                    <select  name="addon_conditions[]"  required style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                        <option value=" ">select</option>
                                                        <option value="1">required</option>
                                                        <option value="0">optional</option>
                                                    </select>
                                                    <p>
                                                        <span >max</span><input type="number" min="0" value="{{ old('max[]') }}" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                        <span>min</span><input type="number" min="0" value="{{ old('min[]') }}" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                    </p>

                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>

                                        <div class="col-12 col-sm-12" id="AddonSection">
                                        </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>portions available : </label>
                                            <ul class="nav nav-tabs" >
                                                <li class="nav-item">
                                                    <a class="nav-link active"  data-toggle="tab" href="#hoome{{ $dish->id }}" >Large</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"  data-toggle="tab" href="#proofile{{ $dish->id }}" >Medium</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"  data-toggle="tab" href="#coontact{{ $dish->id }}" >Small</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">

                                                <div class="tab-pane fade show active" id="hoome{{ $dish->id }}" >
                                                    <input  type="number" min="1" name="portions_available[]" class="form-control "  value="{{ $dish->portions_available[0] }}" style="width: 20%;display:inline"> <span class="mr-5"> persons</span>
                                                    <input  type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[0] }}" style="width: 20%;display:inline"> <span> EGP</span>
                                                </div>
                                                <div class="tab-pane fade" id="proofile{{ $dish->id }}" >
                                                    <input   type="number" min="1" name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[1] }}" style="width: 20%;display:inline"> <span  class="mr-5"> persons</span>
                                                    <input   type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[1] }}" style="width: 20%;display:inline"> <span> EGP</span>
                                                </div>
                                                <div class="tab-pane fade" id="coontact{{ $dish->id }}" >
                                                    <input   type="number" min="1" name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[2]  }}" style="width: 20%;display:inline"> <span class="mr-5"> persons</span>
                                                    <input   type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[2] }}" style="width: 20%;display:inline"> <span> EGP</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <label>Section : </label>
                                        <ul class="dish-type">
                                        @foreach ($sections as $section)
                                        <li>
                                            <input type="radio"  name="section" value="{{ $section->id }}" {{$dish->section_id == $section->id ?'checked':' ' }} >
                                            <label for="f-option">{{ $section->name }}</label>
                                            <div class="check"></div>
                                        </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-12 mb-4">
                                        <label>availability : </label>
                                        <ul class="dish-type">
                                        <li>
                                            <input type="radio" id="f-option" name="dish_available" value="1" {{$dish->available ==1 ? 'checked':'' }}>
                                            <label for="f-option">availabe</label>
                                            <div class="check"></div>
                                            {{-- <input name="available_count" type="number" style="width: 20%" value="{{$dish->available_count??old('available_count')  }}"> dish --}}
                                        </li>
                                        <li>
                                            <input type="radio" id="" name="dish_available" value="0" {{$dish->available ==0 ? 'checked':'' }}>
                                            <label for="f-option">not available</label>
                                            <div class="check"></div>
                                        </li>
                                        </ul>
                                    </div>
                                    {{-- @foreach (config('translatable.locales') as $locale)
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.ingredients')</label>
                                            <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{ $dish->main_ingredients??old($locale.'.ingredients') }}">
                                        </div>
                                    </div>
                                    @endforeach --}}
                                    @foreach (config('translatable.locales') as $locale)

                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.time_of_preparation')</label>
                                            <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{$dish->translate($locale)->main_ingredients ?? old($locale.'.main_ingredients') }}">
                                        </div>
                                    </div>
                                    @endforeach

                                    @foreach (config('translatable.locales') as $locale)

                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.description')</label>
                                            <textarea name="{{ $locale }}[info]" style="width: 90%;display: block;"  rows="3">{{$dish->translate($locale)->info ?? old($locale.'.info') }}</textarea>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <button type="submit" class="btn btn-otbokhly btn-block">Save Changes</button>
                            </form>


                        </div>


                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<div class="col-9 col-sm-9 mt-3" id="newRow" style="display: none">
    <label>Addons : </label><a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;"><i class="far fa-trash-alt "></i></a><a onclick="addRow()" class="float-right mt-2" style="font-size: medium;">+</a>
    <select class="addons1"   name="addons[]"     style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
        <option  value=" ">select</option>
        @foreach ($addons as $addon)
            <option value="{{ $addon->id }}" {{ (in_array($addon->id, $dish->addons->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $addon->name }}</option>
         @endforeach
    </select>
    <div  id="new_addon" style="margin-left: 73px; margin-top:5px">
        <select  name="addon_conditions[]"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" required>
        <option  value=" ">select</option>
            <option value="1">required</option>
            <option value="0">optional</option>
        </select>
        <p>
            <span >max</span><input type="number" min="0" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
            <span>min</span><input type="number" min="0" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
        </p>

    </div>
</div>

@endsection


@push('scripts')
    <script>
    @if (count($errors) > 0)
            $('#Add_Dish').modal('show');
    @endif

function removeDiv(elem){
    $(elem).parent('div').remove();
}

function showImage(file){


for (var i = 0; i < file.files['length']; i++) {

    var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
                "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
                // '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
            '</div>';

    $(".upload-wrap").append(img);

    $('.upload-image').css('display','block');
};
};

$('#category').change(function(){
    $('#addon_section').show();
    var category_id = document.getElementById('category').value;
    $('.addons').find('option').remove().end();

    $.ajax({
        type: 'GET',
        url: '/{{ Config::get('app.locale') }}/dashboard/addons/'+ category_id,
        success: function(data) {
            for (var i = 0; i < data.success.length; i++) {
                $(".addons").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');

            }
        }
    });

});




    function addRow (){
      var test= $('#newRow').clone();
      test.appendTo('#AddonSection');
      test.css('display','block')


    };

    </script>
@endpush


