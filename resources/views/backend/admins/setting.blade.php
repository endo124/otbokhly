@extends('backend.layouts.app')

<style>
    .span {
        position: absolute;
        top: 18px;
        right: 9px;
        background-color: #e8e5e5;
        padding: 6px 10px;
    }
    input ,select{
        border: 1px solid #ddd;
        box-shadow: none;
        color: #333;
        font-size: 15px;
        height: 40px;
    }
    select {
        background: #fff;
        padding-right: 20px;
        padding-left: 20px;
    }
    table tr td{
        padding: 15px 7px;
        font-size: 18px;

    }
    table > tr  {
        display: flex;
        align-items: center
    }
    table tr td:first-child{
        font-size: larger;
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
    }

</style>

@section('content')

<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">General Settings</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="javascript:(0)">Settings</a></li>
                        <li class="breadcrumb-item active">General Settings</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">

            <div class="col-12">

                <!-- General -->

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">General</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('setting.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Google Map API</label>
                                    <input name="googleapikey" type="text" class="form-control" value="{{ $sett->googleapikey ?? old('googleapikey') }}">
                                </div>

                                @foreach (config('translatable.locales') as $locale)
                                    <div class="form-group">
                                        <label for="terms">@lang('site.'.$locale.'.termsandconditions')</label>
                                        <textarea class="form-control" name="{{ $locale }}[terms]" id="" cols="30" rows="10">{{$sett? $sett->translate($locale)->terms : '' }}</textarea>
                                    </div>
                                @endforeach
                                <div class="form-group">
                                    <label>Cash back</label>
                                    <input name="cashback" type="text" class="form-control" value="{{ $sett->cashback ?? old('cashback') }}">
                                </div>
                                {{-- <div class="form-group">
                                    <label>Loyalty</label>
                                    <input name="loyality" type="text" class="form-control" value="{{ $sett->loyality ?? old('loyality') }}">
                                </div> --}}
                                <div class="form-group">
                                    <label>Order rejection time *per mins</label>
                                    <input name="rejection" min="0" type="number" class="form-control" value="{{ $sett->rejection ?? old('rejection') }}">
                                </div>
                                {{-- <div class="form-group">
                                    <label>Website Logo</label>
                                    <input type="file" class="form-control">
                                    <small class="text-secondary">Recommended image size is <b>150px x 150px</b></small>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Favicon</label>
                                    <input type="file" class="form-control">
                                    <small class="text-secondary">Recommended image size is <b>16px x 16px</b> or <b>32px x 32px</b></small><br>
                                    <small class="text-secondary">Accepted formats : only png and ico</small>
                                </div> --}}
                                <button class="btn btn-success" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>

                <!-- /General -->

            </div>
        </div>

        <div class="row">

            <div class="col-md-6 col-sm-12">

                <!-- General -->

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Loyality</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('loyality') }}" method="post">
                                @csrf


                                <table>
                                    <tr>
                                        <td>Rewards Points : </td>
                                        <td colspan="3" style="display: flex;    align-items: center;justify-content: center;">
                                            <input type="radio" id="yes" name="reward" value="1" {{$sett && $sett->reward && $sett->reward == '1' ? 'checked' : ''}}>
                                            <label for="yes" style="margin: 0;padding:10px">Yes</label>
                                            <input type="radio" id="no" name="reward" value="0"  {{$sett && $sett->reward && $sett->reward == '0' ? 'checked' : ''}}>
                                            <label for="no" style="margin: 0;padding:10px">No</label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Set Rewards : </td>
                                        <td style="position: relative;">
                                            <input type="text"  name="reward_amount" value=" {{$sett && $sett->reward_amount  ? $sett->reward_amount  : ''}}">
                                            <span class="span">{{ $sett->currency }}</span>
                                        </td>
                                        <td style=" padding: 0px 20px">
                                            <span  style="background-color: #e8e5e5;padding: 5px 15px">=</span>
                                        </td>
                                        <td style="position: relative;">
                                            <input type="text" name="reward_points"  value=" {{$sett && $sett->reward_points  ? $sett->reward_points  : ''}}">
                                            <span class="span">Points</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Set Percentage : </td>
                                        <td style="position: relative;">
                                            <input type="text" name="loyality_point" value=" {{$sett && $sett->loyality_point  ? $sett->loyality_point  : ''}}">
                                            <span class="span">Points</span>
                                        </td>
                                        <td style=" padding: 0px 20px">
                                            <span  style="background-color: #e8e5e5;padding: 5px 15px">=</span>
                                        </td>
                                        <td style="position: relative;">
                                            <input type="text" name="loyality_percentage" value=" {{$sett && $sett->loyality_percentage  ? $sett->loyality_percentage  : ''}}">
                                            <span class="span">%</span>
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                        <td>Redeem Order : </td>
                                        <td colspan="3">

                                            <select name="" id="">
                                                <option value="">Select Redeem Order</option>
                                            </select>
                                        </td>

                                    </tr> --}}
                                    <tr>
                                        <td>Maximum Value : </td>
                                        <td colspan="1" style="position: relative;">
                                            <input type="text" name="loyality_max" value=" {{$sett && $sett->loyality_max  ? $sett->loyality_max  : ''}}">
                                            <span class="span">{{ $sett->currency }}</span>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Minimum Order : </td>
                                        <td colspan="1" style="position: relative;">
                                            <input type="text" name="loyality_min" value=" {{$sett && $sett->loyality_min  ? $sett->loyality_min  : ''}}">
                                            <span class="span">{{ $sett->currency }}</span>
                                        </td>
                                    </tr>

                                </table>

                                <button class="btn btn-success" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>

                <!-- /General -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

@endsection


@push('scripts')

@endpush
