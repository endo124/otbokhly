@extends('backend.layouts.app')

<style>
    .card-table .card-body {
    padding: 0;
    max-height: 500px;
    overflow: scroll;
}

</style>


@section('content')

<!-- Page Wrapper -->
<div class="page-wrapper">

    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Welcome {{ auth()->guard('admin')->user()->name }}!</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            @if (auth()->guard('admin')->user()->roles[0]->name != 'cook')
            <div class="col-xl-3 col-sm-6 col-12">
                <a href="{{ url('/dashboard/cook') }}">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-primary border-primary">
                                    <i class="fe fe-users"></i>
                                </span>
                                <div class="dash-count">
                                    <h3>{{ count($cooks) }}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <h6 class="text-muted">Cooks</h6>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary" style="width: {{ count($cooks) }}%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endif
            <div class="col-xl-3 col-sm-6 col-12">
                <a href="{{ url('/dashboard/customer') }}">
                    <div class="card">
                        <div class="card-body">
                            @isset($customers)
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-success">
                                    <i class="fa fa-users"></i>
                                </span>
                                <div class="dash-count">

                                        <h3>{{ count($customers) }}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">

                                <h6 class="text-muted">Customers</h6>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-success" style="width: {{ count($customers) }}%"></div>
                                </div>
                            </div>
                            @endisset
                        </div>
                    </div>
                </a>
            </div>

           @isset($orders)
           <div class="col-xl-3 col-sm-6 col-12">
                <a href="{{ url('/dashboard/order') }}">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-danger border-danger">
                                    <i class="fe fe-folder"></i>
                                </span>
                                <div class="dash-count">
                                    <h3>{{ count($orders_accepted) ?? ' ' }}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info" >

                                <h6 class="text-muted">Accepted Orders</h6>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-danger" style="width: {{ count($orders_accepted) ?? 0}}%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>
        <fieldset>
            <legend>Monthly</legend>
            <div class="row">

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #4e9cc9 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $total_profit ?? ' ' }} EGP</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #4e9cc9">Total Profit</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>

                                        <div class="progress-bar" style="background-color : #4e9cc9 !important;  width:{{ $total_profit / 1000  ?? ' '}}% ;background-color:#E9ECEF"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @isset($discount)
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #3799d1 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $discount ?? ' ' }} EGP</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #3799d1">Total Discount</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>

                                        <div class="progress-bar" style="background-color : #3799d1 !important;  width:{{ $discount / 1000  ?? ' '}}% ;background-color:#E9ECEF"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endisset

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-danger border-danger" style="color: #3C8DBC !important;border :3px solid #3C8DBC !important">
                                        <i class="fe fe-money" style="color: #3C8DBC" ></i>
                                    </span>
                                    @isset($delivery_fees)
                                    <div class="dash-count">
                                        <h3>{{$delivery_fees ??' ' }}</h3>
                                    </div>
                                    @endisset
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted" style="color: #3C8DBC">Delivery Fees</h6>
                                    <div class="progress progress-sm">
                                        @isset($delivery_fees)
                                            <div class="progress-bar bg-danger" style="background-color : #3C8DBC !important;width: {{ $delivery_fees/100 ?? 0 }}%;"></div>
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning"  style="color: #1374e2 !important;border :3px solid #1374e2 !important">
                                        <i class="fe fe-money" style="color: #1374e2"></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $total_revenue ?? ' ' }} EGP</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted">Revenue</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning"></div>

                                        <div class="progress-bar" style="background-color : #1374e2 !important;width:{{ $total_revenue / 1000  ?? ' '}}%"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Annual</legend>
            <div class="row">

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #4e9cc9 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $total_profit ?? ' ' }} EGP</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #4e9cc9">Total Profit</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>

                                        <div class="progress-bar" style="background-color : #4e9cc9 !important;  width:{{ $total_profit / 1000  ?? ' '}}% ;background-color:#E9ECEF"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @isset($discount)

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #3799d1 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $discount ?? ' ' }} EGP</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #3799d1">Total Discount</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>

                                        <div class="progress-bar" style="background-color : #3799d1 !important;  width:{{ $discount / 1000  ?? ' '}}% ;background-color:#E9ECEF"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endisset
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-danger border-danger" style="color: #3C8DBC !important;border :3px solid #3C8DBC !important">
                                        <i class="fe fe-money" style="color: #3C8DBC" ></i>
                                    </span>
                                    @isset($delivery_fees)
                                    <div class="dash-count">
                                        <h3>{{$delivery_fees ??' ' }}</h3>
                                    </div>
                                    @endisset
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted" style="color: #3C8DBC">Delivery Fees</h6>
                                    <div class="progress progress-sm">
                                        @isset($delivery_fees)
                                            <div class="progress-bar bg-danger" style="background-color : #3C8DBC !important;width: {{ $delivery_fees/100 ?? 0 }}%;"></div>
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning"  style="color: #1374e2 !important;border :3px solid #1374e2 !important">
                                        <i class="fe fe-money" style="color: #1374e2"></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $total_revenue ?? ' ' }} EGP</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted">Revenue</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning"></div>

                                        <div class="progress-bar" style="background-color : #1374e2 !important;width:{{ $total_revenue / 1000  ?? ' '}}%"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </fieldset>

        @endisset
        <div class="row">
            @if (auth()->guard('admin')->user()->roles[0]->name != 'cook')
            <div class="col-md-6 d-flex">
            <!-- Recent Orders -->
            <div class="card card-table flex-fill">
                <div class="card-header">
                    <h4 class="card-title">Cooks List</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    {{-- <th>Reviews</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @isset($cooks)
                                    @foreach ($cooks as $cook)
                                    <tr>
                                        <td>
                                            <h2 class="table-avatar">
                                                <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{asset('backend/img/cook/'.$cook->images )}}" alt="User Image"></a>
                                                <a href="profile.html">{{ $cook->name }}</a>
                                            </h2>
                                        </td>
                                        <td>{{ $cook->email }}</td>
                                        <td>{{  $cook->phone  }}</td>
                                        {{-- <td>
                                            <i class="fe fe-star-o text-secondary"></i>
                                            <i class="fe fe-star-o text-secondary"></i>
                                            <i class="fe fe-star-o text-secondary"></i>
                                            <i class="fe fe-star-o text-secondary"></i>
                                            <i class="fe fe-star-o text-secondary"></i>
                                        </td> --}}

                                    </tr>
                                    @endforeach
                                @endisset

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /Recent Orders -->
            </div>
            <div class="col-md-6 d-flex">

               <!-- Feed Activity -->
                <div class="card  card-table flex-fill">
                <div class="card-header">
                    <h4 class="card-title">Customer List</h4>
                </div>
                <div class="card-body scroll">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>

                                    {{-- <th>Action</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @isset($customers)
                                @foreach ($customers as $customer)
                                <tr>
                                    <td>
                                        {{-- @dump($customer->name) --}}
                                        <h2 class="table-avatar">
                                            {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="Customer Image"></a> --}}
                                            <a href="#">{{ $customer->name }}</a>
                                        </h2>
                                    </td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->address[0]->address ?? ''}}</td>

                                    {{-- <td >
                                        <div class="actions">
                                            <a class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                <i class="fe fe-trash"></i> Delete
                                            </a>
                                        </div>
                                    </td> --}}
                                </tr>
                                @endforeach

                                @endisset

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /Feed Activity -->
            @endif
        </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger"  style="display: none">

                </div>
                <div class="alert alert-success"  style="display: none">

                </div>

                  <!-- Recent Activity Orders -->
                  <div class="card card-table">
                    <div class="card-header">
                        <h4 class="card-title">Current Activities List</h4>
                    </div>
                    <div class="card-body scroll">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
                                        <th>Customer Name</th>
                                        <th>Cooks Name</th>
                                        <th>
                                            <table class="sub" style="width: 100%">
                                                <tr>
                                                    <th style="width: 50%">Dish Name</th>
                                                    <th >Dish Section</th>
                                                    <th > Price</th>
                                                    <th >Dish Qty</th>
                                                </tr>
                                            </table>
                                        </th>
                                        {{-- <th>Order Price</th> --}}
                                        <th>Order Notes</th>
                                        @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                            <th>Order Status</th>
                                        @endif
                                        <th>Order Price</th>
                                        <th>Order Date</th>
                                        <th>Order Time</th>
                                        {{-- <th>Action </th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($currentActivities)
                                    @foreach ($currentActivities as $index=>$order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                <a href="#">{{ $order->users->name ?? ''}}</a>
                                            </h2>
                                        </td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/cook/') }}" alt="User Image"></a> --}}
                                                <a href="#">{{ $cook_name }}</a>
                                            </h2>
                                        </td>
                                        <td>



                                            <table class="sub" style=" width: 100%">
                                                @if (is_null($order->total_price))
                                                    @php
                                                    $dishes=$order->orderEntry
                                                    @endphp
                                                    @foreach ($dishes as $dish)
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <h2 class="table-avatar">
                                                                <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dish['images'] )[0]) }}" ></a>
                                                                <a href="#">{{ $dish['name_'.LaravelLocalization::getCurrentLocale()] }}</a>
                                                            </h2>

                                                        </td>
                                                        <td >{{ $dish['section_'.LaravelLocalization::getCurrentLocale()] }}</td>
                                                        <td >{{ $dish['price'] }}</td>
                                                        <td >{{ $dish['quantity'] }}</td>
                                                        {{-- <td>{{ $orderEntry['price'] * $orderEntry['qty']}}</td> --}}

                                                    </tr>
                                                    @endforeach
                                                @else
                                                @php
                                                $dishes=$order->orderEntry
                                                @endphp
                                                @for ($i = 0; $i <count($dishes); $i++)
                                                        @if (isset($dishes[$i]['user_id']))
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <h2 class="table-avatar">
                                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dishes[$i]['orderEntry']['images'] )[0]) }}" ></a>
                                                                    <a href="#">{{ $dishes[$i]['orderEntry']['name_'.LaravelLocalization::getCurrentLocale()] }}</a>
                                                                </h2>

                                                            </td>
                                                            <td >{{ $dishes[$i]['orderEntry']['section_'.LaravelLocalization::getCurrentLocale()] }}</td>
                                                            <td >{{ $dishes[$i]['orderEntry']['price'] }}</td>
                                                            <td >{{ $dishes[$i]['orderEntry']['quantity'] }}</td>
                                                            {{-- <td>{{ $orderEntry['price'] * $orderEntry['qty']}}</td> --}}

                                                        </tr>
                                                        @else
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <h2 class="table-avatar">
                                                                    {{-- <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dishes[$i]['images'] )[0]) }}" ></a> --}}
                                                                    <a href="#">{{ $dishes[$i]['name_'.LaravelLocalization::getCurrentLocale()]  ?? ''}}</a>
                                                                </h2>

                                                            </td>
                                                            <td >{{ $dishes[$i]['section_'.LaravelLocalization::getCurrentLocale()] ?? '' }}</td>
                                                            <td >{{ $dishes[$i]['price']  ?? '' }}</td>
                                                            <td >{{ $dishes[$i]['quantity']  ?? '' }}</td>
                                                            {{-- <td>{{ $orderEntry['price'] * $orderEntry['qty']}}</td> --}}

                                                        </tr>
                                                        @endif
                                                @endfor
                                                @endif
                                            </table>



                                        </td>
                                        <td>{{ $dish->notes ?? 'no notes' }}</td>
                                        @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                            <td>
                                                <span >
                                                @if ($order['status'] == 1 )
                                                    Pending
                                                @elseif($order['status'] == 2)
                                                    Accepted
                                                @elseif($order['status'] == 3)
                                                    In progress
                                                @elseif($order['status'] == 4)
                                                    Shipped
                                                @elseif($order['status'] == 5)
                                                    Delivered
                                                @elseif($order['status'] == 6)
                                                    Completed
                                                @elseif($order['status'] == 0)
                                                    Rejected
                                                @endif
                                                </span>
                                            </td>
                                        @endif
                                        <td>{{ $order->total_price }}</td>

                                        <td>{{ $order->date   }}</td>
                                        <td>{{ $order->time != null ? date('h:i A', strtotime($order->time ))  : ' ' }}</td>
                                        {{-- <td>
                                            <div class="actions">
                                                <a  href="#"  style="color: #f00"  onclick="reject({{ $order->id }} )" href="#"  id="{{  $order->id }}"> reject </a>
                                                <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">Accept</a>

                                            </div>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->
                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">orders List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
                                        <th>Customer Name</th>
                                        <th>Cooks Name</th>
                                        <th>
                                            <table class="sub" style=" width: 100%;border:none">
                                                <tr>
                                                    <th style="width: 50%;border:0 ">Dish Name</th>
                                                    <th>Dish Section</th>
                                                    <th> price</th>
                                                    <th>Dish Qty</th>
                                                </tr>
                                            </table>
                                        </th>
                                        {{-- <th>Order Price</th> --}}
                                        <th>Order Notes</th>
                                        <th>Order Status</th>
                                        <th>Order Price</th>
                                        <th>Order Date</th>
                                        <th>Order Time</th>
                                        {{-- <th>Action</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                   @isset($orders)
                                    @foreach ($orders as $index=>$order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                <a href="#">{{ $order->users->name ?? ' '}}</a>
                                            </h2>
                                        </td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/cook/') }}" alt="User Image"></a> --}}
                                                <a href="#">{{ $cook_name }}</a>
                                            </h2>
                                        </td>
                                        <td>
                                            <table class="sub" style=" width: 100%">
                                                @php
                                                $dishes=$order->orderEntry
                                                @endphp
                                                @for ($i = 0; $i <count($dishes); $i++)
                                                @if (isset($dishes[$i]['user_id']))
                                                <tr>
                                                    <td style="width: 50%">
                                                        <h2 class="table-avatar">
                                                            <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dishes[$i]['orderEntry']['images'] )[0]) }}" ></a>
                                                            <a href="#">{{ $dishes[$i]['orderEntry']['name_'.LaravelLocalization::getCurrentLocale()] }}</a>
                                                        </h2>

                                                    </td>
                                                    <td >{{ $dishes[$i]['orderEntry']['section_'.LaravelLocalization::getCurrentLocale()] }}</td>
                                                    <td >{{ $dishes[$i]['orderEntry']['price'] }}</td>
                                                    <td >{{ $dishes[$i]['orderEntry']['quantity'] }}</td>
                                                    {{-- <td>{{ $orderEntry['price'] * $orderEntry['qty']}}</td> --}}

                                                </tr>
                                                @else
                                                <tr>
                                                    <td style="width: 50%">
                                                        <h2 class="table-avatar">
                                                            {{-- <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dishes[$i]['images'] )[0]) }}" ></a> --}}
                                                            <a href="#">{{ $dishes[$i]['name_'.LaravelLocalization::getCurrentLocale()] ?? '' }}</a>
                                                        </h2>

                                                    </td>
                                                    <td >{{ $dishes[$i]['section_'.LaravelLocalization::getCurrentLocale()]?? ''}}</td>
                                                    <td >{{ $dishes[$i]['price'] ?? '' }}</td>
                                                    <td >{{ $dishes[$i]['quantity'] ?? ''}}</td>
                                                    {{-- <td>{{ $orderEntry['price'] * $orderEntry['qty']}}</td> --}}

                                                </tr>
                                                @endif
                                        @endfor

                                             </table>
                                        </td>
                                        <td>{{ $dish->notes ?? 'no notes' }}</td>
                                        <td>
                                            <span >
                                                @if ($order['status'] == 1 )
                                                    Pending
                                                @elseif($order['status'] == 2)
                                                    Accepted
                                                @elseif($order['status'] == 3)
                                                    In progress
                                                @elseif($order['status'] == 4)
                                                    Shipped
                                                @elseif($order['status'] == 5)
                                                    Delivered
                                                @elseif($order['status'] == 6)
                                                    Completed
                                                @elseif($order['status'] == 0)
                                                    Rejected
                                                @endif
                                            </span>
                                        </td>
                                        <td>{{ $order->total_price }}</td>

                                       <td>{{ $order->date   }}</td>
                                       <td>{{ $order->time != null ? date('h:i A', strtotime($order->time ))  : ' ' }}</td>
                                        {{-- <td>
                                            <div class="actions">
                                                @if ($order['status'] == 2)
                                                    <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">In Progress</a>
                                                @endif
                                                <a class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> Delete</a>
                                            </div>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                   @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->



            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->


@endsection
@push('scripts')
    <script>
function reject(order_id ){
        $.ajax({
            type: 'GET',
            url: '/{{ Config::get('app.locale') }}/dashboard/order/reject/'+order_id,

            success:function(data){
                if (data.status == 0) {
                    $('.alert-danger').css('display','block');
                    $('.alert-danger').text('Order Rejected ');

                    setTimeout(function() {
                        $('.alert').fadeOut('fast');
                        location.reload();
                    }, 2000);
                }
            },
            error:function(data){
            }
        });
        location.reload();

};
function status(order_id){
            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/dashboard/order/'+order_id,

                success:function(data){
                    if (data.status == 2) {
                        $('.alert-success').text('Order Accepted ')
                    }else if (data.status == 3){
                        $('.alert-success').text('Order In Progressive ')
                    }
                    $('.alert-success').css('display','block');

                    setTimeout(function() {
                        $('.alert').fadeOut('fast');
                        location.reload();
                    }, 2000);
                },
                error:function(data){
                }
            });
        };
</script>
@endpush
