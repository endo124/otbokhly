@extends('backend.layouts.app')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css">
<style>
    button.accordion {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

    button.accordion.active, button.accordion:hover {
        background-color: #ddd;
    }

    button.accordion:after {
        content: '\02795';
        font-size: 13px;
        color: #777;
        float: right;
        margin-left: 5px;
    }

    button.accordion.active:after {
        content: "\2796";
    }

    div.panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: 0.6s ease-in-out;
        opacity: 0;
    }

    div.panel.show {
        opacity: 1;
        max-height: 100%;
    }
    </style>
@section('content')

<div class="map_container" id="map_container"></div>



@endsection


@push('scripts')

<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-database.js"></script>
<script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script>

<script>
var firebaseConfig = {
  apiKey: "AIzaSyCX54Ej_pzEV3lwwK9QNmpcbMUmc-mAxug",
  authDomain: "logista-282218.firebaseapp.com",
  databaseURL: "https://logista-282218.firebaseio.com",
  projectId: "logista-282218",
  storageBucket: "logista-282218.appspot.com",
  messagingSenderId: "747873953165",
  appId: "1:747873953165:web:ff0d3b01fa6dd2671c89c4",
  measurementId: "G-HE8VE9GSM3"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.database();
    $( document ).ready(function() {
    $.get('https://nominatim.openstreetmap.org/search?q='+'eg'+'&format=json&polygon=1&addressdetails=1', function(data){
    var map = new ol.Map({
    target: 'map_container',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
        center: ol.proj.fromLonLat([data[0]['lat'],data[0]['lon']]),
      zoom: 4
    })
  });
  db.ref('users/TlAqmD51bwfNlnRU0EPz8fTf7cv2/drivers').on("value", (snapshot) => {


    snapshot.forEach((driverSnapshot) => {
        if(driverSnapshot.val().currantLatitude)
        {
            console.log(driverSnapshot.val())
            var layer = new ol.layer.Vector({
     source: new ol.source.Vector({
         features: [
             new ol.Feature({
                 geometry: new ol.geom.Point(ol.proj.fromLonLat([driverSnapshot.val().currantLongitude,driverSnapshot.val().currantLatitude]))
             })
         ]
     }),
     style: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 1],
          src:'http://ahocevar.github.io/gmaps-to-ol3-demo/resources/img/marker-blue.png'
        })
 })
 });
 map.addLayer(layer);
        }
    })
    });
});
})


</script>
@endpush
