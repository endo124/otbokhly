@extends('backend.layouts.app')

@push('style')
    <style>


        .avatar-sm {
         width: 4.5rem !important;
        }
        .sub tr th,.sub tr td{
            border: 0 !important
        }
        .card{
            min-height: 200px !important;
        }
        .action-btn{
            padding: 5px 8px;
            margin: 0 3px !important;
            border: 1px solid
        }


    .link_reject{
        border: 1px solid #f00;
    padding: 3px 5px ;
    margin: 0 8px 0 0 !important;
    }
    .null,.undefined{
        visibility: hidden;
    }
    </style>
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css    ">
@endpush

@section('content')




 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of orders</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">order</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('order')) --}}
                {{-- <div class="col-sm-12 col">
                    {{-- <a href="#Add_orders" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('order') ? 'disabled' : '' }}" >Add</a> --}}
                    {{-- <a href="#Add_orders" data-toggle="modal" class="btn btn-otbokhly float-right mt-2   " >Add</a>
                </div>  --}}
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->

        <div class="row">



            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($orders) }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">orders</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($orders) }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>0</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">late</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: 0%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($onduty) ?? ' ' }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">on duty drivers</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($onduty) ?? ' ' }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($available)?? ' ' }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">available drivers</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($available) ?? ' ' }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

                <div class="row" >
                    <div class="alert alert-danger"  style="display: none">

                    </div>
                    <div class="alert alert-success"  style="display: none">

                    </div>
                    <div class="col-md-12 d-flex">

                    <!-- Recent Activity Orders -->
                    <div class="card card-table w-100">
                        <div class="card-header">
                            <h4 class="card-title">Current Activities List</h4>
                        </div>
                        <div class="card-body scroll">
                            <div class="table-responsive">
                                <table class="table table-hover table-center mb-0" id="refresh">
                                    <thead>
                                        <tr>
                                            <th> Order Id</th>
                                            <th>Customer Name</th>
                                            <th>Cooks Name</th>
                                            {{-- <th>
                                                <table class="sub" style="width: 100%">
                                                    <tr>
                                                        <th style="width: 50%"> Name</th>
                                                        <th > Section</th>
                                                        <th > price</th>
                                                        <th > Qty</th>
                                                    </tr>
                                                </table>
                                            </th> --}}
                                            {{-- <th>Order Price</th> --}}
                                            <th> Notes</th>
                                            @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                                <th>Order Status</th>
                                            @endif
                                            <th style="padding-left:55px"> Price</th>
                                            <th> Date</th>
                                            <th> Time</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($currentActivities)
                                        @foreach ($currentActivities as $index=>$order)
                                        @php
                                            $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                            $cook_name=$cook->name;
                                            $order_date_time=$order->date." ".$order->time;
                                            // $rejection_time=\App\Models\Setting::first()->rejection;

                                        @endphp

{{-- {{strtotime("now")  >= strtotime($order_date_time)+(60*(int)$rejection_time) }} --}}
                                        <tr>
                                            <td>{{ $order->id }}</td>
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                    <a href="#" id="customer_{{ $order->id }}">{{ $order->users->name ?? '' }}</a>
                                                </h2>
                                            </td>
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/cook/') }}" alt="User Image"></a> --}}
                                                    <a href="#" id="cook_{{$order->id}}">{{ $cook_name }}</a>
                                                </h2>
                                            </td>
                                            {{-- <td>
                                                <table class="sub" style=" width: 100%">
                                                    @if (is_null($order->total_price))
                                                        @php
                                                        $dishes=$order->orderEntry
                                                        @endphp
                                                        @foreach ($dishes as $dish)
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <h2 class="table-avatar">
                                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dish['images'] )[0]) }}" ></a>
                                                                    <a href="#">{{ $dish['name_'.LaravelLocalization::getCurrentLocale()] }}</a>
                                                                </h2>

                                                            </td>
                                                            <td >{{ $dish['section_'.LaravelLocalization::getCurrentLocale()] }}</td>
                                                            <td >{{ $dish['price'] }}</td>
                                                            <td >{{ $dish['quantity'] }}</td>

                                                        </tr>
                                                        @endforeach
                                                    @else
                                                    @php
                                                    $dishes=$order->orderEntry
                                                    @endphp
                                                    @for ($i = 0; $i <count($dishes); $i++)
                                                            @if (isset($dishes[$i]['user_id']))
                                                            <tr>
                                                                <td style="width: 50%">
                                                                    <h2 class="table-avatar">
                                                                        <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dishes[$i]['orderEntry']['images'] )[0]) }}" ></a>
                                                                        <a href="#">{{ $dishes[$i]['orderEntry']['name_'.LaravelLocalization::getCurrentLocale()] }}</a>
                                                                    </h2>

                                                                </td>
                                                                <td >{{ $dishes[$i]['orderEntry']['section_'.LaravelLocalization::getCurrentLocale()] }}</td>
                                                                <td >{{ $dishes[$i]['orderEntry']['price'] }}</td>
                                                                <td >{{ $dishes[$i]['orderEntry']['quantity'] }}</td>

                                                            </tr>
                                                            @else
                                                            <tr>
                                                                <td style="width: 50%">
                                                                    <h2 class="table-avatar">
                                                                        <a href="#">{{ $dishes[$i]['name_'.LaravelLocalization::getCurrentLocale()] ?? '' }}</a>
                                                                    </h2>

                                                                </td>
                                                                <td >{{ $dishes[$i]['section_'.LaravelLocalization::getCurrentLocale()]?? ''}}</td>
                                                                <td >{{ $dishes[$i]['price'] ?? '' }}</td>
                                                                <td >{{ $dishes[$i]['quantity'] ?? ''}}</td>

                                                            </tr>
                                                            @endif
                                                    @endfor
                                                    @endif
                                                </table>
                                            </td> --}}
                                            <td>{{ $order->note ?? 'no notes' }}</td>
                                            @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                                <td>
                                                    <span id="order_status_{{$order->id }}">
                                                    @if ($order['status'] == 1 )
                                                        Pending
                                                    @elseif($order['status'] == 2)
                                                        Accepted
                                                    @elseif($order['status'] == 3)
                                                        In progress
                                                    @elseif($order['status'] == 4)
                                                        Shipped
                                                    @elseif($order['status'] == 5)
                                                        Delivered
                                                    @elseif($order['status'] == 6)
                                                        Completed
                                                    @elseif($order['status'] == 0)
                                                        Rejected
                                                    @endif
                                                    </span>
                                                </td>
                                            @endif
                                            <td>{{ $order->total_price }}</td>
                                            <td>{{ $order->date   }}</td>
                                            <td>{{ $order->time != null ? date('h:i A', strtotime($order->time ))  : ' ' }}</td>
                                            <td>
                                                <div class="actions">
                                                    <a id="{{ $order->id }}" class="link_show bg-success-light mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details" >
                                                        <i class="fe fe-eye"></i> @lang('site.View Details')
                                                    </a>
                                                    <a  class="action-btn link_reject" style="color: #f00" id="{{ $order->id }}" data-toggle="modal" href="#reject_modal" data-url=""   > reject </a>
                                                    <a class="action-btn"  style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">Accept</a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endisset

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /Recent  Activity Orders -->
                    </div>
                </div>

                 <!-- Recent Orders -->
                 <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">orders List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0" id="example">
                                <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Customer Name</th>
                                        <th>Cooks Name</th>
                                        {{-- <th>
                                            <table class="sub" style=" width: 100%;border:none">
                                                <tr>
                                                    <th style="width: 50%;border:0 "> Name</th>
                                                    <th> Section</th>
                                                    <th style="padding-left:55px"> price</th>
                                                    <th> Qty</th>
                                                </tr>
                                            </table>
                                        </th> --}}
                                        {{-- <th>Order Price</th> --}}
                                        <th> Notes</th>
                                        <th> Status</th>
                                        <th> Total Price</th>
                                        <th> Date</th>
                                        <th> Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @isset($orders)
                                    @foreach ($orders as $index=>$order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                <a href="#"  id="customer_{{ $order->id }}">{{ $order->users->name ?? '' }}</a>
                                            </h2>
                                        </td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/cook/') }}" alt="User Image"></a> --}}
                                                <a href="#" id="cook_{{$order->id}}">{{ $cook_name }}</a>
                                            </h2>
                                        </td>
                                        {{-- <td>
                                            <table class="sub" style=" width: 100%">
                                                @php
                                                $dishes=$order->orderEntry
                                                @endphp
                                                @for ($i = 0; $i <count($dishes); $i++)
                                                @if (isset($dishes[$i]['user_id']))
                                                <tr>
                                                    <td style="width: 50%">
                                                        <h2 class="table-avatar">
                                                            <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$dishes[$i]['orderEntry']['images'] )[0]) }}" ></a>
                                                            <a href="#">{{ $dishes[$i]['orderEntry']['name_'.LaravelLocalization::getCurrentLocale()] }}</a>
                                                        </h2>

                                                    </td>
                                                    <td >{{ $dishes[$i]['orderEntry']['section_'.LaravelLocalization::getCurrentLocale()] }}</td>
                                                    <td >{{ $dishes[$i]['orderEntry']['price'] }}</td>
                                                    <td >{{ $dishes[$i]['orderEntry']['quantity'] }}</td>

                                                </tr>
                                                @else
                                                <tr>
                                                    <td style="width: 50%">
                                                        <h2 class="table-avatar">
                                                            <a href="#">{{ $dishes[$i]['name_'.LaravelLocalization::getCurrentLocale()] ?? '' }}</a>
                                                        </h2>

                                                    </td>
                                                    <td >{{ $dishes[$i]['section_'.LaravelLocalization::getCurrentLocale()]?? ''}}</td>
                                                    <td >{{ $dishes[$i]['price'] ?? '' }}</td>
                                                    <td >{{ $dishes[$i]['quantity'] ?? ''}}</td>

                                                </tr>
                                                @endif
                                                @endfor

                                             </table>
                                        </td> --}}
                                        <td>{{ $order->note ?? 'no notes' }}</td>
                                        <td>
                                            <span id="order_status_{{$order->id }}">
                                                @if ($order['status'] == 1 )
                                                    Pending
                                                @elseif($order['status'] == 2)
                                                    Accepted
                                                @elseif($order['status'] == 3)
                                                    In progress
                                                @elseif($order['status'] == 4)
                                                    Shipped
                                                @elseif($order['status'] == 5)
                                                    Delivered
                                                @elseif($order['status'] == 6)
                                                    Completed
                                                @elseif($order['status'] == 0)
                                                    Rejected
                                                @endif
                                            </span>
                                        </td>
                                        <td>{{ $order->total_price }}</td>
                                       <td>{{ $order->date   }}</td>
                                       <td>{{ $order->time != null ? date('h:i A', strtotime($order->time ))  : ' ' }}</td>
                                        <td>
                                            <div class="actions">
                                                <a id="{{ $order->id }}" class="link_show bg-success-light mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details" >
                                                    <i class="fe fe-eye"></i> @lang('site.View Details')
                                                </a>
                                                @if ($order['status'] == 2)
                                                    <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">In Progress</a>
                                                @elseif($order['status'] == 3)
                                                <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">Shipped</a>
                                                @endif
                                                @if ($order['status'] == 0  || $order['status'] == 9 )
                                                <a class="link_delete" id="{{ $order->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> Delete</a>
                                                @endif

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                   @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->
    </div>
</div>


  <!-- view order Modal -->

<div class="modal fade" id="edit_orders_details" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('site.Order Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="view">

            </div>
        </div>
    </div>
</div>


<!-- /Page Wrapper -->
  <!-- Add Modal -->
  <div class="modal fade" id="Add_orders" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')

                <form method="POST" action="{{ url('dashboard/order') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row form-row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>phone</label>
                                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>password</label>
                                <input type="password" name="password" class="form-control" >
                            </div>
                        </div>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                aria-selected="true">Home</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                                aria-selected="false">Profile</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                                aria-selected="false">Contact</a>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">Raw denim you
                             </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">Food truck fixie
                             </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">Etsy mixtape
                              </div>
                          </div>
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->



    <!-- Reject Modal -->
      <div class="modal fade" id="reject_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reject</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Reject</h4>
                        <p class="mb-4">Are you sure want to Reject?</p>

                        <div class="text-center">
                            <form  id="formReject" action="" method="post" style="display: inline">
                                @csrf
                                @method('post')
                                <input type="text" name="id" id="hiddenid" hidden>
                                <input class="form-control mb-2" type="text" name="note" placeholder="please enter the reason">
                                <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Yes </button>
                            </form>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Reject Modal -->


@endsection


@push('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>

<script>
    $(document).ready(function() {

        // Setup - add a text input to each footer cell
        $('#refresh thead tr').clone(true).appendTo( '#refresh thead' );
        $('#refresh thead tr:eq(1) th').not(":last").each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder=" '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        });
        var lang='{{ Config::get('app.locale') }}';
        if (lang == 'ar') {
            var url = '//cdn.datatables.net/plug-ins/1.10.24/i18n/Arabic.json';
        }
        var table = $('#refresh').DataTable( {

            language: {
                url: url
            },
            responsive: true,
            orderCellsTop: true,
            fixedHeader: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );


        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').not(":last").each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder=" '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        });
        var lang='{{ Config::get('app.locale') }}';
        if (lang == 'ar') {
            var url = '//cdn.datatables.net/plug-ins/1.10.24/i18n/Arabic.json';
        }
        var table = $('#example').DataTable( {

            language: {
                url: url
            },
            responsive: true,
            // orderCellsTop: true,
            fixedHeader: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );


    });
</script>
    <script>

$('.link_show').on('click',function(){
            $('#view').html('');
            $('#body').html('');
            var order_id=$(this).attr('id');
            var customer=$('#customer_'+order_id).html()
            var cook=$('#cook_'+order_id).html()
            // var payment_method=$('#payment_method_'+order_id).html()
            // var payment_status=$('#payment_status_'+order_id).html()
            // var delivery_status=$('#delivery_status_'+order_id).html()
            var order_status=$('#order_status_'+order_id).html()
            $.ajax(
                {url: '/{{ Config::get('app.locale') }}/dashboard/order/orderdetails/'+order_id,
                 success: function(result){
                    var data= result.order.orderEntry;
                    console.log(data)
                    var details='<div class="row form-row">'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Number : '+result.order.id+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Customer Name : '+customer+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Cook Name : '+cook+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<table class="table table-borderd table-hover">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Dish Name</th>'+
                                                        // '<th>Dish Section</th>'+
                                                        '<th>Dish price</th>'+
                                                        '<th>Amount</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody id="body">'+

                                                '</tbody>'+
                                            '</table>'+

                                            '<table class="table table-borderd table-hover">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Addon Name</th>'+
                                                        // '<th>Dish Section</th>'+
                                                        '<th>Addon price</th>'+
                                                        '<th>Amount</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody id="body_addon">'+

                                                '</tbody>'+
                                            '</table>'+


                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Location : '+result.order.address+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    // '<div class="col-12 col-sm-12">'+
                                    //     '<div class="form-group">'+
                                    //         '<label>Total Price : '+result.order.total_price+'</label>'+
                                    //     '</div>'+
                                    // '</div>'+

                                    // '<div class="col-12 col-sm-12">'+
                                    //     '<div class="form-group">'+
                                    //         '<label>Delivery Status : '+delivery_status+'</label>'+
                                    //     '</div>'+
                                    // '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Order Status : '+order_status+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>';
                                var fees='<div class="col-12 col-sm-12"  style="    padding: 0;">'+
                                    '<div class="form-group">'+
                                        '<label>Delivery Fees : '+ (Number(result.order.delivery_fees)).toFixed(2)+'</label>'+
                                    '</div>'+
                                '</div>';
                                // '<div class="col-12 col-sm-12"  style="    padding: 0;">'+
                                //     '<div class="form-group">'+
                                //         '<label>Delivery Taxes : '+ (Number(result.order.delivery_taxes)).toFixed(2)+'</label>'+
                                //     '</div>'+
                                // '</div>'

                                var discount='<div class="col-12 col-sm-12"  style="    padding: 0;">'+
                                    '<div class="form-group">'+
                                        '<label>Discount value : '+ (Number(result.order.discount_price)).toFixed(2)+'</label>'+
                                    '</div>'+
                                '</div>';
                                var total='<div class="col-12 col-sm-12" style=" padding: 0;display :{{ auth()->guard('admin')->user()->roles[0]->name !='cook' ? 'block' : 'none' }}">'+
                                    '<div class="form-group">'+
                                        '<label >Total Price : <span id="price_total">'+(Number(result.order.total_price)).toFixed(2)+'</sapn></label>'+
                                    '</div>'+
                                '</div>';
                                $('#view').append(details);

                                var role=$('#role').val();
                                if(role != 'cook' && result.order.delivery_fees > 0){
                                    $('#view').append(fees);

                                }
                                if(role != 'cook' && result.order.discount_price > 0){
                                    $('#view').append(discount);

                                }
                                $('#view').append(total);


                      for (let i = 0; i < data.length; i++) {


                        for (let j = 0; j < data[i].addons.length; j++) {

                            var body_Addon= '<tr>'+
                                    '<td>'+data[i].addons[j].add_{{ Config::get('app.locale') }}+'</td>'+
                                    // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                                    '<td>'+data[i].addons[j].add_price+'</td>'+
                                    '<td>'+data[i].addons[j].qty+'</td>'+

                                '</tr>';
                            $('#body_addon').append(body_Addon)  ;

                        }


                       var body= '<tr>'+
                            '<td>'+data[i].name_{{ Config::get('app.locale') }}+'</td>'+
                            // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                            '<td>'+data[i].price+'</td>'+
                            '<td>'+data[i].quantity+'</td>'+

                        '</tr>';
                    $('#body').append(body)  ;


                      }


                }
            });
        });



        @if (count($errors) > 0)
                $('#Add_orders').modal('show');
        @endif

        $('.link_delete').on('click',function(){
             var order=$(this).attr('id');
            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/order/'+order)

        });

        function status(order_id){
                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/dashboard/order/'+order_id,

                    success:function(data){
                        //    document.getElementById(order_id).innerHTML= data.status;

                        if (data.status == 2) {
                            $('.alert-success').text('Order Accepted ')
                        }else if (data.status == 3){
                            $('.alert-success').text('Order In Progressive ')
                        }
                        $('.alert-success').css('display','block');

                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                            location.reload();
                        }, 2000);
                    },
                    error:function(data){
                    }
                });
                // location.reload();

        };

        $('.link_reject').on('click',function(){
             var order=$(this).attr('id');
             $('#hiddenid').val(order);
            $('#formReject').attr('action','/{{ Config::get('app.locale') }}/dashboard/order/reject')

        });

        // setInterval(function() {

        //         var X=getElementById('rejection').value;
        //         alert(X);

        //         $.ajax({
        //             type: 'GET',
        //             url: '/{{ Config::get('app.locale') }}/dashboard/order/'+order_id,

        //             success:function(data){
        //                 //    document.getElementById(order_id).innerHTML= data.status;

        //                 if (data.status == 2) {
        //                     $('.alert-success').text('Order Accepted ')
        //                 }else if (data.status == 3){
        //                     $('.alert-success').text('Order In Progressive ')
        //                 }
        //                 $('.alert-success').css('display','block');

        //                 setTimeout(function() {
        //                     $('.alert').fadeOut('fast');
        //                     location.reload();
        //                 }, 2000);
        //             },
        //             error:function(data){
        //             }
        //         });

        // }, 1000 * 60 * X);











        // function reject(order_id){

        //         $.ajax({
        //             type: 'GET',
        //             url: '/{{ Config::get('app.locale') }}/dashboard/order/reject/'+order_id,

        //             success:function(data){
        //             //    document.getElementById(order_id).innerHTML= data.status;
        //             if (data.status == 0) {
        //                 $('.alert-danger').css('display','block');
        //                 $('.alert-danger').text('Order Rejected ');

        //                 setTimeout(function() {
        //                     $('.alert').fadeOut('fast');
        //                     location.reload();
        //                 }, 2000);
        //             }
        //             },
        //             error:function(data){
        //             }
        //         });

        // };

    </script>
@endpush
