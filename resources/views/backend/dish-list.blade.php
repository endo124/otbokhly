@extends('backend.layouts.app')
<style>
.dish-type  li{
    list-style: none;
    float: left;
    margin-left:10px
}

#addRow{
    display: hidden;
}
#addRow:first-child {
display: block;

}
#new_addon2{
    margin-left: 61px;
    margin-top: 6px;
    width: 100%;
}
.fa-trash-alt{
    margin-left: 5px
}
</style>

@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of Dishes</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Dish</li>
                    </ul>
                </div>
                <div class="col-sm-12 col">
                    <a href="#Add_Dish" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >Add</a>
                </div>
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">dish List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> Name</th>
                                        <th> Images</th>
                                        <th> Cook</th>
                                        <th> Section</th>
                                        <th>Cusine</th>
                                        <th>Price</th>
                                        {{-- <th>Reviews</th> --}}
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if($dishes)
                                    @foreach ($dishes as $dish)


                                        <tr>
                                            <td>
                                                <h2 >{{ $dish->name }}</h2>
                                            </td>
                                            <td>

                                                @php
                                                $images= explode("__",$dish->images);
                                                @endphp
                                                @foreach ($images as $img)
                                                    <img src=" {{ asset('/backend/img/dishes/'.$img) }}" width=50 alt="">
                                                @endforeach
                                            </td>
                                            <td>{{ $dish->users->name}}</td>
                                            <td>{{ $dish->sections->name ?? ' ' }}</td>
                                            <td>{{ $dish->cusines[0]->name ??  ' ' }}</td>
                                            @php
                                            $price=$dish->portions_price;

                                            if($price[0] !=null){
                                                $min=$price[0];
                                            }elseif ($price[1] !=null) {
                                                $min=$price[1];
                                            }else{
                                                $min=$price[2];
                                            }
                                            foreach ($price as $pr) {

                                            if($pr !=null){
                                                if ($min > $pr) {
                                                $min =$pr;
                                            }
                                            }
                                            }
                                        @endphp
                                        {{-- @dump($dish->portions_price) --}}
                                            <td>start from : {{ $min}}</td>
                                            {{-- <td>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                            </td> --}}
                                            <td >
                                                {{-- @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                                    <div class="actions">
                                                        <a  class="link_update bg-success-light mr-2 disabled" data-toggle="modal"  href="#edit_dishes_details" >
                                                            <i class="fe fe-pencil"></i> Edit
                                                        </a>
                                                        <a id="{{ $dish->id }}" class="link_delete disabled" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00" >
                                                            <i class="fe fe-trash"></i> Delete
                                                        </a>
                                                    </div>
                                                @else --}}
                                                <div class="actions">
                                                    <a id="{{ $dish->id }}" class="link_update bg-success-light mr-2" href="{{ route('dish.edit', $dish->id) }}" >
                                                        <i class="fe fe-pencil"></i> Edit
                                                    </a>
                                                    <a id="{{ $dish->id }}" class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        <i class="fe fe-trash"></i> Delete
                                                    </a>
                                                </div>
                                                {{-- @endif --}}
                                            </td>
                                        </tr>
                                        <!-- Edit Details Modal -->
                                        {{-- <div class="modal fade" id="edit_dishes_details{{ $dish->id }}" aria-hidden="true" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document" >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit dishes</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form id="formUpdate" method="post" action="" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('put')


                                                            <div class="row form-row">
                                                                @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                                                <div class="col-12 col-sm-12 mb-4">

                                                                    <label>Cooks : </label>

                                                                    <select  name="cook_name"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                        @foreach ($cooks as $cook)
                                                                        <option {{ $cook->id== $dish->users->id? 'selected' :' ' }}  value="{{ $cook->id }}">{{ $cook->name }}</option>
                                                                    @endforeach
                                                                    </select>
                                                                </div>
                                                                @endif
                                                                @foreach (config('translatable.locales') as $locale)
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.dishname')</label>
                                                                        <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ $dish->translate($locale)->name ??old($locale.'.name') }}">
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div  class="dropzone dz-clickable" >
                                                                            <div class="dz-default dz-message">
                                                                            <label>Dish Images : </label>
                                                                                <br>
                                                                                <input class="box__file" type="file" name="files[]" id="file1" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  multiple  />
                                                                            </div>
                                                                        </div>
                                                                        <div class="upload-wrap">
                                                                            <div class="upload-image float-left" id="upload-image" style="display:inline-block">
                                                                                @php
                                                                                    $dishes=explode('__',$dish->images)
                                                                                @endphp
                                                                            @foreach ($dishes as $img)
                                                                            <img id='img_src' width=50 src="{{ asset('/backend/img/dishes/'.$img) }}">
                                                                            @endforeach
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-12 col-sm-12 mt-4">
                                                                    <div class="form-group row">
                                                                        <div class="col-9 col-sm-9">
                                                                        <label>Cusines : </label>
                                                                            <select name="cusine"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                                                @foreach ($cusines as $cusine)
                                                                                    <option {{ (in_array($cusine->id, $dish->cusines->pluck('id')->toArray()))? 'selected' :' ' }}  value="{{ $cusine->id }}" >{{ $cusine->name }}</option>
                                                                                @endforeach

                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group row">
                                                                        <div class="col-9 col-sm-9">
                                                                        <label>Categories : </label>
                                                                            <select id="category" name="category"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                                                <option value="">select</option>
                                                                                @foreach ($categories as $category)
                                                                                    @isset($dish->categories->id )
                                                                                        <option {{ $category->id == $dish->categories->id ? 'selected' :' ' }} value="{{ $category->id ??' '}}">{{ $category->name??'' }}</option>
                                                                                    @endisset
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group row" id="addon_section2"  >
                                                                        @if (count($dish->addons) >= 1)
                                                                        @for ($i = 0; $i < count($dish->addons); $i++)
                                                                            <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                                                <label>Addons : </label>
                                                                                    <a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a>
                                                                                    <a onclick="addRoow()"  class="float-right mt-2" style="font-size: medium;">+</a>
                                                                                <select class="addons select_addon"   name="addons[]"    style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                                                    <option >select</option>
                                                                                    @foreach ($addons as $addon)
                                                                                        <option value="{{ $addon->id }}" {{ (in_array($addon->id, $dish->addons->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $addon->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <div  id="new_addon2" style="margin-left: 60px; ">
                                                                                    <select  name="addon_conditions[]"      style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" required>
                                                                                        <option value="1" {{ $dish->addons[$i]->pivot->condition == '1' ? 'selected' : '' }} >required</option>
                                                                                        <option value="0"  {{ $dish->addons[$i]->pivot->condition == '0' ? 'selected' : '' }}>optional</option>
                                                                                    </select>
                                                                                    <p>
                                                                                        <span >max</span><input type="text" value="{{ $dish->addons[$i]->pivot->max }}" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                        <span>min</span><input type="text" value="{{ $dish->addons[$i]->pivot->min }}" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                    </p>

                                                                                </div>
                                                                            </div>
                                                                        @endfor
                                                                        @else
                                                                        <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                                            <label>Addons : </label><a onclick="removeDiv(this)" class="float-right mt-2" style=";margin-right:3px;font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a><a onclick="addRoow()" class="float-right mt-2" style="font-size: medium;">+</a>
                                                                            <select class="addons select_addon"    name="addons[]"      style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                                                <option >select</option>
                                                                                @foreach ($addons as $addon)
                                                                                    <option value="{{ $addon->id }}" {{ (in_array($addon->id, $dish->addons->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $addon->name }}</option>
                                                                                @endforeach
                                                                            </select>

                                                                            <div  id="new_addon2" >
                                                                                <select  name="addon_conditions[]"  required style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                                                    <option value="1">required</option>
                                                                                    <option value="0">optional</option>
                                                                                </select>
                                                                                <p>
                                                                                    <span >max</span><input type="text" value="{{ old('max[]') }}" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                    <span>min</span><input type="text" value="{{ old('min[]') }}" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                </p>

                                                                            </div>
                                                                        </div>
                                                                        @endif

                                                                    </div>
                                                                </div>

                                                                 <div class="col-12 col-sm-12" id="AddonSection">
                                                                 </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>portions available : </label>
                                                                        <ul class="nav nav-tabs" >
                                                                            <li class="nav-item">
                                                                              <a class="nav-link active"  data-toggle="tab" href="#hoome{{ $dish->id }}" >Large</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                              <a class="nav-link"  data-toggle="tab" href="#proofile{{ $dish->id }}" >Medium</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                              <a class="nav-link"  data-toggle="tab" href="#coontact{{ $dish->id }}" >Small</a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content">

                                                                            <div class="tab-pane fade show active" id="hoome{{ $dish->id }}" >
                                                                                <input type="text" name="portions_available[]" class="form-control "  value="{{ $dish->portions_available[0] }}" style="width: 20%;display:inline"> <span class="mr-5"> persons</span>
                                                                                <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[0] }}" style="width: 20%;display:inline"> <span> EGP</span>
                                                                            </div>
                                                                            <div class="tab-pane fade" id="proofile{{ $dish->id }}" >
                                                                                <input type="text" name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[1] }}" style="width: 20%;display:inline"> <span  class="mr-5"> persons</span>
                                                                                <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[1] }}" style="width: 20%;display:inline"> <span> EGP</span>
                                                                            </div>
                                                                            <div class="tab-pane fade" id="coontact{{ $dish->id }}" >
                                                                                <input type="text" name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[2]  }}" style="width: 20%;display:inline"> <span class="mr-5"> persons</span>
                                                                                <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[2] }}" style="width: 20%;display:inline"> <span> EGP</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <label>Section : </label>
                                                                    <ul class="dish-type">
                                                                    @foreach ($sections as $section)
                                                                    <li>
                                                                        <input type="radio"  name="section" value="{{ $section->id }}" {{$dish->section_id == $section->id ?'checked':' ' }} >
                                                                        <label for="f-option">{{ $section->name }}</label>
                                                                        <div class="check"></div>
                                                                    </li>
                                                                    @endforeach
                                                                    </ul>
                                                                </div>
                                                                <div class="col-12 col-sm-12 mb-4">
                                                                    <label>availability : </label>
                                                                    <ul class="dish-type">
                                                                    <li>
                                                                        <input type="radio" id="f-option" name="dish_available" value="1" {{$dish->available ==1 ? 'checked':'' }}>
                                                                        <label for="f-option">availabe</label>
                                                                        <div class="check"></div>
                                                                    </li>
                                                                    <li>
                                                                        <input type="radio" id="" name="dish_available" value="0" {{$dish->available ==0 ? 'checked':'' }}>
                                                                        <label for="f-option">not available</label>
                                                                        <div class="check"></div>
                                                                    </li>
                                                                    </ul>
                                                                </div>

                                                                @foreach (config('translatable.locales') as $locale)

                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.time_of_preparation')</label>
                                                                        <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{$dish->translate($locale)->main_ingredients ?? old($locale.'.main_ingredients') }}">
                                                                    </div>
                                                                </div>
                                                                @endforeach

                                                                @foreach (config('translatable.locales') as $locale)

                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.description')</label>
                                                                        <textarea name="{{ $locale }}[info]" style="width: 90%;display: block;"  rows="3">{{$dish->translate($locale)->info ?? old($locale.'.info') }}</textarea>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                            <button type="submit" class="btn btn-otbokhly btn-block">Save Changes</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </div> --}}
                                        <!-- /Edit Details Modal -->
                                    @endforeach
                                  @endif

                                </tbody>
                            </table>
                            <div class="row justify-content-center" >
                                 {{-- {{ $dishes->links() }} --}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

  <!-- Add Modal -->
  <div class="modal fade" id="Add_Dish" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Dish</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')

                <form method="POST" action="{{ route('dish.store')}}" enctype="multipart/form-data">
                    @csrf

                    <div class="row form-row">
                        @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                            <div class="col-12 col-sm-12 mb-4">

                                <label>Cooks : </label>
                                <select id="Vendors" name="Vendor_id" class="selectpicker"  data-live-search="true"   style="width:50%;paddind:2px">
                                    <option value="">select cook</option>
                                    @foreach ($cooks as $cook)
                                    <option value="{{ $cook->id }}">{{ $cook->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        @endif
                        @foreach (config('translatable.locales') as $locale)
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.'.$locale.'.dishname')</label>
                                <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale.'.name') }}">
                            </div>
                        </div>
                        @endforeach
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <div  class="dropzone dz-clickable" >
                                    <div class="dz-default dz-message">
                                    <label>Dish Images : </label>

                                        <input class="box__file" type="file" name="files[]" id="file" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  multiple  hidden/>
                                        <label for="file"><strong>Choose a file</strong></label>
                                    </div>
                                </div>
                                <div class="upload-wrap">


                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>Cusines : </label>

                                    <select name="cusine[]" class="selectpicker" multiple data-live-search="true" style="width:50%;paddind:2px">
                                        @foreach ($cusines as $cusine)
                                            <option value="{{ $cusine->id }}">{{ $cusine->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>Categories : </label>

                                    <select id="category1" class="selectpicker"  name="category" data-live-search="true" style="width:50%;paddind:2px" >
                                        <option value="">select</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id ?? '' }}">{{ $category->name ?? '' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group row" id="addon_section"  style="display: none">
                                <div class="col-9 col-sm-9 mt-3" id="newRow">
                                    <label>Addons : </label><a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;"><i class="far fa-trash-alt "></i></a><a onclick="addRow()" class="float-right mt-2" style="font-size: medium;">+</a>
                                    <select class="addons1"   name="addons[]"     style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                        <option  value="">select</option>
                                    </select>
                                    <div  id="new_addon" style="margin-left: 73px; margin-top:5px">
                                        <select  name="addon_conditions[]"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" required>
                                            <option value="1">required</option>
                                            <option value="0">optional</option>
                                        </select>
                                        <p>
                                            <span >max</span><input type="text" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                            <span>min</span><input type="text" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>Allergens : </label>
                                    <select id="allergens" name="allergens[]" class="selectpicker" multiple data-live-search="true"   style="width:50%;paddind:2px">
                                        @foreach ($allergens as $allergen)
                                        <option value="{{ $allergen->id }}">{{ $allergen->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>price : </label>
                                <input type="text" name="price" class="form-control" placeholder=" 10  " value="{{ old('price') }}" style="width: 20%;display:inline"> <span> $</span>
                            </div>
                        </div> --}}
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>portions available : </label>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                      <a class="nav-link active" id="home-tab1" data-toggle="tab" href="#home1" role="tab" aria-controls="home1"
                                        aria-selected="true">Large</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="profile-tab1" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile1"
                                        aria-selected="false">Medium</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="contact-tab1" data-toggle="tab" href="#contact1" role="tab" aria-controls="contact1"
                                        aria-selected="false">Small</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab1">
                                        <input type="number"  name="portions_available[]" class="form-control " placeholder=" 5 " value="{{ old('portions_lg_available') }}" style="width: 20%;display:inline"> <span class="mr-5"> persons</span>
                                        <input   type="text" name="portions_price[]" class="form-control"  value="{{ old('portions_lg_price') }}" style="width: 20%;display:inline"> <span> EGP</span>
                                    </div>
                                    <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab1">
                                        <input   type="number" name="portions_available[]" class="form-control" placeholder=" 3 " value="{{ old('portions_md_available') }}" style="width: 20%;display:inline"> <span  class="mr-5"> persons</span>
                                        <input   type="text" name="portions_price[]" class="form-control"  value="{{ old('portions_lg_price') }}" style="width: 20%;display:inline"> <span> EGP</span>
                                    </div>
                                    <div class="tab-pane fade" id="contact1" role="tabpanel" aria-labelledby="contact-tab1">
                                        <input   type="number" name="portions_available[]" class="form-control" placeholder=" 2 " value="{{ old('portions_sm_available') }}" style="width: 20%;display:inline"> <span class="mr-5"> persons</span>
                                        <input  type="text" name="portions_price[]" class="form-control"  value="{{ old('portions_lg_price') }}" style="width: 20%;display:inline"> <span> EGP</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <label>Section : </label>

                            <ul class="dish-type">
                               @foreach ($sections as $section)
                               <li>
                                <input type="radio"  name="section" value="{{ $section->id }}">
                                <label for="f-option">{{ $section->name }}</label>
                                <div class="check"></div>
                              </li>
                               @endforeach
                            </ul>
                        </div>
                        <div class="col-12 col-sm-12 mb-4">
                            <label>availability : </label>
                            <ul class="dish-type">
                               <li>
                                <input type="radio" id="f-option" name="dish_available" value="1" checked>
                                <label for="f-option">availabe</label>
                                <div class="check"></div>
                                {{-- <input name="available_count" type="number" style="width: 20%"> dish --}}
                              </li>
                              <li>
                                <input type="radio" id="" name="dish_available" value="0">
                                <label for="f-option">not available</label>
                                <div class="check"></div>
                              </li>
                            </ul>
                        </div>


                        @foreach (config('translatable.locales') as $locale)

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.'.$locale.'.time_of_preparation')</label>
                                <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{ old($locale.'.main_ingredients') }}">
                            </div>
                        </div>
                        @endforeach
                        @foreach (config('translatable.locales') as $locale)
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.'.$locale.'.description')</label>
                                <textarea name="{{ $locale }}[info]" style="width: 90%"  rows="3">{{ old($locale.'.info') }}</textarea>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-contendropdown bootstrap-selectlete">
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endsection


@push('scripts')
    <script>
    @if (count($errors) > 0)
            $('#Add_Dish').modal('show');
    @endif

function removeDiv(elem){
    $(elem).parent('div').remove();
}

function showImage(file){


for (var i = 0; i < file.files['length']; i++) {

    var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
                "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
                // '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
            '</div>';

    $(".upload-wrap").append(img);

    $('.upload-image').css('display','block');
};
};

        $('#category').change(function(){
            $('#addon_section').show();
            var category_id = document.getElementById('category').value;
            $('.addons').find('option').remove().end();

            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/dashboard/addons/'+ category_id,
                success: function(data) {
                    for (var i = 0; i < data.success.length; i++) {
                        $(".addons").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');
                        // $(".addons1").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');

                    }
                    // $('.addons').selectpicker('refresh');
                }
            });

        });

        $('#addon_section').hide();

        $('#category1').change(function(){
            $('#addon_section').show();

            var category_id = document.getElementById('category1').value;
            $('.addons1').find('option').remove().end();
            // $('.addons1').find('.dropdown-menu').remove();
            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/dashboard/addons/'+ category_id,

                success: function(data) {
                    for (var i = 0; i < data.success.length; i++) {
                         $(".addons1").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');;
                        }
                    }
            });

        });

    $('.link_delete').on('click',function(){
        var dish=$(this).attr('id');
        $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/dish/'+dish)

    });


    // $("#addRow").click(function () {
    //     alert('tessssssst')
    //         let test=$("#newRowww").clone().appendTo("#addon_section");
    //         test.css('display','block')
    //         // $("#newRow").selectpicker();
    //         // $('.table-responsive').find().('#newRow').css('display','none')

    // });
    function addRow () {
        let test=$("#newRow").clone().appendTo("#addon_section");
            test.css('display','block')
        // $('#newRow').selectpicker();
        // $('.table-responsive').find().('#newRow').css('display','none')

    };
    // function addRooow (elem) {
    //     let test=$("#newRooow").clone().appendTo("#addon_section2");
    //         // test.css('display','block')
    //     // $('#newRow').selectpicker();
    //     // $('.table-responsive').find().('#newRow').css('display','none')

    // };

    $('.select_addon').change(function(){
        var selectedaddons = [];
        // var addon=$(this).val;
        // alert(addon);
        // selectedaddons.push(addon);
        $.each($(this).parent().parent().find('.select_addon'), function(){
            selectedaddons.push($(this).val());
            });
            console.log(selectedaddons);
    });


    function addRoow () {

        var parent = $('#addon_section2');
        parent.children(':last').clone().appendTo('#addon_section2');

        // $('#newRowedit').clone(true).appendTo('#addon_section2');
        // var form= $('#formUpdate ');

        // $('#newRow').selectpicker();
        // $('.table-responsive').find().('#newRow').css('display','none')

    };


        // $('.link_update').on('click',function(){
        //     var dish=$(this).attr('id');

        //     $('#formUpdate').attr('action','/{{ Config::get('app.locale') }}/dashboard/dish/'+dish+'/edit')

        // })
    </script>
@endpush


