
@extends('backend.layouts.app')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css">

{{-- <link rel="stylesheet" href="https://openlayers.org/en/v4.3.1/css/ol.css" type="text/css"> --}}
    <!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
    {{-- <script src="https://openlayers.org/en/v4.3.1/build/ol.js"></script> --}}
<script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script>

<style>
    .dropzone.dz-clickable{
    cursor:pointer;
    text-align: center;
}
.dropzone {
    background-color: #fbfbfb;
    border: 2px dashed rgba(0, 0, 0, 0.1);
    min-height: 150px;
    border: 2px solid rgba(0,0,0,0.3);
    background: white;
    padding: 20px 20px;
}
.dropzone .dz-message{
    margin: 3em 0;

}
.upload-image{
position: relative;
    width: 80px;
    margin-right: 20px;
    display: none
}
.upload-image img {
    border-radius: 4px;
    height: 80px;
    width: 80px;
}





</style>


@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Profile of Cooks</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Cook profile</li>
                    </ul>
                </div>

            </div>

        </div>
        <!-- /Page Header -->

    <!-- Page Content -->
    <div class="content mt-5">
        <div class="container">

            <div class="row">
                <div class="col-md-7 col-lg-8 col-xl-9">

                    <form action="{{ route('cook.update',$cook->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <!-- Basic Information -->
                        <div class="card">
                            <div class="card-body">
                                @include('backend.partials.errors')

                               <div class="row">
                                <div class="col-md-6">
                                    <h4 class="card-title">Basic Information</h4>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-switch">
                                        {{-- @dump($cook->availability) --}}
                                        <input name="availability" type="checkbox" class="custom-control-input" id="customSwitches" value="{{ $cook->availability }}" {{ $cook->availability == 1 ? 'checked' : ''}} >
                                        <label class="custom-control-label" for="customSwitches">Status</label>
                                      </div>
                                </div>
                               </div>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="change-avatar">
                                                <div class="profile-img mb-3">
                                                    <img id="hidden_img_src" src="{{ asset('/backend/img/cook/'.$cook->images) }}" alt="Cook Image" width="150" style="border-radius: 25%;" >
                                                </div>
                                                <div class="upload-img">
                                                    <div class="change-photo-btn">
                                                        <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                        <input type="file" name="profile_image" class="upload" onchange="hidden_img_src.src=window.URL.createObjectURL(this.files[0])">

                                                    </div>
                                                    <small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Username </label>
                                            <input type="text" name="name" class="form-control" value="{{ $cook->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email </label>
                                            <input type="email" name="email" class="form-control"value="{{ $cook->email }}" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input type="text" name="phone" value="{{ $cook->phone }}" class="form-control">
                                        </div>
                                    </div>

                                    {{-- <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label>Date of Birth</label>
                                            <input name="birth_date"  type="date" value="{{$cook->date_of_birth ?? old('birth_date') }}" class="form-control">
                                        </div>
                                    </div> --}}
                                    <div class="col-md-12">
                                        Address :
                                        <div class="col-sm-12 mb-3">
                                            <div class="row pb-3 map-location pt-3">
                                                {{-- <input type="text" class="form-control " id="coordinatold" value="{{ $cook->address[0]->coordinates ?? ' ' }}"  >
                                                <input type="text" class="form-control "  name="coordinates" id="coordinates" value="{{ $cook->address[0]->coordinates ?? ' ' }}"  >
                                                <input type="text"  class="form-control "  name="address"  value="{{ $cook->address[0]->address ?? ' ' }}"  >
                                                <div id="map"  style="width: 800px;height:200px "></div>
                                                <div id="location" class="marker"><i class="fa fa-arrow"></i></div>

                                                <input hidden type="button" id="track" value="trackme"/> --}}
                                                <input type="text" id="old-coordinates"  class="form-control "   value="{{ $cook->address[0]->coordinates ?? '' }}" hidden >
                                                <input type="text"  class="form-control "  name="coordinates" id="coordinates"  value="{{ $cook->address[0]->coordinates ?? '' }}" hidden >
                                                <input type="text"  class="form-control "  name="address"  value="{{ $cook->address[0]->address ?? ' ' }}"  >
                                                <div id="map"  style="width: 800px;height:200px "></div>

                                            </div>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Title</label>
                                                <input name="title"  type="text" value="{{ $cook->address[0]->title ?? old('title') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>State</label>
                                                <input name="state"  type="text" value="{{ $cook->address[0]->state ?? old('state') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>City</label>
                                                <input name="city"  type="text" value="{{ $cook->address[0]->city ?? old('city')  }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Area</label>
                                                <input name="area"  type="text" value="{{$cook->address[0]->area ?? old('area') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Floor</label>
                                                <input name="floor"  type="text" value="{{$cook->address[0]->floor ?? old('floor') }}" class="form-control">
                                            </div>
                                        </div> --}}
                                    </div>
                                    <div class="col-md-12 mt-3 ">
                                        <div class="col-md-12 mt-3 ">
                                            Availability :
                                        </div>
                                        <div class="col-md-12 mt-3 ">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    @php
                                                        $from=json_decode($cook->work_from,true);
                                                        $to=json_decode($cook->work_to,true);
                                                        $days=['Sat','Sun','Mon','Tue','Wed','Thu','Fri']
                                                    @endphp
                                                    <thead>
                                                        <tr>
                                                            <th> #</th>
                                                            <th> From</th>
                                                            <th> To</th>
                                                            <th> Holiday</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        @for ($i = 0; $i < count($days); $i++)
                                                        <tr>
                                                            <td>{{ $days[$i] }}</td>
                                                            @if ($from[$i] != null  )
                                                                <td class="from"> <input class="from_input" name="work_from[]"  type="time" value="{{ date('H:i:s', strtotime($from[$i])) }}"   ></td>
                                                            @else
                                                                <td class="from"> <input class="from_input" name="work_from[]"  type="time" value=""   ></td>
                                                            @endif
                                                            @if ($to[$i] != null )
                                                                <td class="to"> <input class="to_input" name="work_to[]"   type="time" value="{{date('H:i:s', strtotime($to[$i])) ?? old('work_to') }}"  > </td>
                                                            @else
                                                                <td class="to"> <input class="to_input" name="work_to[]"   type="time" value=""  > </td>
                                                            @endif

                                                            <td class="text-center" onclick="reset(this)">
                                                                <input  name="holiday[]" type="checkbox" id="holiday{{ $i }}"   value="{{ $i }}"  {{ $from[$i] == null ? 'checked':' ' }} >
                                                            </td>
                                                        </tr>
                                                        @endfor


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Basic Information -->

                        <!-- About Me -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">About Me</h4>
                                <div class="form-group mb-0">
                                    <label>info</label>
                                    <textarea name="info" class="form-control"  rows="5" >{{$cook->info ?? old('info') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /About Me -->

                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">terms and conditions <p style="font-size: 10px">*by submit that mean you are  agree with the terms and conditions</p></h4>
                                <div class="form-group mb-0" style="height: 200px;overflow-y: scroll;">
                                    <p>{{ $terms }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="submit-section submit-btn-bottom">
                            <button type="submit" class="btn btn-primary submit-btn"> <i class="fas fa-edit"></i> Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
    <!-- /Page Content -->
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> --}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script>
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.css" type="text/css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.js"></script>

    <script>


function reset(elem){
    $(elem).parent().find('.from').find('.from_input').val('');
    $(elem).parent().find('.to').find('.to_input').val('');

}



let map, infoWindow;
var coordss=$('#old-coordinates').val();

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 10,
  });
  infoWindow = new google.maps.InfoWindow();
  const locationButton = document.createElement("button");
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };

          infoWindow.setPosition(pos);

           if(coordss == null){
                infoWindow.open(map);
                map.setCenter(pos);
                var marker = new google.maps.Marker({
                position: pos,
                map: map,});
                var coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
                $('#coordinates').attr('value',coordinates);

                }else{
                    var pos=coordss.split(",");
               pos ={
                   lat:parseFloat(pos[0]),
                   lng:parseFloat(pos[1])
               }
               console.log(pos,'llllllllll')
            //    infoWindow.setPosition(pos)
            infoWindow.open(map);
            map.setCenter(pos);
            console.log(pos)
            var marker = new google.maps.Marker({
                position:pos,
                map: map,});
                }


            map.addListener("click", (mapsMouseEvent) => {
            // Close the current InfoWindow.
            marker.setMap(null);
            // Create a new InfoWindow.
            marker = new google.maps.Marker({
            position: mapsMouseEvent.latLng,
            map: map,});

            // console.log(marker.getPosition().lat())  ;
            // console.log(marker.getPosition().lng())  ;
            coordinates=(marker.getPosition().lat()).toString()+','+(marker.getPosition().lng()).toString();
            $('#coordinates').attr('value',coordinates);
            // $('#lat').attr('value',marker.getPosition().lat());

        });
        // if(coords != null){
        //     infoWindow.open(map);
        //     console.log(coords)
        //     marker = new google.maps.Marker({
        //         position:coords,
        //         map: map,});
        //         // markers.push(marker);

        //  }
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }





}


function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}



    </script>



@endsection




