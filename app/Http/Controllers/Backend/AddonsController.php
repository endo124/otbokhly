<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Addons;
use App\Models\AddonsTranslation;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\AddonSection;

class AddonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Addons::first()->addonsection_id);
        // if (auth()->guard('admin')->user()->roles[0]->name != 'cook' ){
            $categories=Category::all();
            // $addons=Addons::all();
            // $sections=AddonSection::all();
        // return view('backend.addons_list',compact('addons','categories','sections'));

        // }else{
            // $addons=Addons::all();
            $cook=User::find(auth()->guard('admin')->user()->id);
            $addons=$cook->addons;
            $sections=$cook->addonsections;
        return view('backend.addons_list',compact('addons','categories','sections'));
        // }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // if(auth()->guard('admin')->user()->roles[0]->name != 'cook'){
            $rules=[
                'categories'=>'required|min:1',
                'section'=>'required',
                'price'=>'required|regex:/^\d*\.?\d*$/',
               ];
           foreach (config('translatable.locales') as $locale) {
               $rules += [$locale.'.name'=>['required',Rule::unique('addons_translations','name')]];
           }

           $validator=\Validator::make($request->all(), $rules);
           if ($validator->fails()) {
               return \Redirect::back()->with(['message_add'=>$validator->errors()]);
           }
           else{

            $data=$request->except('categories');
            $addon=Addons::create($data);
            $addon->update([
                'addonsection_id'=>$request->section
            ]);
            \DB::table('addon_cook')->insert([
                        // 'price'=>$request->price,
                        'addon_id'=>$addon->id,
                        'user_id'=>auth()->guard('admin')->user()->id
                        ]);
            //    $addon->users()->attach($request->price);
            foreach($request->categories as $category){
                $addon->categories()->attach($category);
            }
            // }
            // else{
                // $request->validate([
                //     'price'=>'required',
                //     'addon'=>'required',
                // ]);
                // dd($request->addon);
                // foreach($request->addon as $add){
                //     \DB::table('addon_cook')->insert([
                //         'price'=>$request->price,
                //         'addon_id'=>$add,
                //         'user_id'=>auth()->guard('admin')->user()->id
                //         ]);
                // }


            // }
        return back();

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $category=Category::find($id);
            $addons=$category->addons;
        if (auth()->guard('admin')->user()->roles[0]->name != 'cook' ){


            return response()->json(['success'=>$addons]);
        }else{


            $cook=User::find(auth()->guard('admin')->user()->id);
            $addos_id=$cook->addons->pluck('id')->toArray();
            $cook_addon=collect();
            foreach ($addons as $index=>$addon) {

               if(in_array($addon->id,$addos_id) ){

                $cook_addon->push($addon);
                }
            }
            return response()->json(['success'=>$cook_addon]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $addon=Addons::find($id);

        $rules=[ 'categories'=>'required|min:1',
        'price'=>'required|regex:/^\d*\.?\d*$/'

        ];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required','unique:addons,id,'.$addon->id]];
        }
        // if(auth()->guard('admin')->user()->roles[0]->name != 'cook'){



            $validator=\Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return \Redirect::back()->with(['message_update'=>$validator->errors(),'addon_id'=>$addon->id]);
            }
            else{
                    $data=$request->except('categories');
                    $addon->update([
                        'addonsection_id'=>$request->section,

                    ]);
                    $addon->update($data);
                    // $addon->save();

                    $addon->categories()->sync($request->categories);
                    // return  redirect()->route('addons.index');
                    // }else{

                    // $validation=$request->validate([
                    //     'price'=>'required'
                    // ]);
                    $addon->users[0]->pivot->price=$request->price;
                    $addon->users[0]->pivot->save();
                    return  redirect()->route('addons.index');
                }
        // }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $addon_trans=AddonsTranslation::where('addon_id',$id)->delete();
        $addon=Addons::where('id',$id)
        ->with('users')
        ->with('categories')
        ->first();

        if(isset($addon->dishes)){
            foreach($addon->dishes as $dish){
                \DB::table('addon_dish')->where('dish_id', $dish->id)->delete();
            }
        }
        if(isset($addon->categories)){
            foreach($addon->categories as $category){
            \DB::table('addon_category')->where('category_id', $category->id)->delete();
             // $addon->categories()->save();
            }

         }
        // dd($addon->users()->detach(),$addon->categories()->detach());

        if(isset($addon->users) > 1){
            $addon->users()[0]->detach();
            $addon->users()[0]->save();

        }


        \DB::table('addon_cook')->where('addon_id', $addon->id)->delete();
        if(isset($addon->categories) > 1 ){
            foreach($addon->categories as $category){

             $category->detach();
             // $addon->categories()->save();
            }

         }


        $addon->delete();
        return back();
    }
}
