<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\CookSlider;
use App\Models\User;
use App\Models\UserTranslation;
use Illuminate\Http\Request;

class CookAdvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cooks=User::whereHas('roles',function($q){
            $q->where('name','cook');
        })->get();
        $sliders=CookSlider::with('cooks')->get();
        return view('backend.cookAdv_list',compact('cooks','sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
                // 'cook'=>'required',
                'files'=>'required'
            ];
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{
            if ($request->file('files')) {
                    $ext=$request->file('files')->getClientOriginalExtension();
                    $image_name=time().'.'.$ext;

                    $path='backend/img/offers';
                    $request->file('files')->move($path,$image_name);
            }
           

            if(isset($request->cook)){
                $cook=UserTranslation::where('name',$request->cook)->first();
                $ad=CookSlider::create([
                    'image'=>$image_name,
                    'user_id'=>$cook->user_id
                ]);
            }else{
                $cook=UserTranslation::where('name',$request->cook)->first();
                $ad=CookSlider::create([
                    'image'=>$image_name,
                ]);
            }


            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider=CookSlider::find($id);
        // dd($slider,$request->file('files'));
        // $rules=[
        //         'cook_name'=>'required',
        //     ];
        // $validator=\Validator::make($request->all(), $rules);
        // if ($validator->fails()) {
        //     return \Redirect::back()->with(['message_update'=>$validator->errors(),'slider_id'=>$slider->id]);
        // }
        // else{
            if ($request->file('files') !=null) {
                $ext=$request->file('files')->getClientOriginalExtension();
                $image_name=time().'.'.$ext;

                $path='backend/img/offers';
                $request->file('files')->move($path,$image_name);
                $slider->update(
                    [
                        'image'=>$image_name,
                    ]);
            }

            // $cook=UserTranslation::where('id',$request->cook_name)->first();
            // dd($request->cook_name);
            $slider->update(
                [
                    'user_id'=>$request->cook_name
                ]
            );

            return  back();
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad=CookSlider::find($id);
        $ad->delete();

        return back();
    }
}
