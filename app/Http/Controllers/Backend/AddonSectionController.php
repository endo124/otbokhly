<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AddonSection;
use App\Models\AddonSectionTranslation;
use Illuminate\Validation\Rule;
use App\Models\User;

class AddonSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $cook=User::find(auth()->guard('admin')->user()->id);
        $sections=$cook->addonsections;
        return view('backend.addon_section_list',compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required'
            // ,Rule::unique('addonsection_translations','name')
            ]];
        }
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{
            $sections=AddonSectionTranslation::where('name',$request->en['name'])->orWhere('name',$request->ar['name'])->get();
            if(isset($sections)){
                return \Redirect::back()->with(['message_add'=>['this name already taken']]);
            }
            $section=AddonSection::create($request->all());
            $section->update(['user_id'=>auth()->guard('admin')->user()->id]);
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $section=AddonSection::find($id);
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.name' => ['required'
            // , Rule::unique('addonsection_translations', 'name')->ignore($section->id, 'addon_section_id')
            ]];
        }//end of for each
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_update'=>$validator->errors(),'section_id'=>$section->id]);
        }
        else{
            $section->update($request->all());
            $section->save();
            // dd($section);
            return redirect()->route('addons_section.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section_trans=AddonSectionTranslation::where('addon_section_id',$id)->delete();
        $section=AddonSection::find($id);
        // $section->addons()->detach();
        $section->delete();
        return back();
    }
}
