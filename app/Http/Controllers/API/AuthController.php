<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Validator;
use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Notifications\ResetPasswordCustomer;
use App\Traits\GeneralTrait;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    use GeneralTrait;


    public $toke = true;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    public function __construct()
    {
    $this->middleware('auth:api', ['except' => ['login', 'register','forgetpassword','showForgetForm','reset']]);
    }

    public function forgetpassword(Request $request){
        $validator = \Validator::make($request->all(), [
			'email' => 'required|string|email|max:255',

		]);
		if ($validator->fails()) {
			return response()->json($validator->errors());
		}
        $customer =User::where('email', '=', $request->email)
        ->first();
        //Check if the user exists
        if (is_null($customer)) {
            return response()->json(['error' => 'Customer Doesnt Existed'], 401);
        }
        $random = \Str::random(60);
        //Create Password Reset Token
        \DB::table('password_resets')->insert([
        'email' => $request->email,
        'token' =>$random,
        'created_at'=>date('Y-m-d H:i:s'),

    ]);
    //Get the token just created above
    $tokenData = \DB::table('password_resets')->where('email', $request->email)->latest()->first();
    if ($this->sendResetEmail($request->email, $tokenData->token)) {
        return response()->json([
			'success' => true,
			'message' => 'A Forget email has been sent to your email address..',
		 // time to expiration

        ], 200);
        } else {
            return response()->json(['error' => 'A Network Error occurred. Please try again.'], 401);

    }
    }

    private function sendResetEmail($email, $token)
    {

        //Retrieve the user from the database
        $customer =User::where('email', $email)->select('email')->first();
        //Generate, the password reset link. The token generated is embedded in the link

        try {

        $data=['customer'=>$customer,'token'=>$token];
        $customer->notify(new ResetPasswordCustomer($data));
        //Here send the link with CURL with an external email API
        return true;
        } catch (\Exception $e) {

        return $e;
        }
    }

    public function showForgetForm(Request $request, $token )
    {
        return view('front.auth.passwords.forget')->with(
        ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(Request $request){

        $request->validate(['email' => 'required',
        'password' => 'required|confirmed',]);

        $user=User::where('email',$request->email)->first();
        $get_token=DB::table('password_resets')->where(['token'=>$request->token ,'email'=>$request->email] )->first();
        if( $get_token){
            $user->update(['password'=> Hash::make($request->password)]);
        }else{
            return 'invalid inputs';
        }
        return 'reset password successfully';

    }

    public function register(Request $request)
        {

            // return response()->json(['error'=>'you can not sign up now'], 401);
            $validator=Validator::make($request->all(), [
                        'name' => 'required',
                        'email' => 'required|email|unique:users',
                        'phone' => 'required|unique:users|regex:/(01)[0-9]{9}/',
                        'password' => 'required|confirmed',
                     ]);
            if ($validator->fails()) {
               return response()->json(['error'=>$validator->errors()], 401);
            }
            $customer=User::create([
                // 'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'phone' => $request->phone,

            ]);
            $ar=['name'=>$request->name];
            $en=['name'=>$request->name];
            $customer->update(['ar'=>$ar,'en'=>$en]);
            $customerRole=Role::where('name','customer')->first();
            $customer->attachRole($customerRole->id);
            $credentials = $request -> only(['email','password']) ;

            $token=auth('api_customer')->attempt($credentials);
            if (!$token) {
                return response()->json(['error' => 'Your email or password incorrect'], 401);
            }
            $customer->update(['login'=>1]);

          return  $this->respondWithToken($token,$customer);


        }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
         ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $user=User::where('email',$request->email)->first();

        $credentials=$request->only(['email','password']);

        $credentials = request(['email', 'password']);
        if (!  $token=auth()->guard('api_customer')->attempt($credentials)) {

            return response()->json(['error' => 'Your email or password incorrect'], 401);
        }
        // if($user->login != 0){
        //     return response()->json(['auth' => 'You can not login from another device'], 401);

        // }
        $customer=User::where('id',auth('api_customer')->user()->id)->first();
        $customer->update(['login'=>1]);

        return $this->respondWithToken($token,$customer);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $customer=User::where('id',auth('api_customer')->user()->id)->first();
        $customer->update(['login'=>0]);

        auth('api_customer')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $customer=User::where('id',auth('api_customer')->user()->id)->first();

        return $this->respondWithToken(auth()->refresh(),$customer);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token,$customer)
    {
        return response()->json([
            'customer'=>$customer,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api_customer')->factory()->getTTL() * 60,
        ]);
    }

    public function changePass( Request $request)
    {

        $validator=\Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|confirmed',
         ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }


        $customer=User::find(auth('api_customer')->user()->id);
        $current_password=$customer->password;
        if(Hash::check($request->current_password, $current_password)){
            $customer->update([
                'password'=>Hash::make($request->password)
            ]);
            return $this->returnSuccessMessage('password edit successfully');

        }
        else{
            $error = array('current_password' => 'Please enter correct current password');
            return response()->json(array('error' => $error), 400);
        }
    }
}
