<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\AddonSection;
use App\Models\CityTranslation;
use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Container\Container;
class DishController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function wishlist($lang,$items)
    {
        $ids=auth('api_customer')->user()->products->pluck('id');
        $dishes=Dish::whereIn('id',$ids)
        ->with(['users'=>function($q){
            $q->orderBy('availability', 'DESC');
        }])
        ->with('cusines')
        ->with('sections')
        ->with('addons.users')->get();

        $dishes=$dishes->sortByDesc('users.availability')->values();

        $dishes= $this->custom_paginate($dishes ,$items);
        return $this->returnData('dishes',$dishes);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function search($dish,$lang ,$governorate)
    {

        $city=CityTranslation::where('name',$governorate)->first();
        $dishes=[];
        if(isset($city)){
            $cooks=User::whereRoleIs('cook')
            ->where('city_id',$city->city_id)
            ->where('active',1)
            ->pluck('id');

            $dishes_id=DishTranslation::where('name', 'LIKE', "%$dish%")->pluck('dish_id');



            $dishes=Dish::with('users')
            ->with('cusines')
            ->with('sections')
            ->with('addons.users')
            ->whereIn('user_id',$cooks)
            ->whereIn('id',$dishes_id)
            ->paginate(5);
            if(count($dishes) > 0){
                return $this->returnData('dishes',$dishes);

            }else{
                return $this->returnError('there is no data');
            }

        }else{
            return $this->returnError('there is no data in this city');
        }

        // $pageSize = 5;//custom paginate size

        // $dishes= $this->paginate($dishes, $pageSize);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//store to wishlist
    {


        $ids=auth('api_customer')->user()->products->pluck('id')->toArray();

        $customer_id=auth()->guard('api_customer')->user()->id;
        $customer=User::where('id',$customer_id)->first();

        $product=Dish::where('id',$request->id)->first();
        if(in_array($request->id , $ids)){
            $customer->products()->detach($product);
            return $this->returnSuccessMessage('successfully removed from wishlist');

        }else{
            $customer->products()->attach($product);
            return $this->returnSuccessMessage('successfully added to wishlist');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$id)
    {
        $section=[];

        $dish=Dish::where('id',$id)
        ->with('sections')
        ->with('cusines')
        ->with('users')
        ->first();
        $cook_id=$dish->user_id;
        // return($dish['addons']);
        foreach ($dish['addons'] as  $addon) {

            $section_data=AddonSection::where('id',$addon->addonsection_id)->first();
            $section_name=$section_data->name;
            array_push($section,$section_name);
        }
        $section= array_unique($section);
        // return $section;
        $sections = \DB::table('addonsection')
        ->rightJoin('addonsection_translations' ,'addonsection.id' , '=' , 'addonsection_translations.addon_section_id')
        ->where('addonsection.user_id',$cook_id)////////////////////////
        ->where('addonsection_translations.locale',$lang)
        ->whereIn('addonsection_translations.name',$section)
        // return $sections;

        ->rightJoin('addons' ,'addonsection.id' , '=' , 'addons.addonsection_id')
        // ->whereIn('addonsection.id',$section)
        ->rightJoin('addon_cook' ,'addons.id' , '=' , 'addon_cook.addon_id')

        ->rightJoin('addons_translations' ,'addons_translations.addon_id' , '=' , 'addons.id')
        ->where('addons_translations.locale',$lang)

        ->rightJoin('addon_dish' ,'addons.id' , '=' , 'addon_dish.addon_id')

        ->where('addon_dish.dish_id' , $dish->id)
        ->select('addon_dish.min','addon_dish.max','addon_dish.condition','addons.price As price','addons.addonsection_id','addons.id','addonsection_translations.name As section_name','addons_translations.name As addon_name')

        ->get();

        $dish['addonsection']=$sections;


        return response()->json(["dish"=>[$dish ]],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public static function paginate(Collection $results, $pageSize)
    // {

    //     $page = Paginator::resolveCurrentPage('page');

    //     $total = $results->count();

    //     return self::paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
    //         'path' => Paginator::resolveCurrentPath(),
    //         'pageName' => 'page',
    //     ]);

    // }
    // protected static function paginator($items, $total, $perPage, $currentPage, $options)
    // {
    //     return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
    //         'items', 'total', 'perPage', 'currentPage', 'options'
    //     ));
    // }

    function getaddress(Request $request  , $lang,$location)
    {
       $dish=$request->dish;

       $url = 'https://maps.google.com/maps/api/geocode/json?latlng='.$location.'&key=AIzaSyArUJhAM_MY5imJh7g6l6-rgcBOMyOS63s';
       $json = @file_get_contents($url);
       $data=json_decode($json);
       $status = $data->status;


       if($status=="OK")
       {
            $address= $data->results;
            $address_components= $address[0]->address_components;
            for ($i = 0; $i < count($address_components); $i++) {
                $address= $data->results;
                $address_components= $address[0]->address_components;
                $addressType = $address_components[$i]->types[0];
                if ($addressType == 'administrative_area_level_1') {
                    $governorate= $address[0]->address_components[$i]->short_name;
                }
            }
        }
       return $this->search($dish,$lang,$governorate);
    }


    function custom_paginate($data ,$items)
    {

        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($data);

        //Define how many items we want to be visible in each page
        $per_page = $items;

        //Slice the collection to get the items to display in current page
        $currentPageResults = $collection->slice(($currentPage - 1) * $per_page, $per_page)->values();

        //Create our paginator and add it to the data array
        $data['results'] = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);

        //Set base url for pagination links to follow e.g custom/url?page=6
        return $data['results']->setPath(request()->url());
    }
}
