<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\Order;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;
use App\Models\Addons;
use App\Models\SectionTranslation;

class CartController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    // public function index()
    // {
    //     $cart=[];
    //     $order=[];
    //     $orders=Order::where('user_id',auth('api_customer')->user()->id)
    //     ->whereNotNull('status')
    //     ->get();
    //     $orderEntry=[];
    //     $total_price=0;
    //     if(isset($orders[0]) ){
    //         foreach ($orders as  $index=>$or) {
    //             foreach($or['orderEntry'] as $item){
    //                 $addons=[];
    //                 $dish=Dish::find($item['id']);
    //                 $dish['section_ar']=$item['section_ar'];
    //                 $dish['section_en']=$item['section_en'];

    //                 $dish['size_ar']=$item['size_ar'];
    //                 $dish['size_en']=$item['size_en'];
    //                 $dish['price']=$item['price'];
    //                 $dish['qty']=$item['quantity'];
    //                 $total_price+=$item['price'];
    //                 foreach ($item['addons'] as $addon) {
    //                     // dd($addon);
    //                     // $addon=Addons::find($addon['id']);

    //                     array_push($addons,$addon);
    //                 }
    //                 $dish['addons'] = $addons;

    //                 array_push($orderEntry,$dish);

    //             }

    //             $or['orderEntry']=$orderEntry;
    //             $or['total_price']=$total_price;

    //             $cook=User::find($or[$index]['cook_id']);
    //             if(isset($cook->address[0])){
    //             $address=$cook->address[0];

    //             }else{
    //                 $address="test,test";
    //             }

    //             $or['cook']=[
    //                 // 'taxes_type'=>$cook->taxes_type,
    //                 // 'amount'=>$cook->taxes,
    //                 'address'=>$address
    //             ];
    //         }



    //     }


    //     if(count($orderEntry) > 0){


    //         // $cook=User::find($order[0]['cook_id']);
    //         // dd($cook);
    //         // $address=$cook->address[0];
    //         return  response()->json([
    //             'status' => true,
    //             'errNum' => "S000",
    //             'msg' => "",
    //             'cart' => $orders,
    //             // 'cook'=>['taxes_type'=>$cook->taxes_type,'amount'=>$cook->taxes,'address'=>$address]
    //         ]);
    //     }else{
    //         return  response()->json([
    //             'status' => true,
    //             'errNum' => "S000",
    //             'msg' => "",
    //             'cart' => $order,
    //             'cook'=>null
    //         ]);
    //     }


    // }




    public function index($lang)
    {
        $cart=[];
        $order=[];
        $order=Order::where('user_id',auth('api_customer')->user()->id)
        ->whereNull('status')
        ->first();
        // $orderEntry=[];
        // $total_price=0;
        // if(isset($orders[0]) ){
        //     foreach ($orders as  $index=>$or) {
        //         foreach($or['orderEntry'] as $item){
        //             $addons=[];
        //             $dish=Dish::find($item['id']);
        //             $dish['section_ar']=$item['section_ar'];
        //             $dish['section_en']=$item['section_en'];
        //             $dish['size_ar']=$item['size_ar'];
        //             $dish['size_en']=$item['size_en'];
        //             $dish['price']=$item['price'];
        //             $dish['qty']=$item['quantity'];
        //             $total_price+=$item['price'];
        //             foreach ($item['addons'] as $addon) {
        //                 // dd($addon);
        //                 // $addon=Addons::find($addon['id']);

        //                 array_push($addons,$addon);
        //             }
        //             $dish['addons'] = $addons;

        //             array_push($orderEntry,$dish);

        //         }

        //         $or['orderEntry']=$orderEntry;
        //         $or['total_price']=$total_price;

        //         $cook=User::find($or[$index]['cook_id']);
        //         if(isset($cook->address[0])){
        //         $address=$cook->address[0];

        //         }else{
        //             $address="test,test";
        //         }

        //         $or['cook']=[
        //             // 'taxes_type'=>$cook->taxes_type,
        //             // 'amount'=>$cook->taxes,
        //             'address'=>$address
        //         ];
        //     }

        // }

        if(isset($order)){
            $orderEntry=[];
            $total_price=0;

                    foreach($order['orderEntry'] as $item){

                        $addons=[];


                        $dish=Dish::find($item['id']);
                        $dish['cook_id']=$dish['user_id'];
                        $dish['size']=$item['size'];
                        $dish['price']=$item['price'];
                        $dish['qty']=$item['qty'];


                        $section_id=Dish::find($dish['id'])->sections->id;
                        $dish['section']=SectionTranslation::where(['section_id'=>$section_id,'locale'=>$lang])->first()->name;


                        $total_price+=$item['price']*$item['qty'];
                        foreach ($item['addons'] as $add) {
                            $addon=Addons::find($add['id']);
                            $addon['qty']=(int)$add['qty'];
                            array_push($addons,$add);
                        }
                        $dish['addons'] = $addons;

                        array_push($orderEntry,$dish);

                    }

                    $order['orderEntry']=$orderEntry;
                    $order['total_price']=$total_price;

                    array_push($cart,$order);


        }



        if(isset($order['cook_id'])){

            $cook=User::find($order['cook_id']);
            $address=$cook->address[0];
            // dd($cook);

            return  response()->json([
                'status' => true,
                'errNum' => "S000",
                'msg' => "",
                'cart' => $cart,
                'cook'=>['taxes_type'=>'amount','amount'=>'amount','address'=>$address,'from'=>$cook->work_from,'to'=>$cook->work_to,'availability'=>$cook->availability]
                // 'cook'=>['taxes_type'=>$cook->taxes_type,'amount'=>$cook->taxes,'address'=>$address]
            ]);
        }else{
            return  response()->json([
                'status' => true,
                'errNum' => "S000",
                'msg' => "",
                'cart' => $cart,
                'cook'=>null
            ]);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $cart=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();

        $data=array();
        $item=$request->data;

        // foreach ($request->data as  $item) {
        //     $validator=Validate::make($item, [
        //         'qty'=>'required',
        //         'size'=>'required'
        //       ]);
        //     if ($validator->fails()) {
        //     dd('aa');

        //     return response()->json(['error'=>$validator->errors()], 401);
        //     }

            $orderEntr=array();

            $dish=Dish::find($item['id']);

            $orderEntr['id']=$item['id'];
            $orderEntr['qty']=$item['qty'];
            $orderEntr['size']=$item['size'];

            // if(isset($item['notes'])){
                $orderEntr['notes']=$item['notes'];
            // }
            $orderEntr['addons']=$item['addons'];

            $orderEntr['name']=$dish->name;
            $orderEntr['price']=$item['price'];
            $orderEntr['section']=$dish->sections->name;
            $orderEntr['images']=$dish->images;
        // }
        // dd($request->data);
        // $cart=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();

        // dd($orderEntr);
         $address=User::find($dish->user_id)->address->toArray();
        if( ! empty($address) && $address[0]['coordinates'] != null){
            if(isset($cart)){
                array_push($data,$orderEntr);

                if($dish->user_id == $cart->cook_id){
                    $cartEntry=$cart->orderEntry;
                    array_push( $cartEntry,$orderEntr);
                    $cart->update([
                        'orderEntry'=>$cartEntry,
                    ]);
                }elseif ($dish->user_id != $cart->cook_id && $request->force == 1) {
                    $cart->update([
                        'cook_id'=>$dish->user_id,
                        // 'date'=>$request->date,
                        // 'time'=>$request->time,
                        'orderEntry'=>$data,
                        // 'address'=>$request->address,
                    ]);
                    return $this->returnSuccessMessage('cart updated successfully');

                }else{

                    return $this->returnSuccessMessage('you can not add to cart from different Restaurant');

                }

            }else{
                array_push($data,$orderEntr);

                $order=Order::create([
                    'user_id'=>auth()->guard('api_customer')->user()->id,
                    'cook_id'=>$dish->user_id,
                    // 'date'=>$request->date,
                    // 'time'=>$request->time,
                    'orderEntry'=>$data,
                    // 'address'=>$request->address,
                ]);

            }

            return $this->returnSuccessMessage('successfully added to cart');
        }else{
            return $this->returnError('this cook has no address');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang,$id)
    {
    //     $data=array();

    //     foreach ($request->all() as  $item) {
    //     $validator=Validator::make($item, [
    //         'qty'=>'required',
    //         'size'=>'required'
    //       ]);
    //     dd('gjg');

    //     if ($validator->fails()) {
    //     return response()->json(['error'=>$validator->errors()], 401);
    //     }
    //     $orderEntry=array();
    //     $dish=Dish::find($id);
    //     $orderEntry['qty']=$item->qty;
    //     $orderEntry['size']=$item->size;
    //     if($item->notes){
    //         $orderEntry['notes']=$item->notes;
    //     }
    //     $orderEntry['name']=$dish->name;
    //     $orderEntry['price']=$dish->price*$item->qty;
    //     $orderEntry['section']=$dish->sections->name;
    //     $orderEntry['images']=$dish->images;
    //     array_push($data,$orderEntry);
    // }
    // dd($id);

    // $order=Order::find($id);
    // $order->update([
    //     // 'user_id'=>auth()->guard('api_customer')->user()->id,
    //     // 'date'=>$request->date,
    //     // 'time'=>$request->time,
    //     'orderEntry'=>json_encode($data),
    //     // 'address'=>$request->address,
    // ]);
    //     return $this->returnSuccessMessage('successfully edit  cart');

    }



    public function updateqty(Request $request){

        $cart=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();
            if(isset($cart)){
                $orderEntry=[];

                foreach($cart['orderEntry'] as $index=>$item){
                    if($item['id'] != $request->id){
                        array_push($orderEntry,$item);
                    }else{
                        if($request->qty != 0 && $request->index == $index){
                            $dish=Dish::find($item['id']);

                            if( $item['size'] == 'small'){
                                $item['price']=$dish['portions_price'][2];

                            }elseif($item['size'] == 'medium'){
                                // dd($request->qty,$dish['portions_price'][1]);
                                $item['price']=$dish['portions_price'][1];

                            }else{
                                $item['price']=$dish['portions_price'][0];

                            }
                            $item['qty']=$request->qty;
                            $item['price']=(string)$item['price'];

                            array_push($orderEntry,$item);
                        }elseif($request->qty == 0 && $request->index == $index){
                            continue;
                        }
                        else{
                            array_push($orderEntry,$item);
                        }

                    }

                }
            }
            if( $orderEntry==[]){
                $cart->delete();
            }else{
                $cart->update([
                    'orderEntry'=>$orderEntry
                ]);
            }


            // $cart->save();
            return $this->returnSuccessMessage('cart updated successfully');


        }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
