<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class CookController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cooks=User::with('dishes')
        ->whereRoleIs('cook')->get();
        return $this->returnData('cooks',$cooks);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//cook wishlist
    {
        $ids=auth('api_customer')->user()->favcooks->pluck('id')->toArray();

        $customer_id=auth()->guard('api_customer')->user()->id;
        $customer=User::where('id',$customer_id)->first();

        $cook=User::where('id',$request->id)->first();
        if(in_array($request->id , $ids)){
            $customer->favcooks()->detach($cook);
            return $this->returnSuccessMessage('successfully removed from wishlist');

        }else{
            $customer->favcooks()->attach($cook);
            return $this->returnSuccessMessage('successfully added to wishlist');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$id)
    {
        $cook=User::whereRoleIs('cook')
        ->where('id',$id)
        ->with('dishes.sections')
        ->with(['dishes'=>function($q){
            $q->orderBy('available', 'DESC');
        }])
        ->where('active','1')
        ->with('address')
        ->first();

        $customers=[];
        if(isset($cook->customers)){
            foreach($cook->customers as $customer){
                array_push($customers , $customer->id);
            }
        }
        $cook['custs']=$customers;
        return response()->json($cook,200);
    }

    public function wishlist($lang ,$items){

        $ids=auth('api_customer')->user()->favcooks->pluck('id');
        $cooks=User::whereIn('id',$ids)->orderBy('availability', 'desc')
        ->with(['dishes'=>function($q){
            $q->orderBy('available', 'DESC');
        }])
        ->whereRoleIs('cook')
        ->paginate($items);
        // $cooks= $cooks->paginate($items);
        return $this->returnData('cooks',$cooks);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
