<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use App\Models\Discount;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;
use Illuminate\Support\Facades\Auth;

class AdvertisementController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth('api_customer')->user()) {
            $offs=[];
            $offers=Advertisement::all();

            $customer=auth('api_customer')->user()->VIP;

            if ($customer == 1) {
                return $this->returnData('offers',$offers);

            }else{
                    foreach ($offers as $offer) {

                        $coupon=Discount::where('name', $offer->coupon)->first();

                        if(isset($coupon) && $coupon->customers != 'vip'){
                            array_push($offs ,$offer);
                        }
                    }
                return $this->returnData('offers',$offs);

            }

        }else{
            $offers=Advertisement::leftJoin("discounts", "discounts.name", "=", "advertisement.coupon")
            ->where('customers', '!=','vip')->get();
            return $this->returnData('offers',$offers);

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
