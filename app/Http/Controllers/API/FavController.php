<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class FavController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $favs=auth('api_customer')->user()->favcooks;
        return($favs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang,Request $request)
    {

        $favs_id=[];
        $favcook=User::find($request->cook_id);
        $user=User::find(auth('api_customer')->user()->id);
        $cooks=$user->favcooks;
        if(isset( $cooks)){
            foreach($cooks as $cook){
                array_push($favs_id ,$cook->id);
            }

        }

        if(!in_array($request->cook_id ,$favs_id )){
            $user->favcooks()->attach($favcook);
            return $this->returnSuccessMessage('successfully added to wishList');

        }else{
            $user->favcooks()->detach($favcook);
            return $this->returnSuccessMessage('successfully removed from wishList');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
