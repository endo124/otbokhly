<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\User;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class ApplyCouponController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request )
    {

        if($request->name){
            $coupon=Discount::where('name',$request->name)->first();
            if(isset($coupon)){
                // $cook_name=User::find($request->cook_id)->name;
                if($coupon->cooks == 'all'){
                    return $this->returnData('coupon',$coupon);
                }elseif(in_array($request->cook_id,json_decode($coupon->cooks))){
                    return $this->returnData('coupon',$coupon);

                }else{
                    return $this->returnError('this cook is not included');
                }
            return $this->returnData('coupon',$coupon);
            }
            else{
                return $this->returnError('no match coupon');
            }
        }
    }


    public function checkPoints(Request $request )
    {
        $setting=Setting::first();
        $order_points= ($request->sub_total  * $setting->reward_points)/$setting->reward_amount;

        $user_points=auth('api_customer')->user()->points;
        $total_points=auth('api_customer')->user()->points+ $order_points;
        if($request->point == 1 &&  $total_points>0){

            $discount_percentage=($setting->loyality_percentage * $total_points) / $setting->loyality_point;
            $discount=($request->sub_total*$discount_percentage)/100;
            if($request->sub_total >=$setting->loyality_max ){

                if($discount  > $setting->loyality_max){
                    $ammount_back=$discount - $setting->loyality_max ;
                    $discount=$setting->loyality_max;

                    $back_points=($ammount_back * $setting->reward_points) / $setting->reward_amount;
                    // $max_point=($setting->reward_points * $setting->loyality_max )/ $setting->reward_amount ;

                    $points=['disc'=>$discount,'order_points'=>$total_points-$user_points ,'back_points'=>$back_points,'user_points'=>$user_points];
                    return $this->returnData('points',$points);
                }
                $points=['disc'=>$discount,'order_points'=>$total_points-$user_points,'user_points'=>$user_points];
                return $this->returnData('points',$points);

            }else{

                return $this->returnError('Your can\'t use loyality points system with this order');

            }

        }
        else{
            return $this->returnError('You haven\'t any points yet');

        }
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
