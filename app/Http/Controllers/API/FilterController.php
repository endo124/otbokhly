<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Addons;
use App\Models\Category;
use App\Models\CityTranslation;
use App\Models\Cusine;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Container\Container;
use App\Traits\GeneralTrait;

class FilterController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
            // $addons= collect(Addons::all());
            $cusines=Cusine::all();
            $categories=Category::all();
            if($lang == 'ar'){
                return response()->json(['data'=>[
                // ['name'=>'اضافات','values'=>$addons]
                ['name'=>'اطباق','values'=>$cusines]
                ,['name'=>'اقسام','values'=>$categories]
                ]],200);
            }else{
                return response()->json(['data'=>[
                // ['name'=>'addons','values'=>$addons]
                ['name'=>'cusines','values'=>$cusines]
                ,['name'=>'categories','values'=>$categories]
                ]],200);
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($filters,$lang,$governorate)
    {
        $city=CityTranslation::where('name',$governorate)->first();
        $dishes=[];
        if(isset($city)){
            $cooks=User::whereRoleIs('cook')
            ->where('city_id',$city->city_id)
            ->where('active',1)
            ->pluck('id');
            $search=$filters['search'];
            $dishes_id=DishTranslation::where('name', 'LIKE', "%$search%")->pluck('dish_id');
            // $dishes=Dish::with('users')
            // ->with('cusines')
            // ->with('sections')
            // ->with('addons.users')
            // ->whereIn('user_id',$cooks)
            // ->whereIn('id',$dishes_id)
            // ->get();

        // $dishes_id=DishTranslation::where('name', 'LIKE', "%$filters->search%")->pluck('dish_id');
        $dishes=Dish::whereIn('user_id',$cooks)
        ->with('sections')
        ->with('addons.users')
        ->with('cusines')
        ->with('users')
        // ->when($filters['search'] ,function($q) use ($dishes_id){//return dishes where name = search
        //     $q->whereIn('id',$dishes_id)->get();
        // })
        ->where(function($q) use ($filters){//return dishes where have cusines =request['cusine']

            if($filters['cusines']){
                $q->whereHas('cusines',function($res) use ($filters){
                    $res->whereIn('cusines.id',$filters['cusines']);
                })->get();
            }
        })

        ->where(function($q) use  ($filters){//return dishes where have categories =request['categories']
            if($filters['categories']){
                $q->whereHas('categories',function($res) use ($filters){
                    $res->whereIn('categories.id',$filters['categories']);
                })->get();
            }
        })

        ->where(function($q) use  ($filters){//return dishes where availabe from to
            if($filters['avaliable']){
                $q->whereHas('users' ,function($res) use ($filters){
                        $res->where('work_from'  ,'<=',  $filters['avaliable']['from'] )
                        ->Where('work_to' ,'>=', $filters['avaliable']['to']);
                })->get();
            }

        })->get();

        $dishes=collect($dishes)->where('lowest','>=',$filters['price']['min'])->where('lowest','<=',$filters['price']['max'] );//return dishes where price between;



        if(isset($filters['sort'])){
            if($filters['sort'] == 'price'){
                $dishes=$dishes->sortBy('lowest');

            }else{
                $dishes=$dishes->sortBy('name');

            }
        }

    }
    $dishes=$dishes->values();

    if(count($dishes) > 0){
        $pageSize = 5;//custom paginate size

        $dishes= $this->custom_paginate($dishes);
        // $dishes= $this->paginate($dishes, $pageSize);

        return $this->returnData('dishes',$dishes);

    }else{
        return $this->returnError('there is no data');
    }

    // return collect($dishes)->paginate(5);



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function custom_paginate($data)
    {
        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($data);

        //Define how many items we want to be visible in each page
        $per_page = 5;

        //Slice the collection to get the items to display in current page
        $currentPageResults = $collection->slice(($currentPage - 1) * $per_page, $per_page)->values();

        //Create our paginator and add it to the data array
        $data['results'] = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);

        //Set base url for pagination links to follow e.g custom/url?page=6
        return $data['results']->setPath(request()->url());
    }

    // public static function paginate(Collection $results, $pageSize)
    // {

    //     $page = Paginator::resolveCurrentPage('page');

    //     $total = $results->count();

    //     return self::paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
    //         'path' => Paginator::resolveCurrentPath(),
    //         'pageName' => 'page',
    //     ]);

    // }
    // protected static function paginator($items, $total, $perPage, $currentPage, $options)
    // {
    //     return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
    //         'items', 'total', 'perPage', 'currentPage', 'options'
    //     ));
    // }



    function getaddress(Request $request  , $lang,$location)
    {
       $filters=['search'=>$request->search ,'cusines'=> $request->cusines ,'sort'=> $request->sort ,'price'=> $request->price ,'avaliable'=> $request->avaliable ,'categories'=> $request->categories];

       $url = 'https://maps.google.com/maps/api/geocode/json?latlng='.$location.'&key=AIzaSyArUJhAM_MY5imJh7g6l6-rgcBOMyOS63s';
       $json = @file_get_contents($url);
       $data=json_decode($json);

       $status = $data->status;


       if($status=="OK")
       {
            $address= $data->results;
            $address_components= $address[0]->address_components;
            for ($i = 0; $i < count($address_components); $i++) {
                $address= $data->results;
                $address_components= $address[0]->address_components;
                $addressType = $address_components[$i]->types[0];
                if ($addressType == 'administrative_area_level_1') {
                    $governorate= $address[0]->address_components[$i]->short_name;
                }
            }
        }
       return $this->show($filters,$lang,$governorate);
    }
}
