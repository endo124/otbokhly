<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class AddressController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = auth('api_customer')->user();
		$addresses = $customer->address;
        // $addresses['default']=Address::find($customer->default_address);
        return $this->returnData('addresses',$addresses);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = auth('api_customer')->user();

		$validator = \Validator::make($request->all(), [
			'address' => 'required',
            'coordinates'=>'required',
            'name' => 'required',
            'phone' => 'numeric',
			'default' => 'required',
            'building'=>'required'

		]);
		if ($validator->fails()) {
			return response()->json($validator->errors());
		}
		$address = Address::create([
            'user_id' => $customer->id,
			'address' => $request->address,
            'name' =>  $request->name,
            'coordinates'=>$request->coordinates,
            'phone'=>$request->phone,
            'building'=>$request->building
        ]);
        if ($request->default == 1) {

			$customer->update(['default_address' => $address->id]);
		}

        return $this->returnSuccessMessage('address added successfully ');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = auth('api_customer')->user();
		$validator = \Validator::make($request->all(), [
			'address' => 'required',
            'coordinates'=>'required',
            'name' => 'required',
			'phone' => 'numeric',
			'default' => 'required',
            'building'=>'required'

		]);
		if ($validator->fails()) {
			return response()->json($validator->errors());
		}
		$address = Address::where('id', $id)->update([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'phone' => $request->get('phone'),
			'user_id' => $customer->id,
            'building'=>$request->building

		]);
        if ($request->get('default') == 1) {
			$customer->update(['default_address' => $id]);
		}
        return $this->returnSuccessMessage('address updated successfully ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = auth('api_customer')->user();
        $address = Address::find($id);
        $customer->where('default_address', $id)->update(['default_address' => null]);

		// if (is_null($address)) {
		// 	return response()->json([
		// 		'error' => true,
		// 		'message' => 'Address Not Found.',
		// 	], 500);
		// } else {
			Address::where('id', $id)->delete();
            return $this->returnSuccessMessage('address deleted successfully ');

		// }
    }
}
