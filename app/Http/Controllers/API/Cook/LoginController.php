<?php

namespace App\Http\Controllers\API\Cook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Validator;
class LoginController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'email' => 'required|email|',
            'password' => 'required',
         ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $credentials = request(['email', 'password']);
        if (!  $token=auth()->guard('api_cook')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $cook=User::where('id',auth('api_cook')->user()->id)->first();
        if($cook->address[0]){
            $address=$cook->address[0];
        }
        return $this->respondWithToken($token,$cook);
    }

    public function logout()
    {
        auth('api_cook')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token,$cook)
    {
        return response()->json([
            'cook'=>$cook,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api_cook')->factory()->getTTL() * 60,
        ]);
    }
}
