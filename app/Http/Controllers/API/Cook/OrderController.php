<?php

namespace App\Http\Controllers\API\Cook;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Dish;
use App\Models\DishTranslation;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Traits\GeneralTrait;
use Kreait\Firebase\Factory;

class OrderController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();
        return $this->returnData('orders',$orders);
    }

    public function currentorder(){

        $currentActivities=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
        ->where('status',1)
        ->orderBy('id', 'DESC')
        ->get();
        return $this->returnData('currentorder',$currentActivities);

    }

    public function accepted(){

        $currentActivities=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
        ->where('status', '!=', 1)//not pending
        ->where('status', '!=', 0)// not rejected
        // ->where('status', '!=', 4)//not shipped
        ->where('status', '!=', 6)//not delivered
        ->orderBy('id', 'DESC')
        ->get();
        return $this->returnData('acceptedorder',$currentActivities);

    }
    public function delivered(){

        $currentActivities=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
        ->where('status',5)
        ->orderBy('id', 'DESC')
        ->get();
        return $this->returnData('deliveredorder',$currentActivities);

    }

    public function rejected(){
        $rejected=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
        ->where('status',0)
        ->orderBy('id', 'DESC')
        ->get();
        return $this->returnData('rejectedorder',$rejected);

    }

    public function accept($id){

        $order= Order::where('id',$id)->first();
       if($order->status == 1){
        $order->update([
            'status'=>2
        ]);

        $admin=User::first();
        $admin->update(['revenue'=>doubleval($admin->revenue) + $order->total_price + doubleval($order->delivery_fees) ]);
        return $this->returnSuccessMessage('order accepted successfully');

       }

    }

    public function inprogress($id){

        $order= Order::where('id',$id)->first();
       if($order->status == 2){
        $order->update([
            'status'=>3
        ]);
        return $this->returnSuccessMessage('order now in progress ');

       }

    }
    public function shipped($id){

        $order= Order::where('id',$id)->first();
        $order->update([
            'status'=>4
        ]);
        $date=$order->date;
        $time=$order->time;
        $date=(string)strtotime($date . " " . $time);
        $cook=User::find($order->cook_id);
        $customer= User::Find($order->user_id);


        // $address=Address::find($order->customer_address_id);
        // if(isset($address)){
            $customer_coordinates=explode(",",$order->coordinates);
            // dd($customer_coordinates);
        // }
        // else{

        //     $coordinates='31.0413814,31.3478201';
        // }
            // if(isset($address->phone)){
            //     $customer_phone=$address->phone;
            // }else{
            //     $customer_phone=$customer->phone;
            // }
            // return($address);
            if(isset($cook->address[0])){
                $coordinates=explode(",",$cook->address[0]->coordinates);

            }
            else{
            // return response()->json(['error'=>$validator->errors()], 401);

                $coordinates='31.0413814,31.3478201';
                $coordinates=explode(",",$coordinates);

                $address="test";
            }


        $details=$order->orderEntry;
        // dd($order);
        $orders=[];
        $notes=[];

        foreach ( $details as $ord) {
            $item=[];
            $item['id']=(string)$ord['id'];
            $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
            if($ord['size_en'] =='small'){
                $portion_price_index=2;
            }elseif ($ord['size_en'] =='large') {
                $portion_price_index=0;

            }else{
                $portion_price_index=1;

            }

            $dish_price=Dish::find($ord['id'])->portions_price[$portion_price_index];
            $item['price']=(string)$dish_price;
            $item['quantity']=(int)$ord['quantity'];

            if (isset($ord['notes'])) {
                $item['note']=(string)$ord['notes'];
                array_push($notes, $item['note']);
            }
        array_push($orders, $item);
        }

        foreach ($details as $ord) {
              foreach ( $ord['addons'] as $index=>$add) {
                $item['id']=(string)$index;
                $item['name']=$add['add_ar'];
                $item['price']=(string)$add['add_price'];
                $item['quantity']=(int)$add['qty'];
                array_push($orders, $item);
            }
        }

        if(isset($order->customer_address_id)){

            $add=Address::find($order->customer_address_id);

            if(isset($add)){
                if($add->phone){
                    $customer_phone=$add->phone;
                }else{
                    $customer_phone=$customer->phone;
                }

            }else{
                $customer_phone=$customer->phone;
            }
        }
        else{
            $customer_phone=$customer->phone;
        }
        // dd($orders);
        $date=strtotime($order->date . " " . $order->time);
        $customer= User::Find($order->user_id);
        $factory = (new Factory)
        ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        $database = $factory->createDatabase();

        // if(isset($order->customer_address_id)){
        //     $add=Address::find($order->customer_address_id);
        //     if(isset($add)){
        //         $customer_phone=$add->phone;
        //     }
        // }
        // else{
        //     $customer_phone=auth('api_customer')->user()->phone;
        // }
        $getUser = $database
        ->getReference('users')
        ->orderByChild('email')
        ->equalTo('info@otbokhly.com')
        ->getValue();
        $user=array_keys($getUser);
        $newOrder = $database
        ->getReference('/users/'.$user[0].'/tasks')
        ->push(
            [
        'deliver'=>['address'=>['lat'=>$customer_coordinates[0],'lng'=>$customer_coordinates[1],
        'name'=>$order->address],
            'date' => (string)strtotime(date("Y-m-d H:i:s",$date)),
            'description' => '',
            'email'=>$customer->email,
            'name' => $customer->name,
            'orderId' => (string)$order['id'],
            'phone' =>  $customer_phone],
            'distance'=>'',
            'driverId'=>'',
            'estTime'=>'',
            // 'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>'test'],
            'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>$cook->address[0]->address],
            'date' => (string)strtotime(date("Y-m-d H:i:s",$date)),
            'description' => '',
            'email'=>$cook->email,
            'name' => $cook->name,
            'orderId' => (string)$order['id'],
            'phone' =>  $cook->phone ?? ''],
            'status'=>-1,
            'orderItems' => $orders,
            'type'=>'pickupOnDelivery',
            'note'=>$order->note,
            'notes'=>$notes,
            'fees'=>(string)$order->delivery_fees ?? '',
            'taken'=>false,
            'discount_price'=>$order->discount_price ?? '',
            'paid'=>0,
            'vendor_taxes'=>$order->vendor_taxes ?? '',
            'delivery_taxes'=>$order->delivery_taxes ?? '',
            'sub_total'=>$order->sub_total,
            'total_price'=>$order->total_price,

        ]);





        return $this->returnSuccessMessage('order shipped successfully');

    }

    public function reject($id){
        $order= Order::where('id',$id)->first();
        $order->update([
            'status'=>0
        ]);
        return $this->returnSuccessMessage('order rejected successfully');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
