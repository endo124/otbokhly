<?php

namespace App\Http\Controllers\API\Cook;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use App\Models\Withdraw;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;
use Validator;

class WithdrawController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $withdraw=Withdraw::where('user_id',auth()->guard('api_cook')->user()->id)->get();
        return $this->returnData('withdraw',$withdraw);

    }

    public function wallet(){
        $wallet=auth('api_cook')->user()->profit;
        return $this->returnData('wallet',$wallet);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,$lang)
    {
        $cook=User::find(auth()->guard('api_cook')->user()->id);

        $validator=Validator::make($request->all(), [
            'amount' => 'required',
         ]);
        if ($validator->fails()) {
        }
        if($cook->profit < $request->amount){
            return $this->returnSuccessMessage('you have no moeny to make withdraw request');

        }else{
            $withdraw=Withdraw::create([
                'user_id'=>$cook->id,
                'amount'=>$request->amount,
                'status'=>0
            ]);
            $totalProfit=$cook->profit-$request->amount;
            $cook->update(['profit'=>$totalProfit]);


        $notify=['customer_id'=>$cook->id,'notify'=>'withdraw'];
        $notify=json_encode($notify);
        $notification=Notification::create([
            'notify'=>$notify
        ]);
            return $this->returnSuccessMessage('request sent successfully');
        }


    }

    public function cancel($id ,$lang)
    {
        $withdraw=Withdraw::find($id);
        $amount=$withdraw->amount;
        $cook=User::find(auth()->guard('api_cook')->user()->id);
        $profit_past=$cook->profit;
        $cook->update(['profit'=>$profit_past+$amount]);
        $withdraw->delete();
        return $this->returnSuccessMessage('request canceled successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $withdraw=Withdraw::find($id);

        $withdraw->delete();
        return $this->returnSuccessMessage('request deleted successfully');
    }
}
