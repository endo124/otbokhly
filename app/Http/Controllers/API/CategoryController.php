<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryTranslation;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;
use App\Models\CityTranslation;
use App\Models\User;
use App\Models\Dish;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    use GeneralTrait;


    protected $key;

    public function __construct(){

        $this->key=Setting::first()->googleapikey;

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang,$governorate)
    {
        $city=CityTranslation::where('name', $governorate)->first();
        if (isset($city)) {
            $cooks=User::with('dishes')
            ->whereRoleIs('cook')
            ->where('city_id', $city->city_id)
            ->where('active', 1)
            ->pluck('id');


            $categories_id=Dish::whereIn('user_id', $cooks)->groupBy('category_id')->pluck('category_id');

            $categories=CategoryTranslation::whereIn('category_id', $categories_id)->where('locale', $lang)->get();

            if(!is_null($categories)){
                return $this->returnData('categories', $categories);
            }else {

                return $this->returnError('there is no categories in this city');

            }
            // $categories=Category::all();
        }
    }


    function getaddress($lang,$location)
    {


       $url = 'https://maps.google.com/maps/api/geocode/json?latlng='.$location.'&key='.$this->key;
       $json = @file_get_contents($url);
       $data=json_decode($json);
       $status = $data->status;

       if($status=="OK")
       {
            $address= $data->results;
            $address_components= $address[0]->address_components;
            for ($i = 0; $i < count($address_components); $i++) {
                $address= $data->results;
                $address_components= $address[0]->address_components;
                $addressType = $address_components[$i]->types[0];
                if ($addressType == 'administrative_area_level_1') {
                    $governorate= $address[0]->address_components[$i]->short_name;
                }
            }
        }
       return $this->index($lang,$governorate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
