<?php

namespace App\Http\Controllers\API;
use App\Events\NewOrder;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\AddonsTranslation;
use App\Models\SectionTranslation;
use App\Models\Addons;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use App\Traits\GeneralTrait;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Illuminate\Support\Facades\DB;

use App\Models\Address;
class OrderController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
        $orders=Order::where('orders.user_id',auth('api_customer')->user()->id)
        ->WhereNotNull('status')
        ->leftJoin('discounts' ,'orders.discount_id' , '=' , 'discounts.id')

        ->rightJoin('user_translations' ,'orders.cook_id' , '=' , 'user_translations.user_id')
        ->where('locale',$lang)
        ->rightJoin('users' ,'orders.cook_id' , '=' , 'users.id')
        // ->select('orders.id','orders.user_id','orders.cook_id','date','time','orderEntry','total_price','delivery_fees','address','payment_method','payment_status','delivery_type','status','assign','name','images','taxes_type','taxes')
        ->select('orders.id','orders.user_id','orders.cook_id','date','time','orderEntry','total_price','orders.discount_price As discount_value','delivery_fees','address','status','assign','user_translations.name','images','note',
        'discounts.total As discount_price','discounts.discount_type','discounts.discount','orders.delivery_taxes','orders.discount_price','orders.sub_total','orders.vendor_taxes',
        )

        ->get();

        return $this->returnData('orders',$orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'time' => 'required',
            'date' => 'required',
            'address' => 'required',
         ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }

        $cook=User::where('id',$request->cook_id)->first();
        if(isset($cook->address[0])){
            $coordinates=explode(",",$cook->address[0]->coordinates);

        }
        else{

            $coordinates='31.0413814,31.3478201';
        }

        $order=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();
        if( is_null($order)){
            return response()->json(['error'=>'Order successfull - CT'], 401);
        }
        $details=$order->orderEntry;
        $orders=[];
        foreach ( $details as $or) {
            $item=[];
            $item['id']=(string)$or['id'];
            $item['name_ar']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
            $item['name_en']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'en'])->first()->name;
            $item['quantity']=(int)$or['qty'];
            $item['notes']=$or['notes'];

            $item['addons']=[];
            $section_id=Dish::find($item['id'])->sections->id;
            $item['section_ar']=SectionTranslation::where(['section_id'=>$section_id,'locale'=>'ar'])->first()->name;
            $item['section_en']=SectionTranslation::where(['section_id'=>$section_id,'locale'=>'en'])->first()->name;
            $item['size_en']=$or['size'];
            if($or['size'] =='small'){
                $item['size_ar']='صغير';
                $portion_price_index=2;
            }elseif ($or['size'] =='large') {
                $item['size_ar']='كبير';
                $portion_price_index=0;

            }else{
                $item['size_ar']='متوسط';
                $portion_price_index=1;

            }
            $sub_total=0;
            if(isset($or['addons'])){
                $adds=[];
                $adds_price=0;
                foreach ($or['addons'] as  $addon) {
                    $add=[];
                    $add['add_ar']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'ar'])->first()->name;
                    $add['add_en']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'en'])->first()->name;
                    $add['add_price']=Addons::find($addon['id'])->price;
                    $add['qty']=$addon['qty'];

                    $adds_price+=$add['qty']* $add['add_price'];
                    array_push($adds,$add);
                }
                $item['addons']=$adds;
            }

            $price=Dish::find($or['id'])->portions_price[$portion_price_index];
            $item['price']=$price+$adds_price;

            $sub_total+=$item['price'];
            array_push($orders, $item);
        }

        $setting=Setting::first();

        if($setting->loyality_min){//points system
            $order_points= ($sub_total * $setting->reward_points)/$setting->reward_amount;
        }

        $customer_points=auth('api_customer')->user()->points ?? 0;
        auth('api_customer')->user()->update(['points'=>$customer_points+$order_points]);
        // dd(auth('api_customer')->user()->points);


        if($request->point == 1){
            auth('api_customer')->user()->update(['points'=>auth('api_customer')->user()->points - $request->order_points+ $request->back_points]);
        }


        $address=Address::find($request->address);
        if(isset($address->phone)){
            $customer_phone=$address->phone;
        }else{
            $customer_phone=auth('api_customer')->user()->phone;
        }

        $admin=User::first();
        $order->update([
            'vendor_taxes'=>$request->vendor_taxes,
            'delivery_taxes'=>$request->delivery_taxes,
            'discount_price'=>$request->discount_price ?? '',
            'sub_total'=>$sub_total,
            'delivery_fees'=>$request->delivery_fees,
            'total_price'=>(string)$request->total_price,
            'point_discount'=>$request->point_discount,
            'time'=>$request->time,
            'date'=>$request->date,
            'address'=>$address['address'],
            'customer_address_id'=>$request->address,
            'orderEntry'=>$orders,
            'status'=>'1',
            'note'=>$request->note,
            'coordinates'=>$address['lat'].','.$address['long']
        ]);

        if($request->coupon_id != null){
            $coupon=Discount::find($request->coupon_id);
            $order = $order->coupon()->associate($coupon);
            $order->save();

        }


        // $details=$request->products;
        // $orders=[];
        // foreach ( $details as $order) {
        //     $item=[];
        //     $item['id']=(string)$order['id'];
        //     $item['name']=$order['orderEntry']['name'];
        //     $item['price']=(string)$order['orderEntry']['price'];
        //     $item['quantity']=(int)$order['orderEntry']['qty'];
        //     array_push($orders, $item);
        // }
        // $date=strtotime($request->date . " " . $request->time);
        $customer=User::find(auth()->guard('api_customer')->user()->id);
        // $factory = (new Factory)
        // ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        // $database = $factory->createDatabase();


        // $getUser = $database
        // ->getReference('users')
        // ->orderByChild('email')
        // ->equalTo('info@otbokhly.com')
        // ->getValue();
        // $user=array_keys($getUser);

        // $newOrder = $database
        // ->getReference('/users/'.$user[0].'/tasks')
        // ->push(
        //     [
        // 'deliver'=>['address'=>['lat'=>$request->address['lat'],'lng'=>$request->address['long'],
        // 'name'=>$request->address['address']],
        //     'date' => (string)strtotime(date("Y-m-d H:i:s",$date)),
        //     'description' => '',
        //     'email'=>$customer->email,
        //     'name' => $customer->name,
        //     'orderId' => (string)$order['id'],
        //     'phone' =>  $request->customer_phone],
        //     'distance'=>'',
        //     'driverId'=>'',
        //     'estTime'=>'',
        //     'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>$cook->address[0]->address],
        //     'date' => (string)strtotime(date("Y-m-d H:i:s",$date)),
        //     'description' => '',
        //     'email'=>$customer->email,
        //     'name' => $customer->name,
        //     'orderId' => (string)$order['id'],
        //     'phone' =>  $request->customer_phone],
        //     'status'=>-1,
        //     'orderItems' => $orders,
        //     'type'=>'pickupOnDelivery',
        //     'taken'=>false,
        // ]);




        // $notify=json_encode($notify);
        // dd($notify);
        $notification=Notification::create([
            'notify'=>'order',
            'customer_id'=>$customer->id,
            'cook_id'=>$cook->id
        ]);
        event(new NewOrder($order->id));

        $this->sendWebNotification($order);

        return $this->returnData('msg','success');
    }



    public function get_paid(Request $request)
    {
        $factory = (new Factory)
		->withServiceAccount(__DIR__.'/logista-b796f05ac902.json')
		;
		$database = $factory->createDatabase();


		//get value of all the unique ID
		$getUser = $database
			   ->getReference('users')
			   ->orderByChild('email')
			   ->equalTo('info@otbokhly.com')
			   ->getValue();
			   $user=array_keys($getUser);

			   $getZones = $database
			   ->getReference('/users/'.$user[0].'/geoFences')

			   ->getValue();
		$zones=(array_values($getZones));
		$arr_check=[];
		foreach($zones as $zo)
		{
			$arr_x=[];
            $arr_y=[];
            foreach($zo['cords'] as $zone)
            {
                array_push($arr_x,$zone['lat']);
                array_push($arr_y,$zone['lng']);
            }
            $points_polygon = count($arr_x) - 1;  // number vertices - zero-based array
            // $cords=explode(",", str_replace('"', '', $request->coordinates_new));
             // dd('fdhh');
            //  dd($cords);


            $address=Address::find($request->address);
                $coordinates=explode(",", $address->coordinates);
                // dd($coordinates);
                if($coordinates[0] !=''){
                    $check=$this->is_in_polygon($points_polygon, $arr_x, $arr_y,$coordinates[0],$coordinates[1]);

                }else{
                    return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
                }
                // dd($coordinates[0],$coordinates[1]);
                if ($check){
                    array_push($arr_check,$zo);
                }
        }
        if($arr_check==[])
        {
            return response()->json(['error' => true, 'message' => 'Out Of Zones']);
        }
        return response()->json([
            'success' => true,
            'message' => 'Get Orders successfully.',
            'data' => array_values($arr_check[0]['flags']),
        ], 200);


    }

    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
	{
	  $i = $j = $c = 0;
	  for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
		if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
		 ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
		   $c = !$c;
      }
	  return $c;
	}



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function updateFcnToken(Request $request){



        $devices[$request->device_id] = $request->token;
        // dd($devices);
        $user=auth('api_customer')->user();
        // dd((array)(json_decode($user->device_key)));
        $devs=(array)(json_decode($user->device_key));
        foreach ($devs as  $key=>$dev) {
            if( $key!= $request->device_id){
                $devices[$key] = $dev;
            }
        }

        $user->update(['device_key'=>json_encode($devices)]);
        return $this->returnData('msg','token updated successfully');

    }



    public function sendWebNotification($order)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $serverKey='AAAAbXpIFTM:APA91bGHGbsROOwzn4EK-P9HXuHzBB_1yplOuG5QsknXgUQKkdCtECIP2MndU7O5IUKd5ZLQAHHJwPN7GwyXrNb8_dC-nsvnvjmuu7GXrVXwEPpBF0yelBdngg9qYEVMziR4SGW5mvU1';

        $user=User::find($order->cook_id);
        // dd($user->device_key );
       $devices= ( array)(json_decode($user->device_key));
       $devs=[];
       foreach ($devices as $device) {
           array_push($devs, $device);
       }

        $data = [
            "registration_ids" =>array_values( $devs),
            "notification" => [
                // "id" => $order->id,
                // "body" => $order->body,
                // "time" =>$order->updated_at,
                // "status"=> $order->status
                "body" => $order->status,
                "title" =>$order->id,
                "sound" => "notify.mp3",

            ]
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        // FCM response
        return($result);
    }



}
