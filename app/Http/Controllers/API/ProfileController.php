<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTranslation;
use Validator;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class ProfileController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang,$id)
    {
        $customer =User::find($id);
        return $this->returnData('customer',$customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request ,$lang){
        $customer=User::find(auth()->guard('api_customer')->user()->id);
        $validator=\Validator::make($request->all(), [
            'name'=>'required',
            // 'name_en'=>'required',
            'email'=>'required|unique:users,id,'.$customer->id,
            'phone'=>'required|unique:users,id,'.$customer->id,
            // 'password'=>'required|confirmed'
          ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        if ($request->file('image')) {

            $ext=$request->file('image')->getClientOriginalExtension();
            $image_name=time().'.'.$ext;
            $path='backend/images/customers';
            $request->file('image')->move($path,$image_name);
            if ($customer->images) {
                \Storage::disk('customers')->delete($customer->images);
            }
        }
        else{
            $image_name=$customer->images;
        }

        $customer->update([
            'phone'=>$request->phone,
            'email'=>$request->email,
            'images'=>$image_name,

        ]);
        // $customer_en=UserTranslation::where('user_id',auth()->guard('api_customer')->user()->id)->where('locale','en')->first();
        // $customer_en->updateOrCreate([
        //     'name'=>$request->name
        // ]);
        $customer=UserTranslation::where('user_id',auth()->guard('api_customer')->user()->id)->where('locale',$lang)->update([
            'name'=>$request->name
        ]);

        return $this->returnSuccessMessage('data edit successfully');

       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
