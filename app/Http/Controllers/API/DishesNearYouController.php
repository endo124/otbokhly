<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CityTranslation;
use App\Models\Cusine;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Setting;
class DishesNearYouController extends Controller

{
    use GeneralTrait;



    protected $key;

    public function __construct(){

        $this->key=Setting::first()->googleapikey;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($items,$lang,$governorate,$cat_id)
    {
        $city=CityTranslation::where('name',$governorate)->first();
        if(isset($city)){
            $cooks=User::with('dishes')
            ->whereRoleIs('cook')
            ->where('city_id',$city->city_id)
            ->where('active',1)
            ->pluck('id');

            if(is_null($cat_id)){
                $dishes=Dish::orderBy('available', 'DESC')->with('users')
                // ->with(['users'=>function($q){
                //     $q->orderBy('availability', 'DESC');
                // }])
                ->with('cusines')
                ->with('sections')
                ->with('addons.users')
                ->whereIn('user_id',$cooks)->get();
                $dishes=$dishes->sortByDesc('users.availability')->values();

                $dishes= $this->custom_paginate($dishes ,$items);

               return $this->returnData('dishes',$dishes);

            }else{
                $dishes=Dish::where('category_id',$cat_id)->orderBy('available', 'DESC')
                ->with(['users'=>function($q){
                    $q->orderBy('availability', 'DESC');
                }])
                ->with('cusines')
                ->with('sections')
                ->with('addons.users')->get();

                $dishes=$dishes->sortByDesc('users.availability')->values();

                $dishes= $this->custom_paginate($dishes ,$items);
                return $this->returnData('dishes',$dishes);

            }



        }else{
            return $this->returnError('there is no data in this city');
        }

    }

    function getaddress($items,$lang,$location,$cat_id=null)
    {

       $url = 'https://maps.google.com/maps/api/geocode/json?latlng='.$location.'&key='.$this->key;
       $json = @file_get_contents($url);
       $data=json_decode($json);
       $status = $data->status;


       if($status=="OK")
       {
            $address= $data->results;
            $address_components= $address[0]->address_components;
            for ($i = 0; $i < count($address_components); $i++) {
                $address= $data->results;
                $address_components= $address[0]->address_components;
                $addressType = $address_components[$i]->types[0];
                if ($addressType == 'administrative_area_level_1') {
                    $governorate= $address[0]->address_components[$i]->short_name;
                }
            }
        }
       return $this->index($items,$lang,$governorate,$cat_id);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    function custom_paginate($data ,$items)
    {

        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($data);

        //Define how many items we want to be visible in each page
        $per_page = $items;

        //Slice the collection to get the items to display in current page
        $currentPageResults = $collection->slice(($currentPage - 1) * $per_page, $per_page)->values();

        //Create our paginator and add it to the data array
        $data['results'] = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);

        //Set base url for pagination links to follow e.g custom/url?page=6
        return $data['results']->setPath(request()->url());
    }

}
