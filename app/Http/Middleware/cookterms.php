<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use App\Models\User;
use Closure;

class cookterms
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->guard('admin')->user()->roles[0]->name =='cook'){
            $cook=User::find( auth()->guard('admin')->user()->id);

            if($cook->terms == 0){

                return \Redirect::route('cook.edit', $cook->id);
                // return \Redirect::to('dashboard/cook/'.$cook->id.'/edit');
                // $cook=User::where('id',auth()->guard('admin')->user()->id)->first();
                // $setting=Setting::first();
                // $terms=$setting->terms;
                // // dd($cook,$terms);
                // dd($request);
                // return $next;

            }
        }
        return $next($request);

    }
}
