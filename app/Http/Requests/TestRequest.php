<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') ||request()->isMethod('patch') ?
         $this->onUpdate() :  $this->onPost();

    }



    public function onPost(){

        return [

        ];
    }
    public function onUpdate(){
        return [

        ];
    }

    public function attributes()
    {
        return [];
    }
}
