<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Setting;
use Illuminate\Console\Command;

class Rejection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:rejection';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'reject order if not accepted after n mins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        $orders=Order::where('status' , 1)->get();
        $rejection_time=Setting::first()->rejection;
        if($rejection_time == ''){
            $rejection_time=5;
        }
        foreach ($orders as $order) {
            $order_date_time=$order->date." ".$order->time;

           if(strtotime("now")  >= strtotime($order_date_time)+(60*(int)$rejection_time)){
                $order->update(['status'=>0]);
           }
        }
    }
}
