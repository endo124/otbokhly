<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class Addons extends Model implements TranslatableContract
{
    use Translatable;

    protected $guarded = [];

    protected $fillable=['addonsection_id','price'];

    public $translationForeignKey='addon_id';



    public $translatedAttributes = ['name'];
    public function dishes(){
        return $this->belongsToMany('App\Models\Dish','addon_dish','addon_id','dish_id')->withPivot('max', 'min', 'condition');
    }


    public function categories(){
        return $this->belongsToMany('App\Models\Category','addon_category','addon_id','category_id');
    }
    public function users(){
        return $this->belongsToMany('App\Models\User','addon_cook','addon_id','user_id')->withPivot('price');
    }

    public function addonsection(){
        return $this->belongsTo('App\Models\AddonSection','addonsection_id');
    }
}
