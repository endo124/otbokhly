<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Setting extends Model implements TranslatableContract
{
    use Translatable;

    protected $guarded = [];

    protected $fillable=[
        'googleapikey',
        'loyality',
        'cashback',
        'rejection',
        'reward',
        'reward_amount',
        'reward_points',
        'loyality_point',
        'loyality_percentage',
        'loyality_max',
        'loyality_min'];

    public $translatedAttributes = ['terms'];

}
