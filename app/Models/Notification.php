<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public  $table="notification";

    public $fillable=['notify','read','cook_id','customer_id'];
}
