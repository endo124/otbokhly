<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class AddonSection extends Model
{
    use Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['name'];
    public $table="addonsection";

    public function addons(){
        return $this->hasMany('App\Models\Addons','addonsection_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','user_id');

    }
}
