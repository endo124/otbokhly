<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=['discount_id','sub_total','delivery_taxes','vendor_taxes','delivery_fees','discount_price','customer_address_id','user_id','note','coordinates','vendor','assign','cook_id','payment_method','delivery_type','date','time','orderEntry','total_price','address','status'];


    public function users(){
        return $this->belongsTo('App\Models\User','user_id');

    }

    public function coupon(){
        return $this->belongsTo('App\Models\Discount','discount_id');
    }


    public function Review(){
        return $this->hasOne('App\Models\Reivew');
    }

    protected $casts = [
        'orderEntry' => 'array',
    ];



}
