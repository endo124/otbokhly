<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use DateTime;
use DateTimeZone;

class User extends Authenticatable implements TranslatableContract , JWTSubject
{
    use Translatable;
    use LaratrustUserTrait;
    use Notifiable;



    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public static function boot() {
        parent::boot();

        static::roleAttached(function($user, $role, $team) {
        });
        static::roleSynced(function($user, $changes, $team) {
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cookslider_id','email','active','profit','points','revenue','password','default_address','phone','images','info','date_of_birth','work_to','login','terms','work_from','device_key','city_id','commission','VIP','advertisment','contract','availability','commission_type',
    ];

    public $translatedAttributes = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        // 'work_to' => 'date:hh:mm',
        // 'work_from' => 'date:hh:mm',
    ];

    public $appends = ['liked'];


    public function dishes(){
        return $this->hasMany('App\Models\Dish','user_id');

    }

    public function address(){
        return $this->hasMany('App\Models\Address','user_id');
    }


    public function roles(){
        return $this->belongsToMany('App\Models\Role','role_user');
    }

    public function orders(){
        return $this->hasMany('App\Models\Order','user_id');

    }

    public function cities(){
        return $this->belongsTo('App\Models\City','city_id');

    }

    public function discounts(){
        return $this->belongsToMany('App\Models\Discount','discount_user','discount_id','user_id');
    }

    public function cookSliders(){
        return $this->hasMany('App\Models\CookSlider','user_id');

    }

    public function withdraws(){
        return $this->hasMany('App\Models\Withdraw','user_id');

    }

    public function addons(){
        return $this->belongsToMany('App\Models\Addons','addon_cook','user_id','addon_id')->withPivot('price');
    }

    public function feasts(){
        return $this->hasMany('App\Models\Feast');
    }

    public function addonsections(){
        return $this->hasMany('App\Models\AddonSection','user_id');
    }

    public function favcooks(){
        return $this->belongsToMany('App\Models\User','customer_cooks','customer_id','cook_id');
    }


    public function products(){
        return $this->belongsToMany('App\Models\Dish','wishlist','user_id','dish_id');
    }


    public function reviews(){
        return $this->hasMany('App\Models\Review','vendor_id');
    }

    public function avgRating()
    {
        return $this->reviews->avg('rating');
    }


    public function getAvailabilityAttribute($value)
    {



        if (auth('admin')->user()) {
            if ($value == 1) {
                $days=['Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday'];
                $from=json_decode($this->work_from);
                $to=json_decode($this->work_to);
                $index=array_search(date('l'), $days);
                if ($from != null) {
                    $currentday_availability=$to[$index];
                    if ($currentday_availability == null) {
                        return  $this->availability =0;
                    } else {
                        $from=strtotime($from[$index]);
                        $to=strtotime($to[$index]);
                        $current=strtotime(Carbon::now());
                        // dd($from > $to);

                        if ($to < $from) {
                            $to=strtotime('+1 day', $to);
                        }
                        if ($current >= $from && $current <= $to) {
                            return  $this->availability =1;
                        } else {
                            return  $this->availability =0;
                        }
                    }
                } else {
                    // dd('aa', $this->email);
                    return  $this->availability =0;
                }
            } else {
                return  $this->availability =0;
            }
        }elseif( auth('api') && $this->roles[0]->name =='cook'){
            if ($value == 1) {
                $days=['Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday'];
                $from=$this->work_from;
                // dd($this->work_from , $this->from);
                $to=$this->work_to;
                $index=array_search(date('l'), $days);
                if ($from != null) {
                    // dd( $to[$index]);
                    $currentday_availability=$to[$index];
                    // dd($currentday_availability);
                    if ($currentday_availability == null) {
                        return  $this->availability =0;
                    }else {
                        // dd('a');
                        $from=strtotime($from);
                        $to=strtotime($to);
                        $current=strtotime(Carbon::now());

                        if ($to < $from) {
                            $to=strtotime('+1 day', $to);
                        }
                        if ($current >= $from && $current <= $to) {
                            // dd($current >= $from && $current <= $to,'aa');
                            return  $this->availability =1;

                        } else {
                            return  0;

                        }
                    }
                } else {
                    // dd('aa', $this->email);
                    return 0;

                }
            } else {
                $value =0;
                return  $value;
            }
        }
        else{
            if ($value == 1) {
                $from=($this->work_from);
                $to=($this->work_to);
                if ($from != null) {
                    $currentday_availability=$to;
                    if ($currentday_availability == null) {
                        return  $this->availability =0;
                    } else {
                        $from=strtotime($from);
                        $to=strtotime($to);
                        $current=strtotime(Carbon::now());
                        // dd($from > $to);

                        if ($to < $from) {
                            $to=strtotime('+1 day', $to);
                        }
                        if ($current >= $from && $current <= $to) {
                            return  $this->availability =1;
                        } else {
                            return  $this->availability =0;
                        }
                    }
                } else {
                    // dd('aa', $this->email);
                    return  $this->availability =0;
                }
            } else {
                return  $this->availability =0;
            }
        }
    }


    // public function setFromAttribute($value)
    // {
    //     $this->attributes['from'] = strtolower($value);
    // }

    public function getWorkToAttribute($value)
    {
        // dd('a');
        if (!auth('admin')->user()) {
            $days=['Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday'];

            $from=json_decode($value);
            $index=array_search(date('l'), $days);
            // dd($from , $index,$from[$index]);
            if ($from) {
                return $from[$index];
            }
        }else{
            return $value;
        }

    }
    public function getWorkFromAttribute($value)
    {
        if (!auth('admin')->user()) {
            $days=['Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday'];
            $to=json_decode($value);
            $index=array_search(date('l'), $days);
            if ($to) {
                return $to[$index];
            }
        }else{
            return $value;
        }
    }

    public function getLikedAttribute($value)
    {
        $default=0;
        if(auth('api_customer')->user()){
            $ids=auth('api_customer')->user()->favcooks->pluck('id')->toArray();
            if(in_array($this->id , $ids)){
                $default=1;
            }
        }
        return $this->liked=$default;
    }

}
