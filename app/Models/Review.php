<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{


    protected $fillable=['rating','review','vendor_id','order_id'];

    public function Order(){
        return $this->belongsTo('App\Models\Order');
    }

    public function vendor(){
        return $this->belongsTo('App\Models\User','vendor_id');
    }
}
