<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $table='address';
    protected $fillable=['coordinates','address','user_id','name','phone','building'];

    public $appends = ['default'];


    public function users(){
        return $this->belongsTo('App\Models\User','user_id');

    }

    public function getDefaultAttribute ($default)
    {

        if (\Auth::check()) {

            $customer = auth('api_customer')->user();

            if(isset($customer)){
                if($customer['default_address'] !=null){
                    if($this->id == $customer->default_address){
                        return $this->default =1 ;
                    }else{
                        return $this->default =0 ;
                    }
                }else{
                    return $this->default = 1 ;

                }

            }
        }

    }
}
