<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable=['name','code','dishes','cusines','cooks','sections','total','uses','customers','discount','discount_type','activate_from','activate_to'];


    public function dishes(){
        return $this->hasMany('App\Models\Dish','discount_id');
    }

    public function discountCooks(){
        return $this->belongsToMany('App\Models\User','discount_user','discount_id','user_id');
    }

    public function discountDishes(){
        return $this->belongsToMany('App\Models\Dish','discount_dish','discount_id','dish_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order','discount_id');
    }

}




